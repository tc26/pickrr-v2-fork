from rest_framework import generics, status, views
from rest_framework.response import Response
import logging
from . import accessors, models, serializers, services, dao
from .errors import GeneralErrorCodes
from .exceptions import (EntityNotFoundException, InvalidRequestException)
logger = logging.getLogger(__name__)


class ZohoFetchOrderApi(views.APIView):

    def get(self, request):
        try:
            page = int(request.query_params.get('page', 1))
            pull_status = request.query_params.get('pull_status', '')
            auth_token = request.query_params.get('auth_token')
            shop_name = request.query_params.get('shop_name')
            zoho_user_info_obj = dao.get_zoho_user_info(auth_token, shop_name)
            if not zoho_user_info_obj:
                return Response({
                    'error': 'User has no associated store'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            if not pull_status:
                pull_status = zoho_user_info_obj.pull_status

            orders = services.get_zoho_orders_service(page, pull_status, auth_token, zoho_user_info_obj)

            return Response(orders, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({
                'error': str(e)
            },
                status=status.HTTP_400_BAD_REQUEST)


def get_client_zoho_status(zoho_user_obj, order_status):
    if order_status == 'OP':
        current_status = zoho_user_obj.order_placed
    elif order_status == 'RTO':
        current_status = zoho_user_obj.rto
    elif order_status == 'DL':
        current_status = zoho_user_obj.delivered
    elif order_status == 'OT':
        current_status = zoho_user_obj.transit
    elif order_status == 'PP':
        current_status = zoho_user_obj.picked_up
    elif order_status == 'OC':
        current_status = zoho_user_obj.cancelled
    else:
        current_status = None
    return current_status


class ZohoOrderStatusUpdateApi(views.APIView):
    """
    API to update order status on zoho
    """

    def post(self, request):
        try:
            if not request.data:
                return Response({
                    'error': 'Empty payload is not supported'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            auth_token, shop_name = request.data['auth_token'], request.data['shop_name']
            order_id = request.data['order_id']
            order_status = request.data['order_status']
            if not order_status:
                return Response({
                    'error': 'Order Status is Mandatory.'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            zoho_user_info_obj = dao.get_zoho_user_info(auth_token, shop_name)
            if not zoho_user_info_obj:
                return Response({
                    'error': 'No User Found.'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            order_status = get_client_zoho_status(zoho_user_info_obj, order_status)

            if not zoho_user_info_obj.update_tracking_status:
                return Response({
                    'error': 'Updating tracking status not allowed for this Shop'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            order_status_update_response = services.update_order_status_to_zoho(order_id, order_status, auth_token, zoho_user_info_obj)
            return Response(order_status_update_response, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({
                    'error': str(e)
                    },
                    status=status.HTTP_400_BAD_REQUEST)


class ZohoCarrierUpdateApi(views.APIView):
    """
    API to update order carrier on zoho
    """
    def put(self, request):
        try:
            if not request.data:
                return Response({
                    'error': 'Empty payload is not supported'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            auth_token, shop_name = request.data['auth_token'], request.data['shop_name']
            order_id = request.data['order_id']
            tracking_number = request.data['tracking_number']
            carrier = request.data['carrier']

            zoho_user_info_obj = dao.get_zoho_user_info(auth_token, shop_name)
            if not zoho_user_info_obj:
                return Response({
                    'error': 'No User Found.'
                    },
                    status=status.HTTP_404_NOT_FOUND)

            if not zoho_user_info_obj.update_tracking_id:
                return Response({
                    'error': 'Updating tracking id not allowed for this Shop'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            order_carrier_update_response = services.update_tracking_id_to_zoho(order_id, tracking_number, carrier, auth_token, zoho_user_info_obj)
            return Response(order_carrier_update_response, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({
                    'error': str(e)
                    },
                    status=status.HTTP_400_BAD_REQUEST)


class ZohoAPI(views.APIView):
    """
    API to authorize wix user
    """

    def get(self, request):
        """
        redirects to wix authorization url
        """
        code = request.query_params['code']
        response = {'code':code}

        return Response(response, status=status.HTTP_200_OK)


class ZohoAppAuthorizationApi(views.APIView):
    """
    API to authorize zoho user
    """

    def get(self, request):
        """
        redirects to zoho authorization url
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token, shop_name = request.query_params.get('auth_token'), request.query_params.get('shop_name')
            zoho_user_info_obj = dao.get_zoho_user_info(auth_token, shop_name)
            if not zoho_user_info_obj:
                return Response({
                    'error': 'No User Found.'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            response = services.zoho_authorize(zoho_user_info_obj)
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                logger.exception(
                    f'error while authorizing user with auth_token: {auth_token}, shop_name: {shop_name}')

        return response


class ZohoAccessTokenApi(views.APIView):
    """
    API to get access token for the user on getting auth_code from wix
    """

    def get_auth_code(self, request):
        auth_code = request.query_params['code']

        if not auth_code:
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': '`code`` not found from Zoho'
                }
            )

        return auth_code

    def get(self, request):
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token = request.query_params['state']
            try:
                user_info = models.ZohoUserModel.objects.get(
                    auth_token=auth_token)

            except models.ZohoUserModel.DoesNotExist:
                return Response({
                    'error': 'Invalid Auth'
                    },
                    status=status.HTTP_403_FORBIDDEN)

            if not user_info:
                return Response({
                    'error': 'No User Found.'
                    },
                    status=status.HTTP_404_NOT_FOUND)
            auth_code = self.get_auth_code(request)
            location  = request.query_params['location']
            if location == 'us':
                user_info.location = 'com'
            else:
                user_info.location = 'in'
            refresh_token = services.get_zoho_refresh_token(auth_code, location, user_info).json()
            if 'refresh_token' in refresh_token:
                refresh_token = refresh_token['refresh_token']
                user_info.refresh_token = refresh_token
                user_info.save()
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response
