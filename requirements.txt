# Core Requirements
pytz==2020.1
Django==2.2.8
django-environ==0.4.5
django-model-utils==3.2.0
djangorestframework==3.10.2
requests==2.24.0
django-cors-headers==3.4.0

# For the persistence stores
mysqlclient==1.4.6
pymongo==3.10.1

# Debugging
django-debug-toolbar==2.0

# celery
amqp==2.6.0
celery==4.4.6
django-celery-beat==2.0.0
django-celery-results==1.2.1
billiard==3.6.3.0
kombu==4.6.11

# others
django-ckeditor==5.9.0
django-js-asset==1.2.2
django-timezone-field==4.0

future==0.18.2
importlib-metadata==1.7.0
python-crontab==2.5.1
python-dateutil==2.8.1
WooCommerce==2.1.1
magento==3.1

boto==2.45.0

# Build
wheel==0.34.2
six==1.15.0
sqlparse==0.3.1
vine==1.3.0
zipp==3.1.0

# Code style
pylint==2.5.3

# utilities
dicttoxml==1.7.4
xmltodict==0.12.0


# plugins
ShopifyAPI==2.1.5
