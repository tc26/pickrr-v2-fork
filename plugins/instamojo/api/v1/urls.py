from django.urls import path

from .views import (AppAuthorizationView, AccessTokenView, FetchOrdersView,
                    OrderStatusUpdateView, CarrierUpdateView)


urlpatterns = [
    path(
        'authorize-user/',
        AppAuthorizationView.as_view(),
        name='instamojo_authorize_user'
    ),
    path(
        'access-token/',
        AccessTokenView.as_view(),
        name='instamojo_access_token'
    ),
    path(
        'fetch-orders/',
        FetchOrdersView.as_view(),
        name='fetch_instamojo_orders'
    ),
    path(
        'update-order-status/',
        OrderStatusUpdateView.as_view(),
        name='update_instamojo_order_status'
    ),
    path(
        'update-order-tracking/',
        CarrierUpdateView.as_view(),
        name='update_instamojo_order_tracking'
    )
]
