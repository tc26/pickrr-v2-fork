from enum import Enum

REQUEST_TIMEOUT = 120

EASYECOM_PICKRR_CLIENT_ID = "v4b1nms1f9ulenbsz0uqbmp29pd6wyg5"
EASYECOM_API_BASE_URL = "https://api.easyecom.io"
JUNGLEWORKS_API_BASE_URL = "https://api.yelo.red/"
PICKRR_BIKAYI_API_KEY = "46b5cf51a5fa8bc7761d086a48ff694aa5c5ad08ad3988ee7a3938a8"
PICKRR_BIKAYI_API_SECRET = "37b3f0dbdd14701b60d88cae9032cb78e9977f0a673bfde902b914a2"
PICKRR_BIKAYI_APPID = "PICKRR"
PICKRR_BASE_URL = "https://pickrr.com/"
PICKRRTESTING_BASE_URL = "https://pickrrtesting.com/"
PICKRR_HEROKU_BASE = "https://pickrr.herokuapp.com/"


class PickrrEndpoints(Enum):
    PICKRR_BASE_URL = "https://pickrr.com/"
    FETCH_SHOPIFY_CLIENTS = "fetch-shopify-client/?all"
    FETCH_SHOPIFY_CLIENT_BY_SHOP = "fetch-shopify-client/?shop_name="
    UPDATE_PLATFORM_TOKEN = "api/client/platform-tokens/update/"
    PLACE_ORDER = "api/place-order/"
    BULK_ORDER_CSV_VALIDATION = "api/pickrr-order-excel-check-format-upload-api/"


pickrr_shopify_status_mapper = {
    "PP": "picked_up",
    "OT": "in_transit",
    "DL": "delivered",
    "RTO": "failure",
    "RTD": "failure"
}

pickrr_easyecom_status_mapper = {
    "PP": 2,
    "OT": 2,
    "DL": 3,
    "RTO": 17,
    "RTD": 9
}


class WooCommercePickrrConstants(Enum):
    FETCH_CLIENTS = 'plugins/woocom/clients/'


pickrr_jungleworks_status_mapper = {
    "PP": 2,
    "OT": 2,
    "DL": 3,
    "RTO": 17,
    "RTD": 9
}


class PrestaShopEndpoints(Enum):
    ORDERS = '/api/orders'
    ADDRESS = '/api/addresses'
    PRODUCTS = '/api/products'
    ORDER_CARRIERS = '/api/order_carriers'


class PrestaShopPaymentModules(Enum):
    BANK_TRANSFER = 'ps_wirepayment'
    CHECK_PAYMENT = 'ps_checkpayment'


class ShopifyEndpoints(Enum):
    BASE = 'https://{shop_name}.myshopify.com'
    ORDERS = '/admin/api/2020-07/orders/{order_id}.json'


class WixConstants(Enum):
    WIX_APP_ID = 'e1f2ee84-11db-4bc7-9696-a719de232bff'
    WIX_APP_SECRET = '50189896-5824-4c03-b1fe-2eb08324ce05'
    WIX_ORDERS_PAGE_LIMIT = 100


class WixEndpoints(Enum):
    ORDERS = 'https://www.wixapis.com/stores/v2/orders/query'
    ORDER = 'https://www.wixapis.com/stores/v2/orders/{orderId}'
    AUTH = 'https://www.wix.com/oauth/access'
    CREATE_FULFILLMENT = 'https://www.wixapis.com/stores/v2/orders/{orderId}/fulfillments'
    UPDATE_FULFILLMENT = 'https://www.wixapis.com/stores/v2/orders/{orderId}/fulfillments/{fulfillmentId}'  # ALERT:
    # update_fulfillment is in dev preview stage at wix, subject to change in future.

    AUTHORIZE_APP = 'https://www.wix.com/installer/install'
    COMPLETE_AUTHENTICATION = 'https://www.wix.com/_api/site-apps/v1/site-apps/token-received?' \
                              'access_token={access_token}'


class WixPaymentMethods(Enum):
    COD = 'offline'


class PickrrOrderDict(dict):
    def __init__(self):
        kwargs = {
            'order_id': None,
            'created_at': None,
            'platform_order_id': None,
            'pickrr_dict': {
                'auth_token': None,
                'auth_access_token': None,
                'website_url': None,

                'from_name': None,
                'from_address': None,
                'from_pincode': None,
                'from_phone_number': None,
                'from_email': None,

                'to_name': None,
                'to_address': None,
                'to_pincode': None,
                'to_phone_number': None,
                'to_email': None,

                'item_list': None,
                'items_name': None,
                'quantity': None,
                'item_length': None,
                'item_weight': None,
                'item_height': None,
                'item_breadth': None,
                'other_info': None,

                'invoice_number': None,
                'invoice_value': None,
                'cod_amount': None,

                'client_order_id': None,
                'platform_order_id': None,
            }
        }
        super().__init__(**kwargs)

ZANDUCARE_SKU_WEIGHT_MAP = {
    "400013259": 0.15,
    "400011239": 0.14,
    "400011242": 0.14,
    "400007038": 1,
    "400012433": 0.09,
    "400011198": 0.14,
    "400013232": 0.06,
    "400011250": 0.14,
    "400009287": 0.92,
    "400010534": 1.01,
    "400009455": 0.5,
    "400009450": 1.065,
    "400011244": 0.14,
    "400008525": 0.53,
    "400009500": 1.05,
    "400009301": 0.41,
    "400009499": 0.55,
    "400011038": 0.02,
    "400003393": 0.02,
    "400008697": 0.04,
    "400011248": 0.14,
    "400012374": 0.48,
    "400004194": 0.06,
    "400011245": 0.14,
    "400010331": 0.25,
    "400013233": 0.11,
    "400007493": 0.78,
    "400011194": 0.14,
    "400009449": 0.55,
    "400011251": 0.14,
    "400009453": 0.5,
    "400013255": 0.05,
    "400007036": 0.38,
    "400005067": 0.68,
    "400011195": 0.14,
    "400005236": 0.03,
    "400013254": 0.06,
    "400007492": 0.42,
    "400011249": 0.14,
    "400004193": 0.065,
    "400011243": 0.14,
    "400011246": 0.14,
    "400011240": 0.14,
    "400011197": 0.14,
    "400011196": 0.14,
    "400004197": 0.06,
    "400008874": 0.25,
    "400012376": 0.015,
    "999999999": 0.06,
    "400012559": 0.09,
    "400010931": 0.58,
    "400012558": 0.08,
    "400011241": 0.14,
    "400010893": 0.04,
    "400006841": 0.01,
    "400012557": 0.1,
    "400010004": 0.035,
    "400011193": 0.14,
    "400013234": 0.5,
    "400013235": 0.9,
    "400012555": 0.065,
    "400011247": 0.14,
    "400003032": 0.79,
    "400012328": 0.27,
    "400005055": 0.27,
    "400003011": 0.03,
    "400003078": 0.02,
    "400003373": 0.005,
    "400010582": 0.02,
    "400005100": 0.075,
    "400010581": 0.02,
    "400003458": 0.25,
    "400012568": 0.27,
    "400005623": 0.075,
    "400009190": 0.43,
    "400009192": 0.43,
    "400009193": 0.43,
    "400009197": 0.1,
    "400009199": 0.1,
    "400009200": 0.1,
    "400009535": 0.43,
    "400009536": 0.77,
    "400012076": 0.01,
    "400011371": 0.85,
    "400003283": 0.02,
    "400005065": 0.15,
    "400005066": 0.29,
    "400009472": 0.07,
    "400010752": 1,
    "400006956": 0.04,
    "400010896": 0.08,
    "400009998": 0.02,
    "400009993": 0.02,
    "400009992": 0.03,
    "400005094": 0.02,
    "400010920": 0.08,
    "400005054": 0.23,
    "400005050": 0.04,
    "400005371": 0.075,
    "400006950": 0.07,
    "400006949": 0.12,
}

ZANDUCARE_PICKRR_AUTH_TOKEN = "01d0d081e1a3fa186d7cd97ce4daa2e5125402"

SHOPIFY_MIGRATED_CLIENTS = [
    "36af5c05fa1317700ad50ccb3921a8c65544", "c5feda14a09da0a418f23c3efc15ab35136947",
    "8c56657e0eb47d78dec2acdb49e7ea80135854", "10875444a8ba8ff3d691b6685efb9b6d137249",
    "243016caad06a714a2b7f8a46c8c4a83138258", "ca4d63d3030ebd52ef2ddc1097796fd3136437",
    "d81f4283ad80541195b0b8801d2c6bce142477", "cdd9844237892f9e65a3096d8c7bbdf9132708",
    "b16e35137507adbe572e5f6cb8809172131646", "e7e2928658e9ef7889f6fc62d98f18b9126471",
    "e5100f6b64ad9a802c988d19a11630ed145566", "1befbea369242ffd42b34f12d2ce25c5141513",
    "89ae88e4a51dcc607b1e13641438fc2e114669", "55fc7ab6d0298a61975bee530980050f142277",
    "36af5c05fa1317700ad50ccb3921a8c64455", "00bef6ac1d5993bbce16d83177073233136530",
    "23d6e4868b867d74ffe73ba7e232d07b148288", "e7e2928658e9ef7889f6fc62d98f18b9126471",
    "fad5bd45c22baef444ad58ae90a70a90146068", "941b0393d16323d2d87b4937cabc0fdd151296",
    "5bb7fd63cc8d2889c7d1a723f80a90f6150098"
]
