from django.urls import path

from .views import UpdateChannelInfoView, BulkOrderCSVValidation, CreateChannelClient


urlpatterns = [
    path('update/channel/',
         UpdateChannelInfoView.as_view(),
         name='update_channel'),
    path(
        'bulk-order/translator/',
        BulkOrderCSVValidation.as_view(),
        name='bulk_order_translator'
    ),
    path(
        'channel/client/create/',
        CreateChannelClient.as_view(),
        name='create_channel_client'
    )
]
