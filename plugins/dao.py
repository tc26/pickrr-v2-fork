from . import models
from .errors import PrestaShopErrorCodes
from .exceptions import (EntityNotFoundException, )


def get_eazyslip_user_info(auth_token, shop_name):
    try:
        user_info = models.EazySlipsUserInfo.objects.get(
            auth_token=auth_token, shop_name=shop_name)

    except models.EazySlipsUserInfo.DoesNotExist:
        return None

    return user_info


def get_magento_user_info(auth_token, shop_name, version="magento_v2"):
    try:
        user_info = models.MagnetoUserInfo.objects.get(
            auth_token=auth_token, shop_name=shop_name, version=version)

    except models.MagnetoUserInfo.DoesNotExist:
        return None

    return user_info


def easyecom_user_info_obj(auth_token, shop_name=None):
    try:
        if shop_name:
            user_info = models.EasyEcomUserInfo.objects.get(
                auth_token=auth_token, shop_name=shop_name)
        else:
            user_info = models.EasyEcomUserInfo.objects.get(
                auth_token=auth_token)
    except models.EasyEcomUserInfo.DoesNotExist:
        return None

    return user_info


def jungleworks_user_info_obj(auth_token, shop_name=None):
    try:
        if shop_name:
            user_info = models.JungleworksUserInfo.objects.get(
                auth_token=auth_token, shop_name=shop_name
            )
        else:
            user_info = models.JungleworksUserInfo.objects.get(
                auth_token=auth_token
            )
    except models.JungleworksUserInfo.DoesNotExist:
        return None

    return user_info


def get_opencart_user_info(auth_token, shop_name):
    try:
        user_info = models.OpencartUserInfo.objects.get(
            auth_token=auth_token, shop_name=shop_name)

    except models.OpencartUserInfo.DoesNotExist:
        return None

    return user_info


def get_wix_user_info(auth_token, shop_name):
    try:
        user_info = models.WixUserInfo.objects.get(
            auth_token=auth_token,
            shop_name=shop_name
        )
    except models.WixUserInfo.DoesNotExist:
        return None

    return user_info


class ModelDaoBase:
    """
    Common Base Dao layer for all models
    """

    @property
    def model(self):
        raise NotImplementedError('Define model attribute in child class')

    def validate_auth_token(self, auth_token):
        user_objects = self.model.objects.filter(auth_token=auth_token)
        if not user_objects:
            raise EntityNotFoundException(
                PrestaShopErrorCodes.ENTITY_NOT_FOUND,
                **{'message': 'Invalid auth_token provided!! No such token exists in system'}
            )

        return user_objects

    def validate_shop_name(self, auth_token, shop_name):
        # could be done from fetched results, would save one db call, but after large data, won't be efficient
        try:
            user_obj = self.model.objects.get(auth_token=auth_token, shop_name=shop_name)
        except self.model.DoesNotExist:
            raise EntityNotFoundException(
                PrestaShopErrorCodes.ENTITY_NOT_FOUND,
                **{'message': f'Invalid shop name provided!! User is not associated with {shop_name}'}
            )

        return user_obj

    def validate_user(self, auth_token: str, shop_name: str = None, **kwargs):
        """
        Base validate user method
        validation is first done on auth_token, if wrong: separate dedicated error is given
        and then validation on shop_name or other parameters
        ** override this if validation is done on any other parameters than these

        :param auth_token: authorization token generated at seller end
        :param shop_name: [optional] respective shop name
        """

        # user_objects = self.validate_auth_token(auth_token)
        user_obj = self.validate_shop_name(auth_token, shop_name)  # if shop_name else user_objects

        return user_obj


class PrestaShopModelDao(ModelDaoBase):
    model = models.PrestaShopPickupAddress

    @classmethod
    def fetch_addresses(cls, auth_token):
        addresses = cls.model.objects.filter(user__auth_token=auth_token)
        return addresses


class WixModelDao(ModelDaoBase):
    model = models.WixPickupAddress

    @classmethod
    def fetch_addresses(cls, auth_token):
        addresses = cls.model.objects.filter(user__auth_token=auth_token)
        return addresses


def get_bikayi_user_info(auth_token, shop_name=None):
    try:
        if shop_name:
            user_info = models.BikayiUserInfo.objects.get(
                auth_token=auth_token, shop_name=shop_name)
        else:
            user_info = models.BikayiUserInfo.objects.get(
                auth_token=auth_token)

    except models.BikayiUserInfo.DoesNotExist:
        return None

    return user_info


def get_magento1_user_info(auth_token, shop_name, version="magento_v1"):
    try:
        user_info = models.MagnetoUserInfo.objects.get(
            auth_token=auth_token, shop_name=shop_name, version=version)

    except models.MagnetoUserInfo.DoesNotExist:
        return None

    return user_info


def get_zoho_user_info(auth_token, shop_name):
    try:
        user_info = models.ZohoUserModel.objects.get(
            auth_token=auth_token, shop_name=shop_name)

    except models.ZohoUserModel.DoesNotExist:
        return None

    return user_info
