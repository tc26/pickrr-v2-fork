from . import dao
from plugins import constants
import requests
from .constants import SYNC_WOOCOM_TO_PICKRR_V1, SYNC_SHOPIFY_TO_PICKRR_V1
from plugins import commonutils


def sync_woocom_data_to_pickrr(params):
    try:
        url = "https://pickrr.com/plugins/woocom-client/update/"
        params["store_name"] = params["shop_name"]
        commonutils.make_post_request(url, params)
    except Exception as e:
        return {"err": str(e)}


def sync_shopify_data_to_pickrr(params):
    try:
        url = "https://pickrr.com/plugins/shopify-client/update/"
        payload = dict(
            auth_token=params["auth_token"], shop_name=params["shop_name"],
            token=params["token"], platform="shopify"
        )
        commonutils.make_post_request(url, payload)
    except Exception as e:
        return {"err": str(e)}


def create_wooccom_client_service(params):
    try:
        res = dao.create_wooccom_client(params)
        if SYNC_WOOCOM_TO_PICKRR_V1:
            sync_woocom_data_to_pickrr(params)
        return res
    except Exception as e:
        return {"err": str(e)}


def create_magento_client_service(params):
    try:
        return dao.create_magento_client(params)
    except Exception as e:
        return {"err": str(e)}


def create_easyecom_client_service(params):
    try:
        return dao.create_easyecom_client(params)
    except Exception as e:
        return {"err": str(e)}


def create_shopify_client_service(params):
    try:
        shop_name = params["shop_name"]
        auth_token = params["auth_token"]
        url = constants.PICKRR_HEROKU_BASE + constants.PickrrEndpoints.FETCH_SHOPIFY_CLIENT_BY_SHOP.value + shop_name
        client_shopify_info = requests.get(url).json()["clients"][0]
        if not auth_token == client_shopify_info["pickrr_auth_token"]:
            return {
                "err": "Unable to connect with shopify account. Please verify provided details or connect with support team."
            }
        params["email"] = client_shopify_info["failure_emails"]
        params["token"] = client_shopify_info["token"]
        params["update_tracking_id"] = True
        params["update_tracking_status"] = True
        res = dao.create_shopify_client(params)
        if SYNC_SHOPIFY_TO_PICKRR_V1:
            sync_shopify_data_to_pickrr(params)
        return res
    except Exception as e:
        return {"err": str(e)}
