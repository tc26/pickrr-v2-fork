def get_pickrr_platform_token_update_payload(data: dict, platform: str) -> dict:
    return {
        'platform': platform,
        'shop_name': data['shop_name'],
        'shop_token': data['token'],
        'auth_token': data['auth_token']
    }


def wix_create_fulfillment_payload(data, order):
    payload = {
        'fulfillment': {
            'lineItems': [],
            'trackingInfo': {
                'shippingProvider': data['carrier'],
                'trackingNumber': data['tracking_number'],
            }
        }
    }

    for item in order.get('lineItems'):
        payload['fulfillment']['lineItems'].append({'index': item['index']})

    return payload


def wix_update_fulfillment_payload(data):
    return {
        'fulfillmentTrackingInfo': {
            'shippingProvider': data['carrier'],
            'trackingNumber': data['tracking_number'],
        }
    }
