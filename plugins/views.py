from rest_framework import generics, status, views
from rest_framework.response import Response
from .commonutils import make_get_request
from . import models


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class PickrrTrackingApi(views.APIView):

    def get(self, request):
        ip = get_client_ip(request)
        tracking_id = request.query_params.get('tracking_id', None)
        client_order_id = request.query_params.get('client_order_id', None)
        remote_add, created = models.RemoteAddressTracker.objects.get_or_create(
            ip=ip)
        if remote_add.is_active:
            remote_add.traffic += 1
            remote_add.daily_traffic += 1
            remote_add.save()
            if tracking_id:
                url = "https://async.pickrr.com/track/tracking/?tracking_id=" + str(tracking_id)
            elif client_order_id:
                url = "https://async.pickrr.com/track/tracking/?client_order_id=" + str(client_order_id)
            else:
                return Response({"err": "Invalid Filters"}, status=status.HTTP_200_OK)
            res = make_get_request(url, None, 2)
            return Response(res, status=status.HTTP_200_OK)
        else:
            return Response({}, status=status.HTTP_200_OK)


class UpdateRemoteAddress(views.APIView):

    def get(self, request):
        models.RemoteAddressTracker.objects.filter(is_active=True).update(daily_traffic=0)
        return Response({"success": True}, status=status.HTTP_200_OK)
