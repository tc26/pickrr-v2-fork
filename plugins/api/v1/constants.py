
SYNC_WOOCOM_TO_PICKRR_V1 = True
SYNC_SHOPIFY_TO_PICKRR_V1 = True


class ChannelModelsMap:
    ECWID = 'ecwid'
    EAZYSLIPS = 'eazyslips'
    MAGENTO = 'magento'
    EASYECOM = 'easyecom'
    JUNGLEWORKS = 'jungleworks'
    OPENCART = 'opencart'
    PRESTASHOP = 'prestashop'
    BIKAYI = 'bikayi'
    SHOPIFY = 'shopify'
    WOOCOM = 'woocommerce'
    INSTAMOJO = 'instamojo'
