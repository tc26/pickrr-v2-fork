from datetime import datetime
from typing import Any, Dict, List, Optional

import bson
import pymongo
from django.conf import settings


def connect_mongo():
    """
    Connect with the appropriate MongoDB server
    """
    database = settings.MONGO_SETTINGS['DB_NAME']
    host = settings.MONGO_SETTINGS['HOST']
    port = settings.MONGO_SETTINGS['PORT']
    user = settings.MONGO_SETTINGS['USER']
    password = settings.MONGO_SETTINGS['PASSWORD']

    client = pymongo.MongoClient(host, port)
    db = client[database]
    if bool(user):
        db.authenticate(user, password)
    return db


class MongoMixin:

    def __init__(self):
        self._db = connect_mongo()
        self._collection = self._db[self.COLLECTION_NAME]

    def get_object_by_mongo_id(self, mongo_id: str) -> Optional[Dict[str, Any]]:
        """
        Finds the object by ObjectId
        """
        object_id = bson.ObjectId(mongo_id)

        return self._collection.find_one({'_id': object_id})

    def get_objects_by_query(self, query: Optional[Dict[str, Any]]):
        """
        Finds the objects by query
        """
        return self._collection.find(query)

    def insert(self, data_dict: Dict[str, Any]):
        """
        Inserts a single object in the collection
        """
        data_dict['created'] = datetime.now()
        data_dict['modified'] = datetime.now()
        inserted_id = self._collection.insert_one(data_dict).inserted_id
        return self.get_object_by_mongo_id(inserted_id)

    def insert_many(self, data_list: List[Dict[str, Any]]):
        """
        Inserts multiple objects in the collection
        """
        for data in data_list:
            data['created'] = datetime.now()
            data['modified'] = datetime.now()
        self._collection.insert_many(data_list)

    def update_object_by_mongo_id(self, mongo_id: str, data: Dict[str, Any]):
        """
        Updates the object by ObjectId
        """
        data['modified'] = datetime.now()
        object_id = bson.ObjectId(mongo_id)
        self._collection.update_one({'_id': object_id}, {'$set': data})

    def update_objects_by_query(self, query: Dict[str, Any], data: Dict[str, Any]):
        """
        Updates the objects matching the query
        """
        data['modified'] = datetime.now()

        self._collection.update_many(query, {'$set': data})

    def remove_object_by_mongo_id(self, mongo_id: str):
        """
        Remove the object by ObjectId
        """
        object_id = bson.ObjectId(mongo_id)
        return self._collection.delete_one({'_id': object_id})


class PickupAddressService(MongoMixin):
    COLLECTION_NAME = 'pickup_address'

    def create_pickup_address(self, pickup_address_info: Dict[str, Any]):
        """
        Create a pickup address object
        """
        return self.insert(pickup_address_info)

    def update_pickup_address(self, mongo_id, data):
        """
        Update the pickup address by mongo id
        """
        self.update_object_by_mongo_id(mongo_id, data)

    def remove_pickup_address(self, mongo_id):
        """
        Remove the pickup address by mongo id
        """
        self.remove_object_by_mongo_id(mongo_id)

    def get_client_pickup_address(self, auth_token: str) -> List[Dict[str, Any]]:
        """
        Gets all pickup addresses of the client.
        """
        return list(self.get_objects_by_query({'auth_token': auth_token}))
