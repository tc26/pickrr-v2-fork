class ExceptionBase(Exception):
    """
    Custom exception base class to be inherited by all exceptions raised
    """
    def __init__(self, error, **message_params):
        error_message = error.value.format(**message_params)
        super().__init__(error_message)


class EntityNotFoundException(ExceptionBase):
    """
    Exception raised when requested entity is not present
    """

    pass


class InvalidRequestException(ExceptionBase):
    """
    Exception raised when invalid request received or to narrow down when payload schema is invalid
    """

    pass


class InputFileHandlerException(ExceptionBase):
    """
    Exception raised when invalid file type is uploaded to request
    """
