import logging
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.request import Request

from . import dao, easyecomservices, models
from . import constants
from . import commonutils

logger = logging.getLogger('easyecom')

class EasyEcomFetchOrderApi(views.APIView):

    def get_filters(self, request):
        page = int(request.query_params.get('page', 1))
        status = request.query_params.get('status', '')
        date_from = request.query_params.get('date_from', '')
        date_to = request.query_params.get('date_to', '')
        next_url = request.query_params.get('next_url', '')
        filters = {
            'page': page,
            'date_from': date_from,
            'date_to': date_to
        }

        if next_url:
            filters['next_url'] = next_url

        if status:
            filters['status'] = status

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        easyecom_user_info_obj = dao.easyecom_user_info_obj(auth_token, shop_name)

        if not easyecom_user_info_obj:
            return Response({
                'error': 'User has no associated store'
            },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        version = request.query_params.get('v', 'v1')
        orders = easyecomservices.easyecom_orders_service(filters, auth_token, easyecom_user_info_obj, version)

        return Response(orders, status=status.HTTP_200_OK)


class EasyEcomAmazonRegApi(views.APIView):

    def get(self, request: Request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        version = request.query_params.get('v', 'v1')
        easyecom_user_info_obj = dao.easyecom_user_info_obj(auth_token, shop_name)

        if not easyecom_user_info_obj:
            return Response({'error': 'User has no associated store'}, status=status.HTTP_404_NOT_FOUND)

        try:
            user_addresses = models.EasyEcomPickupAddress.objects.filter(user=easyecom_user_info_obj)

            if user_addresses and len(user_addresses) > 0:
                address = user_addresses[0]
            else:
                return Response({'error': 'User has no associated pickup address'}, status=status.HTTP_404_NOT_FOUND)
        except Exception:
            return Response({'error': 'User has no associated pickup address'}, status=status.HTTP_404_NOT_FOUND)

        # This only runs when the user (EasyEcomUserInfo) is already present in the db and also the address (EasyEcomPickupAddress) associated with it.
        res = easyecomservices.easyecom_amazon_registration(easyecom_user_info_obj, address, version)

        return Response(res, status=status.HTTP_200_OK)

    # This only runs when the user (EasyEcomUserInfo) is not created, nor the address (EasyEcomPickupAddress) associated with it.
    def post(self, request: Request):
        try:
            version = request.query_params.get('v', 'v1')
            res = easyecomservices.add_user_info(request.data, version)
            return Response(res, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"err": e.args[0]}, status=e.args[0]['code'] if 'code' in e.args[0] else status.HTTP_400_BAD_REQUEST)


class EasyecomUpdateOrderStatus(views.APIView):
    """
    {
      "company_carrier_id": 288,
      "current_shipment_status_id": 3,
      "awb": "SRTC0040326616",
      "estimated_delivery_date": "2020-09-29",
      "history_scans": [
        {
          "status": "Return to Origin Intransit RTO",
          "time": "2020-09-20 15:34:00",
          "location": "DEL/RTO, Delhi NCR, Delhi"
        },
        {
          "status": "Return to Origin As per client (Pradeep) confirmation (Marked Shipment RTO & changed Destination HubID to 312)",
          "time": "2020-07-24 08:01:00",
          "location": "Pen, Pen, MAHARASHTRA"
        }
      ]
    }
    """

    def get_valid_status(self, params):
        status = params["status"]
        valid_status = constants.pickrr_easyecom_status_mapper[status]
        return valid_status

    def post(self, request):
        try:
            params = commonutils.read_post_json(request.body)
            # auth_token = params['auth_token']
            # easyecom_user_info_obj = dao.easyecom_user_info_obj(auth_token)
            #
            # if not easyecom_user_info_obj:
            #     return Response({
            #         'error': 'User has no associated store'
            #     },
            #         status=status.HTTP_404_NOT_FOUND)
            url = constants.EASYECOM_API_BASE_URL + "/Carrier/updateTrackingStatus?api_token=e754e595c43034f3d40d525fe614b5063f25166f44a9e8eb24245826ed31b7aa" #% easyecom_user_info_obj.api_access_token
            #track_status = self.get_valid_status(params)
            payload = {
                #"company_carrier_id": int(easyecom_user_info_obj.company_carrier_id),
                "current_shipment_status_id": 17 ,# track_status,
                "awb": 9 ,# params["tracking_id"],
            }
            res = commonutils.make_post_request(url, payload)
            logger.info(f'Tracking status sent to Easycom, request: {payload}, response: {res}')

            return Response(res, status=status.HTTP_200_OK)
        except Exception as e:
            logger.exception(e)
            return Response({"err": str(e)}, status=status.HTTP_200_OK)
