from plugins import models


def create_wooccom_client(params):
    try:
        shop_name = params["shop_name"]
        client_acc_name = params["client_acc_name"]
        auth_token = params["auth_token"]
        wooccom_user_obj, created = models.WoocommUserInfo.objects.get_or_create(
            auth_token=str(auth_token), shop_name=str(shop_name))
        params.pop('auth_token')
        params.pop('shop_name')
        params.pop('client_acc_name')
        failed = {}
        has_update = False
        for dict_key, dict_val in params.items():
            if dict_key not in wooccom_user_obj.__dict__.keys():
                failed[dict_key] = 'not found'
            wooccom_user_obj.__setattr__(dict_key, dict_val)
            has_update = True
        if has_update:
            wooccom_user_obj.pickrr_accounts = client_acc_name + ":" + auth_token
            wooccom_user_obj.save()
        return failed
    except Exception as e:
        return {"err": str(e)}


def create_magento_client(params):
    try:
        shop_name = params["shop_name"]
        client_acc_name = params["client_acc_name"]
        auth_token = params["auth_token"]
        magento_user_obj, created = models.MagnetoUserInfo.objects.get_or_create(
            auth_token=str(auth_token), shop_name=str(shop_name))
        params.pop('auth_token')
        params.pop('shop_name')
        params.pop('client_acc_name')
        failed = {}
        has_update = False
        for dict_key, dict_val in params.items():
            if dict_key not in magento_user_obj.__dict__.keys():
                failed[dict_key] = 'not found'
            magento_user_obj.__setattr__(dict_key, dict_val)
            has_update = True
        if has_update:
            magento_user_obj.pickrr_accounts = client_acc_name + ":" + auth_token
            magento_user_obj.save()
        return failed
    except Exception as e:
        return {"err": str(e)}


def create_easyecom_client(params):
    try:
        return True
    except Exception as e:
        return {"err": str(e)}


def create_shopify_client(params):
    try:
        shop_name = params["shop_name"]
        client_acc_name = params["client_acc_name"]
        auth_token = params["auth_token"]
        shopify_user_obj, created = models.ShopifyUserModel.objects.get_or_create(
            auth_token=str(auth_token), shop_name=str(shop_name))
        params.pop('auth_token')
        params.pop('shop_name')
        params.pop('client_acc_name')
        failed = {}
        has_update = False
        for dict_key, dict_val in params.items():
            if dict_key not in shopify_user_obj.__dict__.keys():
                failed[dict_key] = 'not found'
            shopify_user_obj.__setattr__(dict_key, dict_val)
            has_update = True
        if has_update:
            shopify_user_obj.pickrr_accounts = client_acc_name + ":" + auth_token
            shopify_user_obj.save()
        return failed
    except Exception as e:
        return {"err": str(e)}
