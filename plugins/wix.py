from datetime import datetime, timedelta

from rest_framework import views, status
from rest_framework.request import Request
from rest_framework.response import Response

from utils import permissions as custom_permissions

from .dao import ModelDaoBase
from .errors import GeneralErrorCodes
from .exceptions import (EntityNotFoundException, InvalidRequestException)
from .models import WixUserInfo, WixPickupAddress
from .services import (WixOrderFetchService, WixOrderFulfillmentService, WixAuthenticationService, wix_logger)
from .serializers import WixUserInfoSerializer, WixPickupAddressSerializer


class WixApiBase(views.APIView, ModelDaoBase):
    """
    Base API class for all Wix resources
    """

    model = WixUserInfo

    def validate_request(self, auth_token, shop_name):
        if not all([auth_token, shop_name]):
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Both `auth_token` and `shop_name` are mandatory in request'
                }
            )

    def update_obj(self, obj, data):
        for key, value in data.items():
            setattr(obj, key, value)


class WixOrderFetchApi(WixApiBase):
    """
    API to fetch orders from wix
    """

    def get_filters(self, request, user_obj):
        pull_status = request.query_params.get('status') or user_obj.pull_status
        offset = request.query_params.get('offset', 0)
        # wix has only partially, fully, and not fulfilled statuses

        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        if not all([date_from, date_to]):
            # if either or both of date_from and date_to is missing, set a default period of 7 days
            current_date = datetime.now()
            date_from = (current_date-timedelta(days=7)).strftime('%Y-%m-%d')
            date_to = current_date.strftime('%Y-%m-%d')

        filters = {
            'date_from': date_from,
            'date_to': date_to,
            'offset': offset
        }

        if pull_status:
            filters.update({
                'fulfillment_status': pull_status
            })

        return filters

    def get(self, request: Request) -> Response:
        """
        return: returns a Response object with orders fetched from wix
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token, shop_name = request.query_params.get('auth_token'), request.query_params.get('shop_name')
            self.validate_request(auth_token, shop_name)
            user_obj = self.validate_user(auth_token, shop_name)
            filters = self.get_filters(request, user_obj)
            service = WixOrderFetchService(user_obj)
            order_listing_response = service.fetch_orders(filters)
            response.data = order_listing_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while fetching orders. Request params: {request.query_params}')

        return response


class WixOrderFulfillmentApi(WixApiBase):
    """
    API to create, and update order fulfillement on wix
    """

    def check_mandatory_fields(self, request: Request, *fields):
        fields_not_present = []

        for field in fields:
            value = request.data.get(field)
            if not value:
                fields_not_present.append('`' + field + '`')

        if fields_not_present:
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': f"{', '.join(fields_not_present)} is mandatory in request"
                }
            )

    def post(self, request: Request) -> Response:
        """
        creates fulfillment for a particular order
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token, shop_name = request.data.get('auth_token'), request.data.get('shop_name')
            self.validate_request(auth_token, shop_name)

            if not request.data:
                raise InvalidRequestException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Empty payload is not supported'
                    }
                )
            # user calling update fulfillment need to provide index of the items here
            self.check_mandatory_fields(request, 'carrier', 'tracking_number', 'order_id')
            user_obj = self.validate_user(auth_token, shop_name)

            if not user_obj.update_tracking_id:
                raise InvalidRequestException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Creating tracking id not allowed for the user'
                    }
                )

            service = WixOrderFulfillmentService(user_obj)
            order = service.get_order(request.data['order_id'])
            create_fulfillment_response = service.create_fulfillment(request.data, order)
            response.data = create_fulfillment_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while fulfilling order. Request data: {request.data}')

        return response

    def put(self, request: Request) -> Response:
        """
        updates tracking details of a particular fulfillment of a particular order
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token, shop_name = request.data.get('auth_token'), request.data.get('shop_name')
            self.validate_request(auth_token, shop_name)

            if not request.data:
                raise InvalidRequestException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Empty payload is not supported'
                    }
                )

            self.check_mandatory_fields(request, 'carrier', 'tracking_number', 'order_id')
            user_obj = self.validate_user(auth_token, shop_name)

            if not user_obj.update_tracking_id:
                raise InvalidRequestException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Updating id not allowed for the user'
                    }
                )

            service = WixOrderFulfillmentService(user_obj)
            order = service.get_order(request.data['order_id'])
            update_fulfillment_response = service.update_fulfillment(request.data, order)
            response.data = update_fulfillment_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while fetching orders. Request data: {request.data}')

        return response


class WixAccessTokenApi(WixApiBase):
    """
    API to get access token for the user on getting auth_code from wix
    """

    def get_auth_code(self, request: Request) -> str:
        auth_code = request.query_params.get('code')

        if not auth_code:
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': '`code`` not found from Wix'
                }
            )

        return auth_code

    def get_instance_id(self, request: Request) -> str:
        instance_id = request.query_params.get('instanceId')

        if not instance_id:
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': '`instanceId` not found from Wix'
                }
            )

        return instance_id

    def get(self, request: Request) -> Response:
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token, shop_name = request.query_params['state'].split(':')
            user_obj = self.validate_user(auth_token, shop_name)
            auth_code = self.get_auth_code(request)
            user_obj.user_instance_id = self.get_instance_id(request)
            user_obj.save()

            service = WixAuthenticationService()
            response.data = service.get_access_token(auth_code, user_obj)

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response


class WixAppAuthorizationApi(WixApiBase):
    """
    API to authorize wix user
    """

    def get(self, request: Request) -> Response:
        """
        redirects to wix authorization url
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            auth_token, shop_name = request.query_params.get('auth_token'), request.query_params.get('shop_name')
            self.validate_request(auth_token, shop_name)
            self.validate_user(auth_token, shop_name)

            service = WixAuthenticationService()
            response = service.authorize(auth_token, shop_name)
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while authorizing user. Query Params: {request.query_params}')

        return response


class WixCreateUserApi(views.APIView):
    """
    API to get all user's info or to create wix user info
    """

    permission_classes = [custom_permissions.IsTokenVerified]

    def post(self, request):
        """
        request payload: {
            'auth_token': 'awxjwulljkjsdflk21',
            'shop_name': 'my_wix_store',
            .
            .(WixUserModel properties)
            .
            'website_url': 'https://wwww.mysite.wixsite.com'
        }
        """
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)
        try:
            user_obj = WixUserInfo.objects.create(**request.data)
            response.data = {'user': WixUserInfoSerializer(user_obj).data}
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error creating wix user with payload: {request.data}')
        return response


class WixFetchUserApi(WixApiBase):
    """
    API to get, update, or delete(deactivate) a particular user's info
    """

    def get(self, request):
        """
        request query_params:
        {
            'auth_token': 'awxjwulljkjsdflk21', #pickrr_auth_token,
            'shop_name': 'my_wix_store'
        }
        """
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)
        try:
            auth_token, shop_name = request.query_params.get('auth_token'), request.query_params.get('shop_name')
            self.validate_request(auth_token=auth_token, shop_name=shop_name)
            user_obj = self.validate_user(auth_token=auth_token, shop_name=shop_name)
            response.data = {'user': WixUserInfoSerializer(user_obj).data}
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while fetching wix user with params: {request.query_params}')
        return response


class WixUpdateUserApi(WixApiBase):
    """
    API to get, update, or delete(deactivate) a particular user's info
    """

    def post(self, request):
        """
        request payload: {
            'auth_token': 'awxjwulljkjsdflk21',
            'shop_name': 'my_wix_store',
            .
            .(WixUserModel properties)
            .
            'website_url': 'https://wwww.mysite.wixsite.com'
        }
        """
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)
        try:
            auth_token, shop_name = request.data.get('auth_token'), request.data.get('shop_name')
            self.validate_request(auth_token=auth_token, shop_name=shop_name)
            WixUserInfo.objects.filter(auth_token=auth_token, shop_name=shop_name).update(**request.data)
            user_obj = self.validate_user(auth_token=auth_token, shop_name=shop_name)
            response.data = {'user': WixUserInfoSerializer(user_obj).data}
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while updating user with payload: {request.data}')
        return response


class WixPickupAddressGetInsert(WixApiBase):
    """
    API to get, or create pickup address of a user
    """

    def get(self, request):
        """
        request query_params: {
            'auth_token': 'awxjwulljkjsdflk21', #pickrr_auth_token
            'shop_name': 'my_wix_store'
        }
        """
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)
        try:
            auth_token, shop_name = request.query_params.get('auth_token'), request.query_params.get('shop_name')
            self.validate_request(auth_token=auth_token, shop_name=shop_name)
            user_obj = self.validate_user(auth_token=auth_token, shop_name=shop_name)
            pickup_addresses = user_obj.wixpickupaddress_set.all().values()
            response.data = {'pickup_addresses': list(pickup_addresses)}
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while fetching pickup addresses with params: {request.query_params}')

        return response

    def post(self, request):
        """
        request payload:
        {
            "auth_token": "auth_token",
            "shop_name": "test_shop",
            "pickup_address": {
                "name": "Warehouse 1",
                "address_line": "enkay square",
                "pin_code": "122001",
                "phone_number": "12345767890",
                "seller_gstin": "alsjflasj",
                "email": "test@anymail.com"
                "is_default": True,  # optional, True by default
            }
        }
        """
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)
        try:
            auth_token, shop_name = request.data.get('auth_token'), request.data.get('shop_name')
            self.validate_request(auth_token=auth_token, shop_name=shop_name)
            user_obj = self.validate_user(auth_token=auth_token, shop_name=shop_name)
            address_to_create = request.data.get('pickup_address')

            if not address_to_create:
                raise InvalidRequestException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': '`pickup_address` is mandatory in request'
                    }
                )

            pickup_address = user_obj.wixpickupaddress_set.create(**address_to_create)
            response.data = {'pickup_address': WixPickupAddressSerializer(pickup_address).data}
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while creating pickup address with payload: {request.data}')
        return response


class WixPickupAddressUpdate(WixApiBase):
    """
    API to update a user's particular pickup address
    """

    def post(self, request):
        """
        request payload: {
            "auth_token": "awxjwulljkjsdflk21",  # pickrr_auth_token
            "shop_name": "my_wix_store",
            "pickup_offset": 2,  # 0-based offset of pickup address
            "pickup_address": {
                "name": "Warehouse 1",
                "address_line": "enkay square",
                "pin_code": "122001",
                "phone_number": "12345767890",
                "seller_gstin": "alsjflasj",
                "email": "test@anymail.com"
                "is_default": True,  # optional, True by default
            }
        }
        """
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)
        try:
            auth_token, shop_name = request.data.get('auth_token'), request.data.get('shop_name')
            self.validate_request(auth_token=auth_token, shop_name=shop_name)
            user_obj = self.validate_user(auth_token=auth_token, shop_name=shop_name)
            pickup_offset = request.data.get('pickup_offset')

            if not pickup_offset or not request.data.get('pickup_address'):
                raise EntityNotFoundException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': '`pickup_offset` and `pickup_address` are mandatory in request'
                    }
                )

            pickup_obj = user_obj.wixpickupaddress_set.all()[pickup_offset]
            self.update_obj(pickup_obj, request.data.get('pickup_address'))
            pickup_obj.save()
            response.data = {'pickup_address': WixPickupAddressSerializer(pickup_obj).data}
        except (InvalidRequestException, IndexError) as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
                wix_logger.exception(
                    f'error while updating pickup address with payload: {request.data}')

        return response
