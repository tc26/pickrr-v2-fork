from copy import deepcopy
from datetime import datetime, timedelta

from rest_framework import views, status
from rest_framework.request import Request
from rest_framework.response import Response

from .dao import ModelDaoBase
from .errors import PrestaShopErrorCodes
from .exceptions import (EntityNotFoundException, InvalidRequestException)
from .models import PrestaShopUserModel
from .services import (PrestaShopOrderFetchService, PrestaShopOrderUpdateService, PrestaShopOrderCarrierUpdateService)


class PrestaShopApiBase(views.APIView, ModelDaoBase):
    """Base API class for all prestashop resources"""

    model = PrestaShopUserModel

    def validate_request(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')

        if not all([auth_token, shop_name]):
            raise InvalidRequestException(
                PrestaShopErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Both `auth_token` and `shop_name are mandatory in request'
                }
            )


class PrestashopOrderFetchApi(PrestaShopApiBase):
    """
    API to fetch orders from prestashop
    """

    def get_filters(self, request, user_obj):
        pull_status = request.query_params.get('status') or user_obj.pull_status
        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        if not all([date_from, date_to]):
            # if either or both of date_from and date_to is missing, set a default period of 7 days
            current_date = datetime.now()
            date_from = (current_date-timedelta(days=7)).strftime('%Y-%m-%d')
            date_to = current_date.strftime('%Y-%m-%d')

        filters = {
            'date_from': date_from,
            'date_to': date_to,
            'date': 1,  # hardcoded as required for date range filtering on prestashop
        }
        if pull_status:
            filters.update({
                'filter[current_state]': pull_status
            })

        return filters

    def get(self, request: Request) -> Response:
        """
        :return: returns a Response object with orders fetched from prestashop
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            self.validate_request(request)
            auth_token, shop_name = request.query_params['auth_token'], request.query_params['shop_name']
            user_obj = self.validate_user(auth_token, shop_name)
            filters = self.get_filters(request, user_obj)
            service = PrestaShopOrderFetchService(user_obj)
            order_listing_response = service.fetch_orders(filters)
            response.data = order_listing_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response


class PrestashopOrderUpdateApi(PrestaShopApiBase):
    """
    API to update order on prestashop
    """

    def get_filters(self, request: Request) -> dict:
        order_id = request.query_params.get('order_id')

        if not order_id:
            raise InvalidRequestException(
                PrestaShopErrorCodes.INVALID_REQUEST,
                **{
                    'message': '`order_id` is mandatory in request'
                }
            )

        filters = {
            'order_id': order_id
        }

        return filters

    def put(self, request: Request) -> Response:
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            self.validate_request(request)
            auth_token, shop_name = request.query_params['auth_token'], request.query_params['shop_name']

            filters = self.get_filters(request)

            if not request.body:
                raise InvalidRequestException(
                    PrestaShopErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Empty payload is not supported'
                    }
                )
            payload = deepcopy(request.data)

            user_obj = self.validate_user(auth_token, shop_name)
            if not user_obj.update_tracking_status:
                raise InvalidRequestException(
                    PrestaShopErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Updating status not allowed for the user'
                    }
                )
            service = PrestaShopOrderUpdateService(user_obj, filters, payload)
            order_update_response = service.update_orders()
            response.data = order_update_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response


class PrestashopOrderCarrierUpdateApi(PrestaShopApiBase):
    """
    API to update order carrier on prestashop
    """

    def get_filters(self, request: Request) -> dict:
        order_id = request.query_params.get('order_id')
        carrier_id = request.query_params.get('carrier_id')

        if not all([order_id, carrier_id]):
            raise InvalidRequestException(
                PrestaShopErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Both `order_id` and `carrier_id` are mandatory in request'
                }
            )

        filters = {
            'order_id': [order_id],
            'carrier_id': [carrier_id]
        }

        return filters

    def put(self, request: Request) -> Response:
        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            self.validate_request(request)
            auth_token, shop_name = request.query_params['auth_token'], request.query_params['shop_name']

            filters = self.get_filters(request)

            if not request.body:
                raise InvalidRequestException(
                    PrestaShopErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Empty payload is not supported'
                    }
                )
            payload = deepcopy(request.data)

            user_obj = self.validate_user(auth_token, shop_name)
            if not user_obj.update_tracking_id:
                raise InvalidRequestException(
                    PrestaShopErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Updating tracking id not allowed for the user'
                    }
                )
            service = PrestaShopOrderCarrierUpdateService(user_obj, filters, payload)
            order_carrier_update_response = service.update_order_carrier()
            response.data = order_carrier_update_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response
