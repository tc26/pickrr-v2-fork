import os
import boto
from django.conf import settings
import requests
import json
from datetime import datetime, timedelta


def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None


def filter_or_none(model, **kwargs):
    try:
        return model.objects.filter(**kwargs)
    except model.DoesNotExist:
        return None


def make_post_request(url, payload, headers=None):
    if not headers:
        headers = {'content-type': 'application/json'}
    res = requests.post(url, data=json.dumps(payload), headers=headers)
    return res.json()


def make_post_request_with_file(url, payload, file=None, headers=None):
    if not headers:
        headers = {}
    res = requests.post(url, files=file, data=payload, headers=headers)
    return res.json()


def make_get_request(url, headers=None):
    if not headers:
        headers = {'content-type': 'application/json'}
    res = requests.get(url, headers=headers)
    return res.json()


def read_post_json(json_val):
    try:
        return json.loads(json_val)
    except Exception as e:
        return {'err': str(e)}


def correction_in_time(time_obj):
    if not time_obj:
        return ''
    time_correct = time_obj + timedelta(minutes=330)
    return time_correct.strftime("%d %b %Y, %H:%M")


def correction_in_time_wo_tz(time_obj):
    if not time_obj:
        return ''
    time_correct = time_obj
    return time_correct.strftime("%d %b %Y, %H:%M")


def correction_in_time_pickrr(time_obj):
    if not time_obj:
        return ''
    time_correct = time_obj
    return time_correct.strftime("%d-%m-%Y %H:%M")


def upload_file_to_s3(file=None, filename='NA', request=None):
    result = {}
    if request:
        file = request.FILES['data']
    try:
        file.seek(0, os.SEEK_END)
        size = file.tell()
        file.seek(0)
        if size > 1024 * 1024 * 1024:
            return {'err': 'upload file with size less then 1 GB'}
        conn = boto.connect_s3(settings.AWS_S3_ACCESS_KEY, settings.AWS_S3_SECRET_KEY)
        bucket = conn.get_bucket(settings.AWS_S3_BUCKET_NAME)
        filename = datetime.now().isoformat() + '_' + filename
        key = bucket.new_key(filename)
        resp = key.set_contents_from_file(file)
        if resp:
            key.make_public()
            result['url'] = 'https://{bucket}.{host}/{key}'.format(host=conn.server_name(), bucket=bucket, key=key)
        else:
            result.update({'err': 'error in uploading file'})
    except Exception as e:
        result.update({'err': f'error in uploading file: {str(e)}'})

    return result


def google_language_translator(text):
    try:
        from googletrans import Translator
        translator = Translator()
        translation = translator.translate(text)
        translation_text = translation.text
        return translation_text
    except Exception as e:
        return e
