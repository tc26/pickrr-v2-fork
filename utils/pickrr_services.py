# Define all the services here that makes connection to pickrr
# Try to keep the functions annotated
import json
from typing import Dict, Tuple, Optional

import requests
from django.conf import settings


def verify_auth_token(auth_token: str) -> bool:
    """
    This functin checks whether the token is valid or not

    Args:
        auth_token (str): The user's auth token

    Returns:
        bool
    """
    url = settings.PICKRR_BASE_URL + 'api/verify-auth-token/'
    data = {
        'auth_token': auth_token
    }

    res = requests.post(url=url, data=json.dumps(data))

    if res.ok:
        return True

    return False

