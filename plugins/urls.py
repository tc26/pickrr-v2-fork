from django.urls import path, include
from plugins import apis as plugin_apis, jungleworks
from plugins import eazyslips as ezs
from . import magento
from . import magento1
from . import easyecom
from . import shopify
from . import opencart
from . import prestashop
from . import views
from . import bikayi
from . import zoho
from . import wix
from . import apis

urlpatterns = [
    path('fetch-woocom-orders/', plugin_apis.WoocomFetchOrderAPI.as_view()),

    # Ecwid Store APIs - to get info on store, or create a store
    path(
        'fetch-all-ecwid-stores/',
        plugin_apis.UserAllEcwidStoresAPI.as_view(),
        name='retrieve_all_ecwid_stores'
    ),
    path(
        'fetch-user-all-stores/',
        plugin_apis.UserAllStoresAPI.as_view(),
        name='retrieve_all_ecwid_stores'
    ),
    path(
        'client-all-platforms/',
        plugin_apis.ClientAllPlatformAPI.as_view(),
        name='retrieve_all_client_stores'
    ),
    path(
        'client-store-info/',
        plugin_apis.ClientStoreInfoAPI.as_view(),
        name='retrieve_client_info'
    ),
    path(
        'client-all-stores-info/',
        plugin_apis.ClientAllStoresInfoAPI.as_view(),
        name='retrieve_client_all_stores_info'
    ),
    path(
        'ewcid_store_info/<int:store_id>/',
        plugin_apis.UserEcwidStoreAPI.as_view(),
        name='retrieve_update_ecwid_user_api'
    ),
    path('ewcid_store_info/', plugin_apis.UserEcwidStoreAPI.as_view(), name='create_ecwid_user_api'),

    path('fetch-ecwid-orders/', plugin_apis.EcwidFetchOrderAPI.as_view()),
    path('fetch-all-ecwid-users/', plugin_apis.EcwidUsersAPI.as_view()),

    # Pickup Address APIs
    path(
        'client-pickup-address/',
        plugin_apis.PickupAddressAPI.as_view(),
        name="get_or_create_client_pickup_address"
    ),
    path(
        'client-pickup-address/<str:mongo_id>',
        plugin_apis.PickupAddressAPI.as_view(),
        name="update_or_delete_client_pickup_address"
    ),

    # Apis to update data in Ecwid
    path(
        'ecwid/update-tracking-id/',
        plugin_apis.EcwidUpdateTrackingId.as_view(),
        name='udpate_ecwid_tracking_id'
    ),
    path(
        'ecwid/update-order-status/',
        plugin_apis.EcwidUpdateOrderStatus.as_view(),
        name='udpate_ecwid_tracking_id'
    ),

    path(
        'eazyslips/fetch-orders/',
        ezs.EazySlipsFetchOrderApi.as_view(),
        name='fetch_eazyslips_orders'
    ),

    path(
        'magento2/fetch-orders/',
        magento.Magento2FetchOrderApi.as_view(),
        name='fetch_magento2_orders'
    ),
    path(
        'magento2/update-order-status/',
        magento.Magento2UpdateOrderStatus.as_view(),
        name='update_magento2_orders'
    ),

    path(
        'easyecom/fetch-orders/',
        easyecom.EasyEcomFetchOrderApi.as_view(),
        name='fetch_easyecom_orders'
    ),
    path(
        'easyecom/amazon/registration/',
        easyecom.EasyEcomAmazonRegApi.as_view(),
        name='easyecom_amazon_reg'
    ),
    path(
        'easyecom/amazon/update-order-status/',
        easyecom.EasyecomUpdateOrderStatus.as_view(),
        name='easyecom_amazon_reg'
    ),

    path(
        'jungleworks/fetch-orders/',
        jungleworks.JungleworksFetchOrderApi.as_view(),
        name='fetch_jungleworks_orders'
    ),
    path(
        'jungleworks/update-order-status/',
        jungleworks.JungleworksUpdateOrderStatus.as_view(),
        name='jungleworks_update_status'
    ),
    path(
        'tracking/',
        views.PickrrTrackingApi.as_view(),
        name='pickrr_tracking_api'
    ),
    path(
        'update/remote-add/',
        views.UpdateRemoteAddress.as_view(),
        name='update_remote_add'
    ),
    path(
        'opencart/fetch-orders/',
        opencart.OpencartFetchOrderApi.as_view(),
        name='fetch_opencart_orders'
    ),
    path(
        'opencart/update-order-status/',
        opencart.OpencartUpdateOrderStatus.as_view(),
        name='update_opencart_orders_status'
    ),
    path(
        'opencart/update/tracking/',
        opencart.OpenCartUpdateTracking.as_view(),
        name='update_opencart_orders_tracking'
    ),

    path(
        'bikayi/fetch-orders/',
        bikayi.BikayiFetchOrderApi.as_view(),
        name='fetch_bikayi_orders'
    ),

    path(
        'bikayi/client/registration/',
        bikayi.BikayiClientRegistration.as_view(),
        name='bikayi_client_registration'
    ),

    path(
        'bikayi/update-order-status/',
        bikayi.BikayiUpdateOrderStatus.as_view(),
        name='update_bikayi_order_status'
    ),

    path(
        'bikayi/update/tracking/',
        bikayi.BikayiUpdateTracking.as_view(),
        name='update_bikayi_orders_tracking'
    ),

    # PRESTASHOP API ENDPOINTS
    path(
        'prestashop/fetch-orders/',
        prestashop.PrestashopOrderFetchApi.as_view(),
        name='prestashop_orders_listing_api'
    ),
    path(
        'prestashop/update-order/',
        prestashop.PrestashopOrderUpdateApi.as_view(),
        name='prestashop_order_update_api'
    ),
    path(
        'prestashop/update-order-carrier/',
        prestashop.PrestashopOrderCarrierUpdateApi.as_view(),
        name='prestashop_order_carrier_update_api'
    ),
    path(
        'bikayi/client/docs/',
        bikayi.BikayiMerchantDocs.as_view(),
        name='bikayi_merchant_docs'
    ),
    path(
        'bikayi/client/login/',
        bikayi.BikayiMerchantLogin.as_view(),
        name='bikayi_merchant_login'
    ),

    # SHOPIFY APIS
    path(
        'shopify/update-user-info/',
        shopify.UpdateShopifyUser.as_view(),
        name='shopify_update_user'
    ),
    path(
        'shopify/update/tracking/',
        shopify.UpdateTrackingDetailOnShopify.as_view(),
        name='update_shopify_tracking'
    ),
    path(
        'shopify/update/tracking-via-csv/',
        shopify.UpdateTrackingDetailsOnShopifyViaCsv.as_view(),
        name='update_shopify_tracking_csv'
    ),
    path(
        'shopify/update-order-status/',
        shopify.ShopifyUpdateOrderStatus.as_view(),
        name='shopify_update_order_status'
    ),
    path(
        'shopify/fetch-orders/',
        shopify.ShopifyOrderFetchApi.as_view(),
        name='shopify_fetch_orders'
    ),
    path(
        'shopify/sync-users/',
        shopify.SyncShopifyUsers.as_view(),
        name='shopify_sync_users'
    ),
    path(
        'shopify/update-order-tags/',
        shopify.UpdateOrderTags.as_view(),
        name='shopify_update_order_tags'
    ),

    path(
        'magento1/fetch-orders/',
        magento1.Magento1FetchOrderApi.as_view(),
        name='fetch_magento1_orders'
    ),

    path(
        'magento1/update-order-tracking/',
        magento1.Magento1CarrierUpdateApi.as_view(),
        name='update_magento1_order_tracking'
    ),

    path(
        'magento1/update-order-status/',
        magento1.Magento1OrderStatusUpdateApi.as_view(),
        name='update_magento1_order_status'
    ),
    path(
        'zoho/fetch-orders/',
        zoho.ZohoFetchOrderApi.as_view(),
        name='fetch_zoho_orders'
    ),
    path(
        'zoho/update-order-status/',
        zoho.ZohoOrderStatusUpdateApi.as_view(),
        name='update_zoho_order_status'
    ),
    path(
        'zoho/update-order-tracking/',
        zoho.ZohoCarrierUpdateApi.as_view(),
        name='update_zoho_order_tracking'
    ),
    path(
        'zoho/test/',
        zoho.ZohoAPI.as_view(),
        name='zoho_test'
    ),
    path(
        'zoho/authorize-user/',
        zoho.ZohoAppAuthorizationApi.as_view(),
        name='zoho_app_authorization'
    ),
    path(
        'zoho/user-access-token/',  # this is the redirect url* accept code and fetch r_token and store it.
        zoho.ZohoAccessTokenApi.as_view(),
        name='zoho_user_access_token'
    ),
    path('api/v1/', include('plugins.api.v1.urls')),

    # Wix APIs
    path(
        'wix/fetch-orders/',
        wix.WixOrderFetchApi.as_view(),
        name='wix_fetch_orders'
    ),
    path(
        'wix/order-fulfillment/',
        wix.WixOrderFulfillmentApi.as_view(),
        name='wix_order_fulfillment'
    ),
    path(
        'wix/authorize-user/',
        wix.WixAppAuthorizationApi.as_view(),
        name='wix_app_authorization'
    ),
    path(
        'wix/user-access-token/',  # this is the redirect url
        wix.WixAccessTokenApi.as_view(),
        name='wix_user_access_token'
    ),
    path(
        'wix/create-user/',
        wix.WixCreateUserApi.as_view(),
        name='wix_create_user'
    ),
    path(
        'wix/fetch-user/',
        wix.WixFetchUserApi.as_view(),
        name='wix_fetch_user'
    ),
    path(
        'wix/update-user/',
        wix.WixUpdateUserApi.as_view(),
        name='wix_update_user'
    ),
    path(
        'wix/users/pickup-addresses/',
        wix.WixPickupAddressGetInsert.as_view(),
        name='wix_pickup_addresses_get_insert'
    ),
    path(
        'wix/users/update-pickup-address/',
        wix.WixPickupAddressUpdate.as_view(),
        name='wix_user_pickup_address_update'
    ),
    path(
        'local-lang/place-order/',
        apis.PlaceOrderApi.as_view(),
        name='order_place'
    ),
    path(
        'translate/local-lang/',
        apis.TranslateData.as_view(),
        name='order_place'
    ),
    path('instamojo/api/v1/', include('plugins.instamojo.api.v1.urls')),

]
