from enum import Enum


class PrestaShopErrorCodes(Enum):
    INVALID_REQUEST = 'Invalid request received.! message - {message}'
    ENTITY_NOT_FOUND = 'Requested entity not found.! message - {message}'


class GeneralErrorCodes(Enum):
    INVALID_REQUEST = 'Invalid request received.! message - {message}'
    ENTITY_NOT_FOUND = 'Requested entity not found.! message - {message}'


class FileHandlerErrorCodes(Enum):
    EMPTY_CSV = 'Empty file received'
