import base64
import json
import logging
import os
import re
import sys
import time
import dateutil.parser as parser
from collections import defaultdict
from copy import deepcopy
from datetime import datetime, timedelta
from typing import Any, Dict, List, Optional, Tuple
from urllib.parse import urlencode

import dicttoxml
import magento
import requests
import woocommerce
import xmltodict
from django.conf import settings
from django.forms.models import model_to_dict
from django.shortcuts import redirect
from django.urls import reverse
from mongo import services as mongo_services
from plugins import models
from rest_framework import generics, status, views
from rest_framework.utils.urls import replace_query_param
from utils import helpers
from utils.helpers import google_language_translator, make_post_request
from plugins.instamojo.models import (UserModel as InstaMojoUserModel,
                                      PickupAddress as InstaMojoPickupAddess)

from . import commonutils, constants
from .dao import PrestaShopModelDao, WixModelDao
from .errors import PrestaShopErrorCodes, GeneralErrorCodes
from .exceptions import InvalidRequestException


logger = logging.getLogger(__name__)
wix_logger = logging.getLogger('wix')

WOOCOM_PER_PAGE_ORDERS = 100
ECWID_PER_PAGE_ORDERS = 100


def get_woocom_user_details(auth_token: str) -> Dict[str, str]:
    """
    Use this to fetch woocom user details from pickrr.
    Args:
        auth_token (str): Token uniquely identifying the user

    Returns:
        user_info (dict):  Woocom User info
    Raise:
        Exception if there is some error on fetching the details.
    """

    url = settings.PICKRR_BASE_URL + 'api/fetch-woocom-user-details/'
    data = {
        'auth_token': auth_token
    }

    res = requests.post(url=url, data=json.dumps(data))

    if not res.ok:
        raise Exception(res.json().get('error'))

    return res.json().get('user_woocom_info')


def get_clients_pickup_locations(auth_token: str) -> List[Dict[str, str]]:
    """
    Function to get all the client pickup locations associated with the user.

    Args:
        auth_token (str): Token uniquely identifying the user.

    Returns:
        List[Dict[str, str]]: Pickup list of user's locations
    """

    url = settings.PICKRR_BASE_URL + 'api/fetch-client-pickup-locations/'

    data = {
        'auth_token': auth_token
    }

    res = requests.post(url=url, data=json.dumps(data))

    if not res.ok:
        raise Exception(res.json().get('error'))

    return res.json().get('pickup_list')


def get_clients_ecwid_pickup_locations(auth_token: str) -> List[Dict[str, str]]:
    """
    Function to get all the client pickup locations associated with the user.

    Args:
        auth_token (str): Token uniquely identifying the user.

    Returns:
        List[Dict[str, str]]: Pickup list of user's locations
    """

    # pickup_list = mongo_services.PickupAddressService().get_client_pickup_address(auth_token)
    pickup_list = models.PickupAddress.objects.filter(user__auth_token=auth_token)
    res = []

    for address in pickup_list:
        data = {
            'from_address': address.address_line,
            'from_email': address.email,
            'from_name': address.name,
            'from_phone_number': address.phone_number,
            'from_pincode': address.pin_code,
        }

        res.append(data)

    return res


def make_woocom_url(store_name: str,
                    consumer_key: str,
                    consumer_secret: str,
                    next_url: Optional[str],
                    before: Optional[str],
                    after: Optional[str],
                    status: Optional[str]) -> str:
    """
    Construct the url to fetch orders from the woocom servers.

    Args:
        store_name (str): User's store name.
        consumer_key (str): Key to authenticate user on woocom server.
        consumer_secret (str): Secret to authenticate user on woocom server.
        next_url (Optional[str]): Next url to fetch.
        before (Optional[str]): Limit response to resources published after a given ISO8601 compliant date.
        after (Optional[str]): Limit response to resources published before a given ISO8601 compliant date.
        status (Optional[str]): Limit response to orders assigned a specific status.

    Returns:
        str: Url to hit to get the orders
    """
    if not next_url:
        WOOCOM_BASE_URL = f"https://{store_name}/wp-json/wc/v3/orders?"
        if not after:
            after = (datetime.now() - timedelta(days=1)).isoformat()
        url = WOOCOM_BASE_URL + f"after={after}"

        if before:
            url += f"&before={before}"
        if status:
            url += f"&status={status}"

    else:
        url = next_url

    url += f"&per_page={WOOCOM_PER_PAGE_ORDERS}"
    url += f"&consumer_key={consumer_key}" + f"&consumer_secret={consumer_secret}"

    return url


def fetch_orders_from_woocom_https(
    url: str,
    user_woocom_info: Dict[str, Any]) -> Tuple[List[Dict[str, Any]], Optional[str]]:
    """
    Hit the woocom url and return the orders and the next url to fetch.

    Args:
        url (str): Url to hit
        user_woocom_info (Dict[str, Any]): Info about user woocom store.

    Raises:
        Exception: Log the error if it occurs and raises the exception

    Returns:
        Tuple[List[Dict[str, Any]], Optional[str]]: Woocom orders and the next url
    """
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        response = requests.get(url, headers=headers, timeout=30)

        if not response.ok:
            logger.debug("Error getting Woocom orders over https")
            raise Exception("Error getting orders over https")

        if "link" in response.headers:
            try:
                if len(response.headers["link"].split(",")) > 0:
                    link = response.headers["link"].split(",")[1]
                else:
                    link = response.headers["link"]
            except Exception:
                link = response.headers["link"]
            if link.split("; ")[1] == 'rel="next"':
                next_url = link.split(";")[0].replace(" ", "")[1:-1]
            else:
                next_url = None
        else:
            next_url = None

        woocom_orders = response.json()

        return woocom_orders, next_url

    except Exception as e:
        logger.exception("Error while getting orders from woocom")
        logger.exception(f"Woocom user store name {user_woocom_info['store_name']}")

        raise Exception('Unable to get data from woocom')


def get_woocom_orders_over_https(
    user_woocom_info: Dict[str, str],
    filter_dict: Dict[str, Any]
):
    store_name = user_woocom_info['store_name']
    consumer_key = user_woocom_info['username']
    consumer_secret = user_woocom_info['password']

    woocom_order_url = make_woocom_url(store_name, consumer_key, consumer_secret, **filter_dict)
    orders_data, next_url = fetch_orders_from_woocom_https(woocom_order_url, user_woocom_info)

    return orders_data, next_url


def get_woocom_orders_over_http(
    user_woocom_info: Dict[str, str],
    filter_dict: Dict[str, Any]
):
    store_name = user_woocom_info['store_name']
    consumer_key = user_woocom_info['username']
    consumer_secret = user_woocom_info['password']

    wcapi = woocommerce.API(
        url=f"http://{store_name}",
        consumer_key=consumer_key,
        consumer_secret=consumer_secret,
        version="wc/v3"
    )

    next_url = filter_dict.pop('next_url')
    if not next_url:
        next_url = 2

    else:
        filter_dict["page"] = next_url
        next_url += 1

    filter_dict["per_page"] = WOOCOM_PER_PAGE_ORDERS

    parms = {key: value for (key, value) in filter_dict.items() if value is not None}

    response = wcapi.get("orders", params=parms)

    if not response.ok:
        logger.exception(f"Error getting Woocom orders over http auth - {response.content}")
        raise Exception("Error Woocom orders over http auth")

    orders_data = response.json()

    next_url = str(next_url)

    return orders_data, next_url


def fetch_duplicate_client_orders(
    client_order_ids: List[str],
    auth_token: str,
    shop_platform: str,
) -> List[str]:
    """
    CLient may have orders already placed on the pickkr platform.
    So we check which orders are already present.

    Args:
        client_order_ids (List[str]): List of order id fetched from the clients platform.
        auth_token (str): Token identifying the user.
        shop_platform (str): Identifies the order's origin platform.

    Returns:
        List[str]: List of order ids already present on pickrr
    """
    url = settings.PICKRR_BASE_URL + 'api/fetch-existing-client-orders-ids/'
    data = {
        'auth_token': auth_token,
        'client_order_ids': client_order_ids,
        'shop_platform': shop_platform,
    }

    res = requests.post(url=url, data=json.dumps(data))

    if not res.ok:
        return []
        raise Exception(res.json().get('error'))

    return res.json().get('duplicate_ids')


def prepare_pickrr_dict(auth_token: str,
                        order: Dict[str, Any],
                        from_location: Dict[str, Any]) -> Dict[str, Any]:
    """
    Convert order data fetched from woocom api and convert it into form required by pickrr.

    Args:
        auth_token (str): Token identifying the user.
        order (Dict[str, Any]): Order information.
        from_location (Dict[str, Any]): Location from which the pickup must happen.

    Returns:
        Dict[str, Any]: Converted order info
    """

    pickrr_dict = {}
    pickrr_dict.update(from_location)
    pickrr_dict['auth_token'] = auth_token
    pickrr_dict['item_list'] = []
    items_name = []
    quantity = 0

    for item in order['line_items']:
        item_dict = {}
        item_dict['item_name'] = item['name']

        items_name.append(item['name'])

        item_dict['sku'] = item['sku']
        if int(item['quantity']) > 1:
            item_dict['price'] = float(item['total']) / int(item['quantity'])
        else:
            item_dict['price'] = item['total']

        quantity += item['quantity']
        pickrr_dict['item_list'].append(item_dict)

    pickrr_dict['to_name'] = order['shipping']['first_name'] + " " + order['shipping']['last_name']
    pickrr_dict['to_name'] = (pickrr_dict['to_name']).replace("None", "").strip()
    pickrr_dict['to_phone_number'] = order['billing']['phone']
    pickrr_dict['to_pincode'] = order['shipping']['postcode'] if order['shipping']['postcode'] else order['billing'][
        'postcode']

    address_components = []
    address_components.append(order['shipping']['address_1'])
    address_components.append(order['shipping']['address_2'])
    address_components.append(order['shipping']['city'])
    address_components.append(order['shipping']['company'])
    address_components.append(order['shipping']['state'])
    address_components.append(order['shipping']['country'])

    pickrr_dict['to_address'] = ' '.join(address_components)
    pickrr_dict['to_address'] = pickrr_dict['to_address'].replace("None", "").strip()
    pickrr_dict['to_email'] = order['billing']['email']
    pickrr_dict['order_time'] = order['date_created']
    pickrr_dict['total_discount'] = order['discount_total']
    pickrr_dict['shipping_charge'] = order['shipping_total']
    pickrr_dict['invoice_number'] = 'retail' + str(order['id'])
    pickrr_dict['item_weight'] = 0.5
    pickrr_dict['item_length'] = 10
    pickrr_dict['item_height'] = 10
    pickrr_dict['item_breadth'] = 10
    pickrr_dict['item_name'] = ', '.join(items_name)
    pickrr_dict['invoice_value'] = order['total']
    pickrr_dict['client_order_id'] = str(order['id'])
    pickrr_dict['woocom_order_id'] = str(order['id'])

    if order['payment_method'].lower() == 'cod':
        cod_amount = order['total']
    else:
        cod_amount = 0.0

    pickrr_dict['cod_amount'] = cod_amount
    pickrr_dict['quantity'] = quantity

    return pickrr_dict


def fetch_most_suitable_location(order: Dict[str, Any], pickup_list: List[Dict[str, Any]]) -> Dict[str, Any]:
    """
    If client has multiple pickup locations. This function tries to suggest a pickup location.

    Args:
        order (Dict[str, Any]): Order information.
        pickup_list (List[Dict[str, Any]]): List of client's pickup locations

    Returns:
        Dict[str, Any]: Most suitable pickup location
    """
    from_location = pickup_list[0]
    sku = str(order["line_items"][0]["sku"])
    if len(sku) > 0:
        for address in pickup_list:
            if (address["from_name"].split('|')[0]).lower() in sku.lower():
                from_location = address
                break
    return from_location


def convert_orders_woocom_to_pickrr_format(pickup_loc_list: List[Dict[str, str]],
                                           orders_data: List[Dict[str, Any]],
                                           auth_token: str,
                                           woocom_user_info: Dict[str, str],
                                           next_url: Optional[str]
                                           ) -> Dict[str, Any]:
    """
    Convert all orders to form required by the frontend.

    Args:
        pickup_loc_list: Client's pickup locations.
        orders_data: Orders fetched from woocom server.
        auth_token: Token identifying the user.
        woocom_user_info: User's woocom info stored on pickrr.
        next_url (Optional[str]): Url to hit next for woocom data

    Returns:
        Dict[str, Any]: Dict containing all the info required by front-end.
            {
                uorders: Unplaced orders. Orders not present on pickrr.
                select_pickup_location:  Whether user must select the pickup location.
                uorders_id_list: Ids of orders not placed on pickrr.
                porders: Orders already placed on the pickrr platform.
                allorders: All the orders. Placed + Unplaced.
                id_list: Ids of all the orders.
                duplicate_ids: Ids of orders already on pickrr.
                next_url: Next url to hit to get orders.
                uorders_id: Unplaced order ids.
                store_name: Users woocom store name.
                pickup_list: List of all the clients pickup locations.
            }
    """
    converted_orders: Dict = {}
    unplaced_orders = []
    placed_orders = []
    all_orders = []
    unplaced_orders_id_list = []
    orders_id_list = []

    client_order_ids = [str(order['id']) for order in orders_data]
    duplicate_ids = fetch_duplicate_client_orders(client_order_ids, auth_token, shop_platform='woocommerce')
    if not duplicate_ids:
        duplicate_ids = []

    multi_location = False
    if woocom_user_info.get('is_multiple_location'):
        multi_location = True

    select_pickup_location = True if len(pickup_loc_list) > 1 else False  # Client has multiple pickup locations

    for order in orders_data:
        from_location = pickup_loc_list[0]
        if multi_location:
            from_location = fetch_most_suitable_location(order, pickup_loc_list)

        pickrr_dict = prepare_pickrr_dict(auth_token, order, from_location)
        order_id_str = str(order['id'])

        is_fulfilled = False
        if order_id_str in duplicate_ids:
            is_fulfilled = True

        current_order_data = {
            'platform_order_id': order_id_str,
            'created_at': order['date_created'],
            'status': order['status'],
            'pickrr_dict': pickrr_dict,
            'is_fulfilled': is_fulfilled
        }

        if is_fulfilled:
            placed_orders.append(current_order_data)

        else:
            unplaced_orders.append(current_order_data)
            unplaced_orders_id_list.append(order_id_str)

        orders_id_list.append(order_id_str)
        all_orders.append(current_order_data)

    converted_orders['uorders'] = unplaced_orders
    converted_orders['select_pickup_location'] = select_pickup_location
    converted_orders['uorders_id_list'] = unplaced_orders_id_list
    converted_orders["porders"] = placed_orders
    converted_orders["allorders"] = all_orders
    converted_orders['id_list'] = orders_id_list
    converted_orders['duplicate_ids'] = duplicate_ids
    converted_orders['next_url'] = next_url
    converted_orders['uorders_id'] = ','.join(unplaced_orders_id_list)

    converted_orders['store_name'] = woocom_user_info['store_name']
    converted_orders['pickup_list'] = pickup_loc_list

    return converted_orders


def get_woocom_orders_service(user_woocom_info: Dict[str, str],
                              params: Dict[str, Any]) -> Dict[str, Any]:
    """
    Function to get woocom orders for the user.

    Args:
        user_woocom_info (Dict): User woocom info
        params (Dict): Parameters to pass in woocom obj
    """

    auth_token = params['auth_token']

    filter_dict = {
        'next_url': params.get('next_url', None),
        'before': params.get('before', None),
        'after': params.get('after', None),
        'status': params.get('status', None),
    }

    try:
        orders_data, next_url = get_woocom_orders_over_https(user_woocom_info, filter_dict)
    except Exception:
        orders_data, next_url = get_woocom_orders_over_http(user_woocom_info, filter_dict)
    except Exception:
        raise Exception("Error fetching orders from woocom")

    pickup_loc_list = get_clients_pickup_locations(auth_token)

    orders = convert_orders_woocom_to_pickrr_format(
        pickup_loc_list,
        orders_data,
        auth_token,
        user_woocom_info,
        next_url
    )

    return orders


def pull_orders_from_ecwid(user_ewcid_info_obj: Dict[str, Any], filters: Dict[str, Any]) -> Dict[str, Any]:
    user_store_id = user_ewcid_info_obj.store_id
    token = user_ewcid_info_obj.store_private_token

    ecwid_orders_url = f'https://app.ecwid.com/api/v3/{user_store_id}/orders?token={token}'

    if 'page_no' in filters:
        # ecwid expects us to pass the offset as per per limit
        filters['page_no'] = (filters['page_no'] - 1) * ECWID_PER_PAGE_ORDERS
        filters['per_page'] = ECWID_PER_PAGE_ORDERS

    if 'from_date' in filters and filters['from_date']:
        filters['from_date'] = int(datetime.strptime(filters['from_date'], '%Y-%m-%d').timestamp())

        # since to_date will be the upper bound
        filters['to_date'] = datetime.strptime(filters['to_date'], '%Y-%m-%d') + timedelta(days=1)
        filters['to_date'] = int(filters['to_date'].timestamp())

    filters_mapping = {
        'page_no': 'offset',
        'per_page': 'limit',
        'from_date': 'createdFrom',
        'to_date': 'createdTo'
    }

    for param in filters:
        ecwid_param_name = filters_mapping.get(param, None)
        if not ecwid_param_name:
            ecwid_param_name = param

        if filters.get(param):
            ecwid_orders_url += f"&{ecwid_param_name}={filters[param]}"

    try:
        response = requests.get(url=ecwid_orders_url).json()

    except Exception as e:
        logger.exception('Error fetching orders from ecwid store : ')
        logger.exception(f'Ecwid store id: {user_store_id}')

        raise Exception('Unable to fetch orders from ecwid store')

    return response


def convert_ecwid_orders(ecwid_orders, auth_token, pickup_locations):
    converted_orders = {}
    unplaced_orders = []
    placed_orders = []
    all_orders = []
    unplaced_orders_id_list = []
    orders_id_list = []

    ecwid_order_ids = [order['vendorOrderNumber'] for order in ecwid_orders]

    select_pickup_location = True if len(pickup_locations) > 1 else False
    already_placed_order_ids = fetch_duplicate_client_orders(ecwid_order_ids, auth_token, shop_platform='ecwid')

    for order in ecwid_orders:
        order_info = {}
        order_info.update(pickup_locations[0])
        order_info['auth_token'] = auth_token
        order_info['item_list'] = []
        quantity = 0
        items_name = []
        shipping_total = 0

        for item in order['items']:
            item_dict = {}

            item_dict['item_name'] = item['name']
            items_name.append(item['name'])

            item_dict['sku'] = item['sku']
            item_dict['price'] = item['price']
            item_dict['quantity'] = item['quantity']

            shipping_total += item['shipping']

            quantity += item['quantity']
            order_info['item_list'].append(item_dict)

        ship_info = order.get('shippingPerson') if order.get('shippingPerson') else order.get('billingPerson')

        order_info['to_name'] = ship_info['name']
        order_info['to_phone_number'] = ship_info.get('phone', '')
        order_info['to_pincode'] = ship_info.get('postalCode')
        order_info['to_email'] = order['email']
        to_address = [
            ship_info.get('street'),
            ship_info.get('city'),
            ship_info.get('stateOrProvinceName',''),
            ship_info.get('countryName')
        ]

        created_at = parser.parse(order['createDate'])
        created_at = created_at.isoformat()
        order_info['order_time'] = created_at
        order_info['to_address'] = ' '.join(to_address)
        order_info['total_discount'] = order['discount'] + order['couponDiscount']
        order_info['shipping_charge'] = shipping_total
        order_info['invoice_number'] = 'retail' + order['vendorOrderNumber']

        order_info['item_weight'] = 0.5
        order_info['item_length'] = 10
        order_info['item_height'] = 10
        order_info['item_breadth'] = 10

        order_info['item_name'] = ', '.join(items_name)
        order_info['invoice_value'] = order['total']
        order_info['client_order_id'] = order['vendorOrderNumber']
        order_info['ecwid_order_id'] = order['vendorOrderNumber']
        if order['paymentMethod'] in ['Pay by cash', 'Cash on Delivery']:
            order_info['cod_amount'] = order['total']
        else:
            order_info['cod_amount'] = 0.0
        order_info['quantity'] = quantity

        is_fulfilled = False
        if order['vendorOrderNumber'] in already_placed_order_ids:
            is_fulfilled = True

        order_data = {
            'platform_order_id': order['vendorOrderNumber'],
            'created_at': created_at,
            'pickrr_dict': order_info,
            'is_fulfilled': is_fulfilled,
        }

        if is_fulfilled:
            placed_orders.append(order_data)
        else:
            unplaced_orders.append(order_data)
            unplaced_orders_id_list.append(order['vendorOrderNumber'])

        orders_id_list.append(order['vendorOrderNumber'])
        all_orders.append(order_data)

    converted_orders['uorders'] = unplaced_orders
    converted_orders['select_pickup_location'] = select_pickup_location
    converted_orders['uorders_id_list'] = unplaced_orders_id_list
    converted_orders["porders"] = placed_orders
    converted_orders["allorders"] = all_orders
    converted_orders['id_list'] = orders_id_list
    converted_orders['duplicate_ids'] = already_placed_order_ids
    converted_orders['uorders_id'] = ','.join(unplaced_orders_id_list)

    converted_orders['pickup_list'] = pickup_locations

    return converted_orders


def make_ecwid_pagination(total, count, absolute_url, filters):
    if total > ECWID_PER_PAGE_ORDERS:
        total_pages = total // ECWID_PER_PAGE_ORDERS
    else:
        total_pages = 1

    next_url = ''
    prev_url = ''
    current_page_no = filters['page_no']

    if current_page_no < total_pages:
        next_page_no = current_page_no + 1
        next_url = replace_query_param(absolute_url, 'page_no', next_page_no)

    if current_page_no > 1:
        prev_page_no = current_page_no - 1
        prev_url = replace_query_param(absolute_url, 'page_no', prev_page_no)

    return {
        'count': count,
        'total_pages': total_pages,
        'next_url': next_url,
        'prev_url': prev_url,
    }


def get_ecwid_orders_service(filters, absolute_url, auth_token, user_ewcid_info_obj):
    ecwid_orders_response = pull_orders_from_ecwid(user_ewcid_info_obj, deepcopy(filters))
    count = ecwid_orders_response['count']
    total = ecwid_orders_response['total']
    ecwid_orders = ecwid_orders_response.get('items')

    orders = []
    if ecwid_orders:
        pickup_loc_list = get_clients_ecwid_pickup_locations(auth_token)
        if not len(pickup_loc_list):
            return {"err": "Please Update Pickup Address."}
        orders = convert_ecwid_orders(ecwid_orders, auth_token, pickup_loc_list)

    response = make_ecwid_pagination(total, count, absolute_url, filters)
    response['store_id'] = user_ewcid_info_obj.store_id
    response.update({'orders': orders})

    return response


def update_ecwid_order(ecwid_user_obj, store_id, client_order_id, data):
    private_token = ecwid_user_obj.store_private_token

    update_url = f'https://app.ecwid.com/api/v3/{store_id}/orders/{client_order_id}?token={private_token}'

    headers = {
        'Content-Type': 'application/json;charset=utf-8'
    }

    res = requests.put(url=update_url, data=json.dumps(data), headers=headers)

    if not res.ok:
        return False, res.reason

    return True, None


def update_tracking_id_ecwid(store_id, tracking_id, client_order_id):
    resp, reas = False, None
    data = {
        'trackingNumber': tracking_id,
    }
    ecwid_user_obj = models.EcwidUserInfo.objects.get(store_id=store_id)
    if ecwid_user_obj.update_tracking_id:
        resp, reas = update_ecwid_order(ecwid_user_obj, store_id, client_order_id, data)
    if ecwid_user_obj.update_tracking_status and ecwid_user_obj.order_placed:
        status = ecwid_user_obj.order_placed
        resp, reas = update_order_status_ecwid(ecwid_user_obj, store_id, status, client_order_id)
    return resp, reas


def update_order_status_ecwid(ecwid_user_obj, store_id, status, client_order_id):
    accepted_status = [
        'AWAITING_PROCESSING',
        'PROCESSING',
        'SHIPPED',
        'DELIVERED',
        'WILL_NOT_DELIVER',
        'RETURNED',
        'READY_FOR_PICKUP',
    ]

    # if status not in accepted_status:
    #     return False, 'Status is not in accepted statuses'

    data = {
        'fulfillmentStatus': status
    }
    return update_ecwid_order(ecwid_user_obj, store_id, client_order_id, data)


def get_client_ecwid_status(ecwid_user_obj, order_status):
    current_status = None
    if order_status == 'RTD':
        current_status = ecwid_user_obj.rtd
    elif order_status == 'RTO':
        current_status = ecwid_user_obj.rto
    elif order_status == 'DL':
        current_status = ecwid_user_obj.delivered
    elif order_status == 'OT':
        current_status = ecwid_user_obj.transit
    elif order_status == 'PP':
        current_status = ecwid_user_obj.picked_up
    elif order_status == 'OC':
        current_status = ecwid_user_obj.cancelled
    else:
        pass
    return current_status


def get_eazyslips_orders_service(filters, absolute_url, auth_token, eazyslips_user_info_obj):
    response = {}
    response['stop_name'] = eazyslips_user_info_obj.shop_name  # needs to change dict key to shop name
    eazyslips_orders_response = pull_orders_from_eazyslips(eazyslips_user_info_obj, deepcopy(filters))
    if "error" in eazyslips_orders_response and eazyslips_orders_response["error"] in [1, '1']:
        response["err"] = eazyslips_orders_response["message"]
        return response
    message = eazyslips_orders_response.get('message')
    response["page"] = -1
    if type(message) == list and len(message):
        pickup_loc_list = get_clients_pickup_locations_by_type(auth_token, "eazyslips")
        orders = prepare_pickrr_orders_from_eazyslips_orders(eazyslips_user_info_obj, message, auth_token,
                                                             pickup_loc_list)
        response["pickup_locations"] = pickup_loc_list
        response["page"] = filters['page'] + 1
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["other"] = eazyslips_orders_response

    return response


def pull_orders_from_eazyslips(eazyslips_user_info_obj, filters: Dict[str, Any]) -> Dict[str, Any]:
    user_shop_name = eazyslips_user_info_obj.shop_name
    license_key = eazyslips_user_info_obj.license_key
    email = eazyslips_user_info_obj.email
    token = email + ":" + license_key
    encoded_token = base64.b64encode(token.encode('ascii')).decode("utf-8")
    Authorization = f"Basic {encoded_token}"
    if "status" not in filters:
        if eazyslips_user_info_obj.pull_status:
            filters["status"] = eazyslips_user_info_obj.pull_status
    eazyslips_orders_endpoint = f"https://ezyslips.com/api/getorders?page={filters['page']}"
    # ?page=0&status=O&date_from=2020-09-01&date_to=2020-09-04"
    if "status" in filters:
        eazyslips_orders_endpoint += f"&status={filters['status']}"
    else:
        eazyslips_orders_endpoint += "&status=O"
    if "date_from" in filters and filters["date_from"]:
        date_from = filters["date_from"]
    else:
        date_from = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    if "date_to" in filters and filters["date_to"]:
        date_to = filters["date_to"]
    else:
        date_to = datetime.now().strftime('%Y-%m-%d')
    eazyslips_orders_endpoint += f"&date_from={date_from}"
    eazyslips_orders_endpoint += f"&date_to={date_to}"
    headers = {
        'Authorization': Authorization
    }

    try:
        response = requests.get(url=eazyslips_orders_endpoint, headers=headers).json()

    except Exception as e:
        logger.exception('Error fetching orders from eazyslips store : ')
        logger.exception(f'Eazyslips shop name: {user_shop_name}')

        raise Exception('Unable to fetch orders from eazyslips store')

    return response


def get_clients_pickup_locations_by_type(auth_token: str, type: str) -> List[Dict[str, str]]:
    """
    Function to get all the client pickup locations associated with the user.

    Args:
        auth_token (str): Token uniquely identifying the user.

    Returns:
        List[Dict[str, str]]: Pickup list of user's locations
    """
    res = []
    pickup_list = []
    if type == "eazyslips":
        pickup_list = models.EazySlipsPickupAddress.objects.filter(user__auth_token=auth_token)
    elif type == "magento":
        pickup_list = models.MagentoPickupAddress.objects.filter(user__auth_token=auth_token)
    elif type == "magento1":
        pickup_list = models.MagentoPickupAddress.objects.filter(user__auth_token=auth_token)
    elif type == "easyecom":
        pickup_list = models.EasyEcomPickupAddress.objects.filter(user__auth_token=auth_token)
    elif type == "zoho":
        pickup_list = models.ZohoPickupAddress.objects.filter(user__auth_token=auth_token)
    elif type == "instamojo":
        pickup_list = InstaMojoPickupAddess.objects\
            .filter(user__auth_token=auth_token)
    for address in pickup_list:
        data = {
            'from_address': address.address_line,
            'from_email': address.email,
            'from_name': address.name,
            'from_phone_number': address.phone_number,
            'from_pincode': address.pin_code,
        }

        res.append(data)

    return res


def prepare_pickrr_orders_from_eazyslips_orders(user_obj, message, auth_token, pickup_loc_list):
    try:
        unplaced_orders = []
        placed_orders = []
        eazyslips_order_ids = [order['order_id'] for order in message]
        already_placed_order_ids = fetch_duplicate_client_orders(eazyslips_order_ids, auth_token,
                                                                 shop_platform='eazyslips')
        for order in message:
            if user_obj.is_multilocation:
                pass
            pickrr_dict = prepare_eazyslips_pickrr_dict(user_obj, auth_token, order, pickup_loc_list)
            if order['order_id'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def prepare_eazyslips_pickrr_dict(user_obj, auth_token, json_dict, pickup_loc_list):
    try:
        pickrr_dict = pickup_loc_list[0].copy()
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['item_list'] = []
        items_name = ""
        quantity = 0
        for item in json_dict['products']:
            item_dict = {}
            item_dict['item_name'] = item['product']
            if len(json_dict['products']) > 1:
                items_name = items_name + ', ' + (item['product'])
            else:
                items_name = item['product']
            item_dict['sku'] = item['product_code']
            item_dict['price'] = float(item['price']) / int(item['amount']) if int(item['amount']) > 0 else item[
                'price']
            item_dict['quantity'] = item['amount']
            quantity += int(item['amount'])
            pickrr_dict['item_list'].append(item_dict)
        pickrr_dict['to_name'] = str(str(json_dict['s_firstname']) + str(" ") + str(
            json_dict['s_lastname']))
        if len(pickrr_dict['to_name']) < 3:
            try:
                pickrr_dict['to_name'] = str(
                    str(json_dict['b_firstname']) + str(" ") + str(
                        json_dict['b_lastname']))
            except:
                pass
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['to_phone_number'] = str(json_dict['b_phone'])
        pickrr_dict['to_pincode'] = str(json_dict['s_zipcode'])
        if len(pickrr_dict['to_pincode']) == 0:
            try:
                pickrr_dict['to_pincode'] = str(json_dict['b_zipcode'])
            except:
                pass
        pickrr_dict['to_address'] = str(
            json_dict['s_address'] + str(" ") + json_dict['s_address_2']
            + str(" ") + json_dict['s_city'] + str(" ")
            + json_dict['s_state']
            + str(" ") + json_dict['s_country'])
        if len(pickrr_dict['to_address']) < 6:
            try:
                pickrr_dict['to_address'] = str(json_dict['b_address'] + str(" ") +
                                                json_dict['b_address_2'] + str(" ") +
                                                json_dict['b_city'] + str(" ") +
                                                json_dict['b_state'] + str(" ") +
                                                json_dict['b_country'])
            except:
                pass
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_email'] = json_dict['email']
        pickrr_dict['order_time'] = json_dict['order_date']
        pickrr_dict['total_discount'] = json_dict['discount']
        pickrr_dict['shipping_charge'] = json_dict['shipping_cost']
        pickrr_dict['invoice_number'] = str(json_dict['invoice_number'])
        pickrr_dict['item_weight'] = float(json_dict["weight"]) / 1000 if float(
            json_dict["weight"]) / 1000 > 0.5 else 0.5
        pickrr_dict['item_length'] = json_dict["box_length"] if int(json_dict["box_length"]) > 10 else 10
        pickrr_dict['item_height'] = json_dict["box_height"] if int(json_dict["box_length"]) > 10 else 10
        pickrr_dict['item_breadth'] = json_dict["box_breadth"] if int(json_dict["box_length"]) > 10 else 10
        pickrr_dict['item_name'] = items_name
        pickrr_dict['invoice_value'] = json_dict['order_total']
        pickrr_dict['client_order_id'] = str(json_dict['order_id'])
        pickrr_dict['platform_order_id'] = str(json_dict['ezyslip_order_id'])
        cod = 0.0
        payment_method = json_dict['payment_method'].lower()
        try:
            payment_id = int(json_dict['payment_id'])
            if payment_id == 6:
                cod = json_dict['order_total']
        except:
            if "cod" in payment_method or "cash on delivery" in payment_method:
                cod = json_dict['order_total']
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict = {
            "pickrr_dict": pickrr_dict,
            "order_id": str(json_dict['order_id']),
            "created_at": str(json_dict['order_date']),
            "platform_order_id": str(json_dict['ezyslip_order_id'])
        }
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def pull_orders_from_magento2(magento2_user_info_obj, filters: Dict[str, Any]) -> Dict[str, Any]:
    user_shop_name = magento2_user_info_obj.shop_name
    website_url = magento2_user_info_obj.website_url
    username = magento2_user_info_obj.username
    password = magento2_user_info_obj.password
    url = website_url + "/rest/V1/integration/admin/token"
    headers = {
        'content-type': 'application/json',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.'
                      '0.2661.102 Safari/537.36',
    }
    payload = {'username': username, 'password': password}
    token = requests.post(url, data=json.dumps(payload), headers=headers)
    access_token = "Bearer " + token.json()
    fetch_url = website_url + "/rest/V1/orders?"
    # fetch_url += "&searchCriteria[currentPage]=" + str(filters["page"])
    # fetch_url += "&searchCriteria[pageSize]=250"
    if "date_from" in filters and filters["date_from"]:
        date_from = filters["date_from"]
    else:
        date_from = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    if "date_to" in filters and filters["date_to"]:
        date_to = filters["date_to"]
        date_to = (datetime.strptime(date_to, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
    else:
        date_to = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')
    fetch_url += "searchCriteria[filter_groups][0][filters][0][field]=created_at&searchCriteria[filter_groups][0][filters][0][value]=%s&searchCriteria[filter_groups][0][filters][0][condition_type]=from&searchCriteria[filter_groups][1][filters][1][field]=created_at&searchCriteria[filter_groups][1][filters][1][value]=%s&searchCriteria[filter_groups][1][filters][1][condition_type]=to" % (
        date_from, date_to + " 23:59:00")
    try:
        response = requests.get(
            fetch_url,
            headers={'content-type': 'application/json',
                     "Authorization": access_token,
                     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) '
                                   'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.'
                                   '0.2661.102 Safari/537.36',
                     }
        ).json()
        response["auth_access_token"] = access_token
    except Exception as e:
        logger.exception('Error fetching orders from magento2 store : ')
        logger.exception(f'Magento2 shop name: {user_shop_name}')

        raise Exception('Unable to fetch orders from magento2 store')

    return response


def get_magento2_orders_service(filters, auth_token, magento2_user_info_obj):
    response = {}
    response['shop_name'] = magento2_user_info_obj.shop_name
    magento2_orders_response = pull_orders_from_magento2(magento2_user_info_obj, deepcopy(filters))
    if "items" not in magento2_orders_response or "message" in magento2_orders_response:
        response["err"] = magento2_orders_response
        return response
    response["page"] = -1
    if type(magento2_orders_response["items"]) == list and len(magento2_orders_response):
        pickup_loc_list = get_clients_pickup_locations_by_type(auth_token, "magento")
        orders = prepare_pickrr_orders_from_magneto2_orders(
            magento2_user_info_obj, magento2_orders_response, auth_token, pickup_loc_list)
        response["pickup_locations"] = pickup_loc_list
        response["page"] = filters['page'] + 1
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["other"] = magento2_orders_response
    response["page"] = -1
    return response


def prepare_pickrr_orders_from_magneto2_orders(user_obj, magento_response, auth_token, pickup_loc_list):
    try:
        unplaced_orders = []
        placed_orders = []
        items = magento_response["items"]
        magento2_order_ids = [order['increment_id'] for order in items]
        already_placed_order_ids = fetch_duplicate_client_orders(
            magento2_order_ids, auth_token, 'magento_v2')
        auth_access_token = magento_response["auth_access_token"]
        for order in magento_response["items"]:
            if user_obj.is_multilocation:
                pass
            pickrr_dict = magento2_to_pickrr_dict(user_obj, auth_token, order, pickup_loc_list, auth_access_token)
            if order['increment_id'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def magento2_to_pickrr_dict(user_obj, auth_token, json_dict, pickup_loc_list, auth_access_token):

    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:

        order_dict = {
            "pickrr_dict": "",
            "order_id": str(json_dict['increment_id']),
            "created_at": (
                datetime.strptime(json_dict['created_at'], '%Y-%m-%d %H:%M:%S') + timedelta(minutes=330)).strftime(
                '%Y-%m-%d  %H:%M:%S'),
            "platform_order_id": str(json_dict['increment_id'])
        }

        pickrr_dict = pickup_loc_list[0].copy() if pickup_loc_list else {}
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['auth_access_token'] = auth_access_token
        pickrr_dict['website_url'] = user_obj.website_url
        pickrr_dict['item_list'] = []
        other_info = []
        items_name = ""
        quantity = 0
        for item in json_dict['items']:
            item_dict = {}
            item_dict['item_name'] = item['name']
            if len(items_name) > 1:
                items_name = items_name + ', ' + (item['name'])
            else:
                items_name = item['name']
            item_dict['sku'] = item['sku']
            try:
                item_dict['price'] = float(item["price_incl_tax"]) / int(item['qty_ordered']) if int(
                    item['qty_ordered']) > 0 else item["price_incl_tax"]
            except:
                item_dict['price'] = float(item["parent_item"]["price_incl_tax"]) / int(item['qty_ordered']) if int(
                    item['qty_ordered']) > 0 else item["parent_item"]["price_incl_tax"]
            item_dict['quantity'] = item['qty_ordered']
            quantity += int(item['qty_ordered'])
            pickrr_dict['item_list'].append(item_dict)
            d = {
                "order_item_id": item['qty_ordered'],
                "qty": item['item_id']
            }
            other_info.append(d.copy())
        pickrr_dict["other_info"] = other_info
        try:
            address = json_dict["extension_attributes"]["shipping_assignments"][0]["shipping"]["address"]
        except:
            address = json_dict['billing_address']
        pickrr_dict['to_name'] = address['firstname'] + " " + address['lastname']
        pickrr_dict['to_email'] = address['email']
        pickrr_dict['to_phone_number'] = str(address['telephone'])
        pickrr_dict['to_pincode'] = str(address['postcode'])
        street = ','.join(address["street"])
        pickrr_dict['to_address'] = str(street + str(" ") +
                                        address['city'] + str(" ") +
                                        address['region'] + str(" ") +
                                        address['country_id'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['order_time'] = (
                datetime.strptime(json_dict['created_at'], '%Y-%m-%d %H:%M:%S') + timedelta(minutes=330)).strftime(
            '%Y-%m-%d  %H:%M:%S')
        pickrr_dict['item_weight'] = 0.5
        pickrr_dict['item_length'] = 10
        pickrr_dict['item_height'] = 10
        pickrr_dict['item_breadth'] = 10
        pickrr_dict['item_name'] = items_name
        pickrr_dict['client_order_id'] = str(json_dict['increment_id'])
        pickrr_dict['platform_order_id'] = str(json_dict['entity_id'])
        pickrr_dict['total_discount'] = json_dict['discount_amount']
        pickrr_dict['shipping_charge'] = json_dict['shipping_incl_tax']
        pickrr_dict['invoice_number'] = "retail_" + str(json_dict['increment_id'])
        pickrr_dict['invoice_value'] = json_dict['grand_total']
        cod = 0.0
        try:
            if json_dict['payment']["additional_information"][0].lower() == "cash on delivery":
                cod = json_dict['payment']['amount_ordered']
        except:
            pass
        if 'cashondelivery' in json_dict['payment']["method"].lower():
            cod = json_dict['payment']['amount_ordered']
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict


def update_tracking_id_to_magento2(
    items, notify, append_comment, order_id, tracking_id,
    pickrr_auth_token, magento2_user_info_obj=None, access_token=None):
    """
        {
            "items": [
                {
                    "order_item_id": 7,
                    "qty": 1
                }
            ],
            "notify": true,
            "appendComment": false,
            "tracks": [
                {
                    "track_number": "<a href='http://pickrr.com/tracking/#?tracking_id=PKRC1000094316'>PKRC1000094316</a>",
                    "title": "Pickrr Tracking",
                    "carrier_code": "Pickrr"
                }
            ]
        }
    :return:
    """
    if not access_token:
        if not magento2_user_info_obj:
            magento2_user_info_obj = models.MagnetoUserInfo.objects.get(auth_token=pickrr_auth_token)
        user_shop_name = magento2_user_info_obj.shop_name
        website_url = magento2_user_info_obj.website_url
        username = magento2_user_info_obj.username
        password = magento2_user_info_obj.password
        url = website_url + "/rest/V1/integration/admin/token"
        headers = {'content-type': 'application/json'}
        payload = {'username': username, 'password': password}
        token = requests.post(url, data=json.dumps(payload), headers=headers)
        access_token = "Bearer " + token.json()
    d = {
        "items": items,
        "notify": notify,
        "appendComment": append_comment,
        "tracks": [
            {
                "track_number": str(tracking_id),
                "title": "Pickrr Tracking",
                "carrier_code": "Pickrr"
            }
        ]
    }
    website_url = magento2_user_info_obj.website_url
    headers = {'content-type': 'application/json', "Authorization": access_token}
    url = website_url + "/rest/V1/order/%s/ship" % str(order_id)
    res = requests.post(url, data=json.dumps(d), headers=headers)
    return res.json()


def get_jungleworks_orders_service(auth_token, jungleworks_user_info_obj, filters):
    response = {}
    response['shop_name'] = jungleworks_user_info_obj.shop_name
    jungleworks_orders_response = pull_orders_from_jungleworks(jungleworks_user_info_obj, filters)
    if "data" not in jungleworks_orders_response or jungleworks_orders_response["status"] != 200:
        response["err"] = jungleworks_orders_response
        return response

    response["page"] = -1
    if type(jungleworks_orders_response["data"]["all_jobs"]) == list and len(jungleworks_orders_response):
        # pickup_loc_list = get_clients_pickup_locations_by_type(auth_token, "jungleworks")
        orders = prepare_pickrr_orders_from_jungleworks_orders(
            jungleworks_user_info_obj, jungleworks_orders_response, auth_token)
        response["pickup_locations"] = []
        # response["page"] = filters['page'] + 1
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["other"] = jungleworks_orders_response
    response["page"] = -1
    return response


def pull_orders_from_jungleworks(jungleworks_user_info_obj, filters):
    pull_status = jungleworks_user_info_obj.pull_status
    api_token = jungleworks_user_info_obj.api_key
    marketplace_user_id = jungleworks_user_info_obj.marketplace_user_id

    jungleworks_orders_url = constants.JUNGLEWORKS_API_BASE_URL + f'open/orders/getAll'

    if "date_from" in filters and filters["date_from"]:
        start_date = filters["date_from"]
    else:
        start_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    if "date_to" in filters and filters["date_to"]:
        date_to = filters["date_to"]
        end_date = (datetime.strptime(date_to, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
    else:
        end_date = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')
    payload = {
        "api_key": api_token,
        "marketplace_user_id": str(marketplace_user_id),
        "start_date": start_date,
        "end_date": end_date
    }
    # payload.update(filters)
    orders = commonutils.make_post_request(jungleworks_orders_url, payload)
    orders["auth_access_token"] = api_token
    return orders


def prepare_pickrr_orders_from_jungleworks_orders(user_obj, json_dict, auth_token):
    try:
        unplaced_orders = []
        placed_orders = []
        items = json_dict["data"]["all_jobs"]
        jungleworks_order_ids = [order['order_id'] for order in items]
        already_placed_order_ids = fetch_duplicate_client_orders(
            jungleworks_order_ids, auth_token, 'jungleworks')
        auth_access_token = json_dict["auth_access_token"]
        for order in items:
            if user_obj.is_multilocation:
                pass
            pickrr_dict = prepare_jungleworks_pickrr_dict(user_obj, auth_token, order, auth_access_token)
            if order['order_id'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}

def prepare_jungleworks_pickrr_dict(user_obj, auth_token, json_dict, auth_access_token):
    try:
        # json_dict = {
        #         "job_seller_count": 1,
        #         "job_status": 10,
        #         "pd_or_appointment": 2,
        #         "business_type": 1,
        #         "user_id": 682908,
        #         "order_id": 1964591,
        #         "job_id": 2873990,
        #         "pickup_job_status": 6,
        #         "delivery_job_status": 6,
        #         "seller_id": 682908,
        #         "creation_datetime": "2020-10-06T08:09:00.000Z",
        #         "job_delivery_datetime": "October 06 2020 02:09 PM",
        #         "job_delivery_datetime_utc": "2020-10-06T08:09:00.000Z",
        #         "job_description": None,
        #         "marketplace_user_id": 682908,
        #         "product_id": 0,
        #         "job_pickup_datetime": "October 06 2020 01:39 PM",
        #         "job_address": "101, near Apollo hospital, Sector 48, Gurugram, Haryana, India, 122001",
        #         "payment_type": "CASH",
        #         "customer_rating": None,
        #         "is_custom_order": 0,
        #         "order_currency_symbol": "$",
        #         "total_amount": 7760,
        #         "order_amount": 15500,
        #         "delivery_method": 2,
        #         "job_pickup_address": "101, near Apollo hospital, Sector 48, Gurugram, Haryana, India, 122001",
        #         "job_pickup_phone": "+917896543287",
        #         "job_pickup_name": "priya",
        #         "pickup_tracking_link": "",
        #         "delivery_tracking_link": "",
        #         "customer_id": 1964591,
        #         "customer_username": "priya",
        #         "task_type": 0,
        #         "transaction_id": "0",
        #         "transaction_status": None,
        #         "overall_transaction_status": None,
        #         "vendor_is_deleted": 0,
        #         "order_preparation_time": 0,
        #         "additionalPaymentViaLink": None,
        #         "is_scheduled": 0,
        #         "is_menu_enabled": 0,
        #         "edit_task": None,
        #         "debt_amount": 0,
        #         "product_details": [
        #             "Designer Saree$#@3.00",
        #             "Kanchipuram Saree$#@1.00",
        #             "Party Wear saree$#@2.00"
        #         ],
        #         "product_names": "Designer Saree, Kanchipuram Saree, Party Wear saree",
        #         "product_ids": "18952990,18953003,18953004",
        #         "delivery_system_job_id": None,
        #         "merchant_name": "e-denimstore",
        #         "store_name_json": "{\"en\": \"e-denimstore\"}",
        #         "storefront_user_id": 682908,
        #         "merchant_is_deleted": 0,
        #         "job_pickup_datetime_utc": "2020-10-06T08:09:00.000Z",
        #         "show_refund_popup": 1,
        #         "payment_method": 8,
        #         "can_change_status": 1,
        #         "customer_is_deleted": 0
        #     }

        # pickrr_dict = pickup_loc_list[0].copy()#pickup location available in json dict
        pickrr_dict = {}
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['from_address'] = str(json_dict['job_pickup_address'])
        pickrr_dict['from_address'] = str(json_dict['job_pickup_address']).replace("None", "").strip()
        pickrr_dict['from_name'] = str(json_dict['job_pickup_name'])
        pickrr_dict['from_phone_number'] = str(json_dict['job_pickup_phone'])
        from_pin = json_dict['job_pickup_address'].split(',')[-1].strip()
        pickrr_dict['from_pincode'] = from_pin
        # try:
        #     if from_pin.isnumeric() and len(from_pin) == 6:
        #         pickrr_dict['from_pincode'] = from_pin
        # except:
        #     pass
        pickrr_dict['item_list'] = []
        items_name = ""
        quantity = 0
        for item in json_dict['product_details']:
            item_dict = {}
            item_dict['item_name'] = item.split('$')[0]
            # if len(json_dict['product_details']) > 1:
            #     items_name = items_name + ', ' + (item['productName'])
            # else:
            #     items_name = item['productName']
            # item_dict['sku'] = item['sku']
            # item_dict['price'] = float(item['selling_price']) / int(item['quantity']) if int(item['quantity']) > 0 else item[
            #     'selling_price']
            item_dict['quantity'] = int(float(item.split('@')[1]))
            quantity += int(float(item.split('@')[1]))
            pickrr_dict['item_list'].append(item_dict)
        pickrr_dict['to_name'] = str(json_dict['customer_username'])
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        # pickrr_dict['to_phone_number'] = str(json_dict['b_phone'])#doubt not available
        pin = json_dict['job_address'].split(',')[-1].strip()
        pickrr_dict['to_pincode'] = pin
        # try:
        #     if pin.isnumeric() and len(pin) == 6:
        #         pickrr_dict['to_pincode'] = pin
        # except:
        #     pass
        pickrr_dict['to_address'] = str(json_dict['job_address'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        # pickrr_dict['to_email'] = json_dict['email']#doubt n.a
        pickrr_dict['order_time'] = json_dict['creation_datetime']
        # pickrr_dict['total_discount'] = json_dict['discount']# n.a
        # pickrr_dict['shipping_charge'] = json_dict['shipping_cost']#n.a
        pickrr_dict['invoice_number'] = "retail" + str(json_dict['order_id'])
        pickrr_dict['item_weight'] = 0.5
        pickrr_dict['item_length'] = 10
        pickrr_dict['item_height'] = 10
        pickrr_dict['item_breadth'] = 10
        pickrr_dict['item_name'] = json_dict['product_names']
        pickrr_dict['invoice_value'] = json_dict['total_amount']  # or order amount
        pickrr_dict['client_order_id'] = str(json_dict['order_id'])  # or order_id or user_id
        pickrr_dict['platform_order_id'] = str(json_dict['order_id'])
        cod = 0.0
        if json_dict['payment_type'].lower() == 'cash':
            cod = json_dict['order_amount']  # or total_amount
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        pickrr_dict['auth_access_token'] = auth_access_token
        order_dict = {
            "pickrr_dict": pickrr_dict,
            "order_id": str(json_dict['order_id']),
            "created_at": str(json_dict['creation_datetime']),
            "platform_order_id": str(json_dict['job_id'])
        }
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e), "other": error}


def prepare_client_store_info(user_store_list, platform_type, res=None):
    if res is None:
        res = []
    for user in user_store_list:
        accounts = []
        for acc in user.pickrr_accounts.split(','):
            d = {
                "account_name": acc.split(':')[0],
                "account_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        d = {}
        d["platform_type"] = platform_type
        d["auth_token"] = user.auth_token
        d["shop_name"] = user.shop_name
        d["email"] = user.email
        d["website_url"] = user.website_url
        d["update_tracking_id"] = user.update_tracking_id
        d["update_tracking_status"] = user.update_tracking_status
        d["accounts"] = accounts
        d["display_name"] = platform_type + " - " + user.shop_name
        d["show_display_name"] = True if platform_type == "shopify" else False
        if platform_type == "shopify":
            d["token"] = user.token
        if platform_type == "ecwid":
            d["store_id"] = user.store_id
        res.append(d.copy())
    return res


def get_all_active_platforms_for_user(auth_token):
    try:
        res = []
        ecwid_users = helpers.filter_or_none(
            models.EcwidUserInfo, auth_token=auth_token, is_active=True)
        if ecwid_users:
            prepare_client_store_info(ecwid_users, "ecwid", res)
        eazyslips_users = helpers.filter_or_none(
            models.EazySlipsUserInfo, auth_token=auth_token, is_active=True)
        if eazyslips_users:
            prepare_client_store_info(eazyslips_users, "eazyslips", res)
        magento_users = helpers.filter_or_none(
            models.MagnetoUserInfo, auth_token=auth_token, is_active=True, version="magento_v2")
        if magento_users:
            prepare_client_store_info(magento_users, "magento_v2", res)
        easyecom_users = helpers.filter_or_none(
            models.EasyEcomUserInfo, auth_token=auth_token, is_active=True)
        if easyecom_users:
            prepare_client_store_info(easyecom_users, "easyecom", res)
        jungleworks_users = helpers.filter_or_none(
            models.JungleworksUserInfo, auth_token=auth_token, is_active=True)
        if jungleworks_users:
            prepare_client_store_info(jungleworks_users, "jungleworks", res)
        opencart_users = helpers.filter_or_none(
            models.OpencartUserInfo, auth_token=auth_token, is_active=True)
        if opencart_users:
            prepare_client_store_info(opencart_users, "opencart", res)
        prestashop_users = helpers.filter_or_none(
            models.PrestaShopUserModel, auth_token=auth_token, is_active=True
        )
        if prestashop_users:
            prepare_client_store_info(prestashop_users, 'prestashop', res)
        bikayi_users = helpers.filter_or_none(
            models.BikayiUserInfo, auth_token=auth_token, is_active=True)
        if bikayi_users:
            prepare_client_store_info(bikayi_users, "bikayi", res)
        shopify_users = helpers.filter_or_none(
            models.ShopifyUserModel, auth_token=auth_token, is_active=True)
        if shopify_users:
            prepare_client_store_info(shopify_users, "shopify", res)
        magento1_users = helpers.filter_or_none(
            models.MagnetoUserInfo, auth_token=auth_token, is_active=True, version="magento_v1")
        if magento1_users:
            prepare_client_store_info(magento1_users, "magento_v1", res)
        wix_users = helpers.filter_or_none(
            models.WixUserInfo, auth_token=auth_token, is_active=True)
        if wix_users:
            prepare_client_store_info(wix_users, 'wix', res)
        zoho_users = helpers.filter_or_none(
            models.ZohoUserModel, auth_token=auth_token, is_active=True)
        if zoho_users:
            prepare_client_store_info(zoho_users, 'zoho', res)
        instamojo_users = helpers.filter_or_none(
            InstaMojoUserModel, auth_token=auth_token, is_active=True)
        if instamojo_users:
            prepare_client_store_info(instamojo_users, 'instamojo', res)
        magento_users = helpers.filter_or_none(
            models.MagnetoUserInfo, auth_token=auth_token, is_active=True, version="shoptimize")
        if magento_users:
            prepare_client_store_info(magento_users, "shoptimize", res)
        return res
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e), "other": error}


def update_tracking_id_to_opencart(order_id, tracking_id, tracking_url, pickrr_auth_token,
                                   opencart_user_info_obj=None, access_token=None):
    """
        {
            "key_1":"q{pnP.(fKV0kR(0itN,hS}r)YexDm40d",
            "key_2":"{bW4Km}6APH3]LKr}HXfb7PXoWe2j5(x",
            "order_id": "2",
            "notify": 1,
            "comment": "http://www.pickrr.com/tracking/#/?tracking_id=12151137876151111111111111111111111"
        }
    :return:
    """
    if not opencart_user_info_obj:
        opencart_user_info_obj = models.OpencartUserInfo.objects.get(auth_token=pickrr_auth_token)
    key1 = opencart_user_info_obj.key1
    key2 = opencart_user_info_obj.key2
    note = "<a href=%s>" % tracking_url + tracking_url + "</a>" + "tracking_id=" + str(tracking_id)
    data = {
            "key_1": str(key1),
            "key_2": str(key2),
            "order_id": str(order_id),
            "notify": 1,
            "comment": note
            }
    website_url = opencart_user_info_obj.website_url
    headers = {'content-type': 'application/json'}
    url = website_url + "/index.php?route=extension/module/pickrr/history"
    res = requests.post(url, data=json.dumps(data), headers=headers)
    return res.json()


def pull_orders_from_opencart(opencart_user_info_obj, filters: Dict[str, Any]) -> Dict[str, Any]:
    user_shop_name = opencart_user_info_obj.website_url
    username = opencart_user_info_obj.key1
    password = opencart_user_info_obj.key2
    fetch_url = user_shop_name + "/index.php?route=extension/module/pickrr/orders"
    if "date_from" in filters and filters["date_from"]:
        date_from = filters["date_from"]
    else:
        date_from = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    if "date_to" in filters and filters["date_to"]:
        date_to = filters["date_to"]
        date_to = (datetime.strptime(date_to, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
    else:
        date_to = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')
    payload = {
            'start_date': str(date_from),
            'end_date': str(date_to),
            'key_1': str(username),
            'key_2': str(password)
        }
    headers = {
            'content-type': "application/json",
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
        }
    try:
        response = commonutils.make_post_request(fetch_url, payload, headers)
    except Exception as e:
        logger.exception('Error fetching orders from opencart store : ')
        logger.exception(f'Opencart shop name: {user_shop_name}')

        raise Exception('Unable to fetch orders from opencart store')

    return response


def opencart_to_pickrr_dict(user_obj, auth_token, json_dict):
    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:
        order_dict = {
            "pickrr_dict": "",
            "order_id": str(json_dict['client_order_id']),
            "created_at": str(json_dict['date_added']),
            "platform_order_id": str(json_dict['client_order_id'])
        }
        from_dict = {
            "from_name": str(json_dict['from_name']),
            "from_address": str(json_dict['from_address']),
            "from_pincode": str(json_dict['from_pincode']),
            "from_phone_number": str(json_dict['from_phone_number']),
            "from_email": str(json_dict['from_email'])
        }
        pickrr_dict = from_dict
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['auth_access_token'] = str(user_obj.key1) + " " +str(user_obj.key2)
        pickrr_dict['website_url'] = user_obj.website_url
        pickrr_dict['item_list'] = []
        other_info = []
        items_name = ""
        quantity = 0
        weight = 0.0
        length = 10
        height = 10
        breadth = 10
        for item in json_dict['item_list']:
            item_dict = {}
            item_dict['item_name'] = item['item_name']
            if len(json_dict['item_list']) > 1:
                items_name = items_name + ', ' + item['item_name']
            else:
                items_name = item['item_name']
                length = item['item_length']
                height = item['item_height']
                breadth = item['item_breadth']
            item_dict['sku'] = item['sku']
            item_dict['price'] = item['price']
            item_dict['quantity'] = item['quantity']
            item_dict['weight'] = item['item_weight']
            item_dict['length'] = item['item_length']
            item_dict['breadth'] = item['item_breadth']
            item_dict['height'] = item['item_height']
            quantity += int(item['quantity'])
            weight += float(item['item_weight'])
            pickrr_dict['item_list'].append(item_dict)
            d = {
                "qty": item['quantity']
            }
            other_info.append(d.copy())
        pickrr_dict["other_info"] = other_info
        pickrr_dict['to_name'] = str(str(json_dict['to_name']))
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['to_phone_number'] = str(json_dict['to_phone_number'])
        pickrr_dict['to_pincode'] = str(json_dict['to_pincode'])
        pickrr_dict['to_address'] = str(json_dict['to_address'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_email'] = str(json_dict['to_email'])
        #pickrr_dict['order_time'] = json_dict['date']#unavialable
        pickrr_dict['invoice_number'] = "retail_" + str(json_dict['client_order_id'])
        pickrr_dict['item_weight'] = weight
        pickrr_dict['item_length'] = length
        pickrr_dict['item_height'] = height
        pickrr_dict['item_breadth'] = breadth
        pickrr_dict['item_name'] = items_name
        pickrr_dict['invoice_value'] = json_dict['invoice_value']
        pickrr_dict['client_order_id'] = str(json_dict['client_order_id'])
        pickrr_dict['platform_order_id'] = str(json_dict['client_order_id'])
        cod = 0.0
        if 'cod_amount' in json_dict and json_dict['cod_amount']:
            cod = json_dict['cod_amount']
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict


def prepare_pickrr_orders_from_opencart_orders(user_obj, opencart_response, auth_token):
    try:
        unplaced_orders = []
        placed_orders = []
        opencart_order_ids = [opencart_response[str(order)]['client_order_id'] for order in opencart_response]
        already_placed_order_ids = fetch_duplicate_client_orders(
            opencart_order_ids, auth_token, 'opencart')
        for order in opencart_response:
            opencart_order = opencart_response[str(order)]
            if user_obj.is_multilocation:
                pass
            pickrr_dict = opencart_to_pickrr_dict(user_obj, auth_token, opencart_order)
            if opencart_response[str(order)]['client_order_id'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def get_opencart_orders_service(filters, auth_token, opencart_user_info_obj):
    response = {}
    response['shop_name'] = opencart_user_info_obj.shop_name
    opencart_orders_response = pull_orders_from_opencart(opencart_user_info_obj, deepcopy(filters))
    if 'warning' in opencart_orders_response and opencart_orders_response['warning'] == 'No Orders found for given criteria':
        response["err"] = opencart_orders_response
        return response
    if "error" in opencart_orders_response and opencart_orders_response["error"]:
        response["err"] = opencart_orders_response["error"]
        return response
    pickup_loc_list = []
    if len(opencart_orders_response):
        orders = prepare_pickrr_orders_from_opencart_orders(
            opencart_user_info_obj, opencart_orders_response, auth_token)
        response["pickup_locations"] = pickup_loc_list
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["other"] = opencart_orders_response
    return response


def create_bikayi_signature(map_dict):
    import hmac
    import hashlib
    import base64
    import json
    json_payload = str(json.dumps(map_dict, separators=(',', ':')))
    api_key = constants.PICKRR_BIKAYI_API_KEY
    api_secret = constants.PICKRR_BIKAYI_API_SECRET
    message = bytes(api_key + '|' + base64.b64encode(json_payload.encode()).decode(), 'utf-8')
    signature = hmac.new(
        api_secret.encode(),
        msg=message,
        digestmod=hashlib.sha256
    ).hexdigest()
    return signature


def bikayi_to_pickrr_dict(user_obj, auth_token, json_dict, picrrdict):
    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:
        order_dict = {
            "pickrr_dict": "",
            "order_id": str(json_dict['orderId']),
            "created_at": str(datetime.fromtimestamp(json_dict['date'])),
            "platform_order_id": str(json_dict['orderId'])
        }
        pickrr_dict = picrrdict
        pickrr_dict['auth_token'] = user_obj.merchant_id
        pickrr_dict['item_list'] = []
        items_name = ""
        quantity = 0
        for item in json_dict['items']:
            item_dict = {}
            item_dict['item_name'] = item['name']
            if len(json_dict['items']) > 1:
                items_name = items_name + ', ' + item['name']
            else:
                items_name = item['name']
            item_dict['price'] = item['unitPrice']
            item_dict['sku'] = item['id']
            item_dict['quantity'] = item['quantity']
            quantity += int(item['quantity'])
            pickrr_dict['item_list'].append(item_dict)
        pickrr_dict['to_name'] = str(str(json_dict['customerName']))
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['to_phone_number'] = str(json_dict['customerPhone'])
        pickrr_dict['to_pincode'] = str(json_dict['customerAddress']['pinCode'])
        pickrr_dict['to_address'] = str(json_dict['customerAddress']['address'] + str(" ") + json_dict['customerAddress']['city'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['order_time'] = str(datetime.fromtimestamp(json_dict['date']))
        pickrr_dict['invoice_number'] = "retail_" + str(json_dict['orderId'])
        pickrr_dict['item_weight'] = 0.5
        pickrr_dict['item_length'] = 10
        pickrr_dict['item_height'] = 10
        pickrr_dict['item_breadth'] = 10
        pickrr_dict['item_name'] = items_name
        pickrr_dict['invoice_value'] = json_dict['total']
        pickrr_dict['client_order_id'] = str(json_dict['orderId'])
        pickrr_dict['platform_order_id'] = str(json_dict['orderId'])
        #pickrr_dict['auth_access_token'] = auth_access_token #merchant id
        cod = 0.0
        if json_dict['paymentMethod'] == 'cod':
            cod = json_dict['total']
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict.copy()
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict


def get_merchant_info(merchant_id):
    import collections
    url = "https://asia-south1-bikai-d5ee5.cloudfunctions.net/platformPartnerFunctions-fetchMerchant"
    map_dict = collections.OrderedDict()
    map_dict['appId'] = constants.PICKRR_BIKAYI_APPID
    map_dict['merchantId'] = str(merchant_id)
    signature = create_bikayi_signature(map_dict)
    pickup_response = requests.post(
        url, data=json.dumps(map_dict),
        headers={'content-type': 'application/json',
                 "Authorization": signature}
    )
    response = json.loads(pickup_response.text)
    return response


def pickup_location_from_merchantId_bikayi(user_obj):
    try:
        response = get_merchant_info(user_obj.merchant_id)
        if response['status'] == 200:
            pickup_address = response['merchant']['location']
            from_dict = {
                "from_name": str(response['merchant']['name']),
                "from_address": str(response['merchant']['name'] + str(" ") +
                                pickup_address['address'] + str(" ") +
                                pickup_address['city'] + str(" ") +
                                str(pickup_address['pinCode']) + str(" ") +
                                pickup_address['state'] + str(" ") +
                                pickup_address['country']),#to give full address
                "from_pincode": str(pickup_address['pinCode']),
                "from_phone_number": str(response['merchant']['phoneNumber']),
                "from_email": str(response['merchant']['email'])
            }
            return from_dict
        else:
            return {"err": "Pickup address not found for given merchantId"}
    except Exception as e:
        return {"err": str(e)}


def prepare_pickrr_orders_from_bikayi_orders(user_obj, bikayi_response, auth_token):
    try:
        unplaced_orders = []
        placed_orders = []
        bikayi_order_ids = [order['orderId'] for order in bikayi_response]
        already_placed_order_ids = fetch_duplicate_client_orders(
            bikayi_order_ids, auth_token, 'bikayi')
        pickrrdict = pickup_location_from_merchantId_bikayi(user_obj)
        for order in bikayi_response:
            if user_obj.is_multilocation:
                pass
            pickrr_dict = bikayi_to_pickrr_dict(user_obj, auth_token, order, pickrrdict)
            if order['orderId'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def pull_orders_from_bikayi(bikayi_user_info_obj, start_date, epoch_time=None):
    import collections
    user_shop_name = bikayi_user_info_obj.shop_name
    fetch_url = "https://asia-south1-bikai-d5ee5.cloudfunctions.net/platformPartnerFunctions-fetchOrders"
    map_dict = collections.OrderedDict()
    if not epoch_time:
        if start_date:
            start_date = datetime.strptime(str(start_date), "%Y-%m-%d")
        else:
            start_date = datetime.now() - timedelta(days=1)
        start_date = int(time.mktime(start_date.timetuple()))
    else:
        start_date = str(epoch_time)
    map_dict['appId'] = constants.PICKRR_BIKAYI_APPID
    map_dict['merchantId'] = str(bikayi_user_info_obj.merchant_id)
    map_dict['timestamp'] = str(start_date)
    signature = create_bikayi_signature(map_dict)
    #payload = {"appId":"PICKRR","merchantId":str(bikayi_user_info_obj.merchant_id),"timestamp": "1604224800"}
    headers = {
            "Content-Type": "application/json",
            "Authorization": str(signature)
        }
    try:
        response = commonutils.make_post_request(fetch_url, map_dict, headers)
    except Exception as e:
        logger.exception('Error fetching orders from bikayi store : ')
        logger.exception(f'bikayi shop name: {user_shop_name}')

        raise Exception('Unable to fetch orders from bikayi store')

    return response


def get_bikayi_orders_service(filters, auth_token, bikayi_user_info_obj):
    response = {}
    response['shop_name'] = bikayi_user_info_obj.shop_name
    start_date = filters['date_from']
    bikayi_orders_response = pull_orders_from_bikayi(bikayi_user_info_obj, start_date)
    if 'status' in bikayi_orders_response and bikayi_orders_response['status'] == 200:
        if len(bikayi_orders_response['orders']):
            total_order = bikayi_orders_response['orders']
            if len(bikayi_orders_response['orders']) >= 10:
                while True:
                    order_time = bikayi_orders_response['orders'][-1]['date']
                    # order_time = time.strftime('%Y-%m-%d', time.localtime(int(order_time)))
                    bikayi_orders_response = pull_orders_from_bikayi(bikayi_user_info_obj, "", order_time)
                    if 'status' in bikayi_orders_response and bikayi_orders_response['status'] == 200:
                        total_order += bikayi_orders_response['orders']
                    else:
                        break
                    if len(bikayi_orders_response['orders']) < 10:
                        break
                # else:
                #     response["err"] = "No order found"
            if len(total_order):
                orders = prepare_pickrr_orders_from_bikayi_orders(
                    bikayi_user_info_obj, total_order, auth_token)
                response.update({'orders': orders})
        else:
            response["err"] = "No order found"
    else:
        response["other"] = bikayi_orders_response
    return response


def update_details_to_pickrr(url, pickrr_data):
    try:
        from utils.helpers import make_post_request
        return make_post_request(url, pickrr_data)
    except Exception as e:
        return {"err": str(e)}


class PrestaShopServiceBase:
    def __init__(self, user_obj):
        self.base_params = {
            'ws_key': user_obj.shop_token,
            'display': 'full'
        }
        self.base_headers = {'Output-Format': 'JSON'}
        self.user_obj = user_obj
        self.client_order_status_mapping = {
            'OP': self.user_obj.order_placed,
            'RTD': self.user_obj.rtd,
            'RTO': self.user_obj.rto,
            'DL': self.user_obj.delivered,
            'OT': self.user_obj.transit,
            'PP': self.user_obj.picked_up,
            'OC': self.user_obj.cancelled
        }

    def pull_orders(self, params: dict) -> list:
        """
        Pull orders from prestashop for provided filters
        :param params: filters
        :return: response list
        """
        url = self.user_obj.website_url + constants.PrestaShopEndpoints.ORDERS.value

        date_from, date_to = params.pop('date_from', None), params.pop('date_to', None)
        if date_from and date_to:
            date_filter = f'filter[date_add]=[{",".join([date_from, date_to])}]'
            url += '?' + date_filter

        qp = deepcopy(self.base_params)
        qp.update(params)
        response = commonutils.CustomRequestBase.make_request(
            'GET', url, headers=self.base_headers, query_params=qp
        )

        if type(response) == dict:
            response = response.get('orders', [])
        if type(response) == str:
            raise Exception(response)

        return response

    def pull_address_details(self, address_ids: List[str]):
        """
        Pull address details from prestashop for provided filters
        :param address_ids: list of address ids
        :return: response list
        """
        if type(address_ids) != list:
            address_ids = [address_ids]

        url = self.user_obj.website_url + constants.PrestaShopEndpoints.ADDRESS.value
        url += f'?filter[id]=[{"|".join(address_ids)}]'

        response = commonutils.CustomRequestBase.make_request(
            'GET', url, headers=self.base_headers, query_params=self.base_params
        )

        if type(response) == dict:
            response = response.get('addresses', [])

        return response

    def pull_product_details(self, product_ids: List[str]):
        """
        Pull product details from prestashop for provided filters
        :param product_ids: list of product ids
        :return: response list
        """
        if type(product_ids) != list:
            product_ids = [product_ids]

        url = self.user_obj.website_url + constants.PrestaShopEndpoints.PRODUCTS.value
        url += f'?filter[id]=[{"|".join(product_ids)}]'

        response = commonutils.CustomRequestBase.make_request(
            'GET', url, headers=self.base_headers, query_params=self.base_params
        )

        if type(response) == dict:
            response = response.get('products', [])

        return response

    def pull_order_carrier_details(self, order_ids: List[str], carrier_ids: List[str]):
        """
        Pull order carrier details from prestashop for provided filters
        """

        url = self.user_obj.website_url + constants.PrestaShopEndpoints.ORDER_CARRIERS.value
        url += f'?filter[id_order]=[{"|".join(order_ids)}]&filter[id_carrier]=[{"|".join(carrier_ids)}]'
        response = commonutils.CustomRequestBase.make_request(
            'GET', url, headers=self.base_headers, query_params=self.base_params
        )

        if type(response) == dict:
            response = response.get('order_carriers', [])

        return response


class PrestaShopOrderFetchService(PrestaShopServiceBase):
    """
    Service class to handle all services regarding order fetching for prestashop
    """
    def prepare_pickrr_orders(self, prestashop_order_response, existing_order_ids: set, address_id_to_detail_map,
                              product_id_to_detail_map):
        orders, error = {}, {}

        try:
            unplaced_orders = []
            placed_orders = []
            pickup_addresses = PrestaShopModelDao.fetch_addresses(self.user_obj.auth_token)
            pickup_address = model_to_dict(pickup_addresses[0]) if pickup_addresses else {}

            for order in prestashop_order_response:
                # check for user multi-location case later, not required now
                address = address_id_to_detail_map.get(order.get('id_address_delivery'))
                pickrr_dict = self.prestashop_to_pickrr_dict(order, address, product_id_to_detail_map, pickup_address)
                if order['id'] in existing_order_ids:
                    placed_orders.append(pickrr_dict)
                else:
                    unplaced_orders.append(pickrr_dict)

            orders = {
                'uorders': unplaced_orders,
                'porders': placed_orders
            }
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
            error = {'err': str(e), 'other': error}

        return orders, error

    def fetch_orders(self, params):
        response = {
            'shop_name': self.user_obj.shop_name,
            'err': None,
            'pickup_locations': [],
            'orders': None,
            'other': None
        }

        prestashop_order_response = self.pull_orders(params)
        if not prestashop_order_response:
            response.update({
                'err': 'No orders found',
                'other': prestashop_order_response
            })
            return response

        unique_order_ids = {order["id"] for order in prestashop_order_response}
        existing_order_ids = fetch_duplicate_client_orders(list(unique_order_ids), self.user_obj.auth_token,
                                                           'prestashop')

        unique_address_ids = {order.get('id_address_delivery') for order in prestashop_order_response}
        address_details = self.pull_address_details(list(unique_address_ids))
        address_id_to_detail_map = {str(address_detail['id']): address_detail for address_detail in address_details}

        unique_product_ids = set()
        for order in prestashop_order_response:
            products = order.get('associations', {}).get('order_rows', [])
            unique_product_ids = unique_product_ids.union({product['product_id'] for product in products})

        product_details = self.pull_product_details(list(unique_product_ids))
        product_id_to_detail_map = {str(product_detail['id']): product_detail for product_detail in product_details}

        orders, error = self.prepare_pickrr_orders(prestashop_order_response, set(existing_order_ids),
                                                   address_id_to_detail_map, product_id_to_detail_map)

        if not error:
            error = None
        response.update({
            'err': error,
            'orders': orders
        })

        return response

    def prestashop_to_pickrr_dict(self, order, address_details, product_id_to_detail_map, pickup_address):
        order_dict = constants.PickrrOrderDict()
        pickrr_dict = dict()

        order_dict['order_id'] = order['id']
        order_dict['created_at'] = order.get('date_add', '')
        order_dict['platform_order_id'] = order['id']
        order_dict['pickrr_dict'] = pickrr_dict

        pickrr_dict['auth_token'] = self.user_obj.auth_token
        pickrr_dict['auth_access_token'] = self.user_obj.auth_token
        pickrr_dict['website_url'] = self.user_obj.website_url

        pickrr_dict['from_name'] = pickup_address.get('name', '')
        pickrr_dict['from_address'] = pickup_address.get('address_line', '')
        pickrr_dict['from_pincode'] = pickup_address.get('pin_code', '')
        pickrr_dict['from_phone_number'] = pickup_address.get('phone_number', '')
        pickrr_dict['from_email'] = pickup_address.get('email', '')

        pickrr_dict['to_name'] = f'{address_details.get("firstname", "")} {address_details.get("lastname", "")}'
        pickrr_dict['to_address'] = f'{address_details.get("address1", "")} {address_details.get("address2", "")} ' \
                                    f'{address_details.get("city", "")} '
        pickrr_dict['to_pincode'] = address_details.get('postcode', "NA")
        pickrr_dict['to_phone_number'] = address_details.get('phone') or address_details.get('phone_mobile')
        pickrr_dict['to_email'] = ''

        pickrr_dict['item_list'] = []
        pickrr_dict['items_name'] = []  # to be converted to string later
        pickrr_dict['quantity'] = 0
        pickrr_dict['item_length'] = 0
        pickrr_dict['item_height'] = 0
        pickrr_dict['item_breadth'] = 0
        pickrr_dict['item_weight'] = 0
        pickrr_dict['other_info'] = []
        for item in order.get('associations', {}).get('order_rows', []):
            pickrr_dict['items_name'].append(item.get('product_name', ''))
            item_dict = {
                'item_name': item.get('product_name'),
                'sku': item.get('product_reference'),
                'price': float(item.get('product_price', 0.0)),
                'quantity': int(item.get('product_quantity', 0))
            }
            pickrr_dict['quantity'] += int(item.get('product_quantity', 0))
            pickrr_dict['item_list'].append(item_dict)
            pickrr_dict['other_info'].append({
                'qty': int(item.get('product_quantity', 0))
            })

            product_detail = product_id_to_detail_map.get(item['product_id'], {})
            pickrr_dict['item_length'] += float(product_detail.get('depth', 0.0))
            pickrr_dict['item_height'] += float(product_detail.get('height', 0.0))
            pickrr_dict['item_breadth'] += float(product_detail.get('width', 0.0))
            pickrr_dict['item_weight'] += float(product_detail.get('weight', 0.0))

        pickrr_dict['item_length'] = 10 if pickrr_dict['item_length'] < 10 else pickrr_dict['item_length']
        pickrr_dict['item_height'] = 10 if pickrr_dict['item_height'] < 10 else pickrr_dict['item_height']
        pickrr_dict['item_breadth'] = 10 if pickrr_dict['item_breadth'] < 10 else pickrr_dict['item_breadth']
        pickrr_dict['item_weight'] = 0.5 if pickrr_dict['item_weight'] < 0.5 else pickrr_dict['item_weight']

        pickrr_dict['items_name'] = ', '.join(pickrr_dict['items_name'])

        pickrr_dict['invoice_number'] = order.get('invoice_number', '0')
        pickrr_dict['invoice_value'] = float(order.get('total_paid'))
        pickrr_dict['cod_amount'] = float(order.get('total_paid')) if \
            constants.PrestaShopPaymentModules.CHECK_PAYMENT.value == order.get('module') else 0

        pickrr_dict['client_order_id'] = order['id']
        pickrr_dict['platform_order_id'] = order['id']
        pickrr_dict['carrier_id'] = order.get('id_carrier')
        pickrr_dict['store_name'] = self.user_obj.shop_name

        return order_dict


class PrestaShopOrderUpdateService(PrestaShopServiceBase):
    """
    Service class to handle all services regarding order details update for prestashop
    """

    def __init__(self, user_obj, filters, payload):
        self.filters = filters
        self.payload = payload
        super().__init__(user_obj)

    def prepare_order_update_payload(self, order_data):
        order_update_payload = deepcopy(order_data)

        order_status = self.payload.pop('status', None)
        if order_status and self.client_order_status_mapping.get(order_status):
            order_update_payload['current_state'] = self.client_order_status_mapping[order_status]

        order_update_payload.update(self.payload)

        return dicttoxml.dicttoxml({
            'prestashop': order_update_payload
        })

    def update_orders(self):
        """
        Update order entity which includes status, price etc.
        """

        params = {
            'filter[id]': self.filters['order_id']
        }
        order_data = self.pull_orders(params)
        if not order_data:
            raise InvalidRequestException(
                PrestaShopErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Order not found for this request!'
                }
            )
        order_data = order_data[0]
        order_update_payload = self.prepare_order_update_payload(order_data)

        url = self.user_obj.website_url + constants.PrestaShopEndpoints.ORDERS.value
        response = commonutils.CustomRequestBase.make_request(
            'PUT', url, query_params=self.base_params, payload=order_update_payload
        )
        response = xmltodict.parse(response)

        return response


class PrestaShopOrderCarrierUpdateService(PrestaShopServiceBase):
    """
    Service class to handle all services regarding order carrier details update for prestashop
    """

    def __init__(self, user_obj, filters, payload):
        self.filters = filters
        self.payload = payload
        super().__init__(user_obj)

    def prepare_order_carrier_update_payload(self, order_carrier_data):
        order_carrier_update_payload = deepcopy(order_carrier_data)

        tracking_id = self.payload.pop('tracking_id', None)
        if tracking_id:
            order_carrier_update_payload['tracking_number'] = tracking_id

        order_carrier_update_payload.update(self.payload)

        return dicttoxml.dicttoxml({
            'prestashop': order_carrier_update_payload
        })

    def update_order_carrier(self):
        order_carrier_data = self.pull_order_carrier_details(self.filters.get('order_id'),
                                                             self.filters.get('carrier_id'))
        if not order_carrier_data:
            raise InvalidRequestException(
                PrestaShopErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Order carrier not found for this request!'
                }
            )

        order_carrier_data = order_carrier_data[0]
        order_carrier_update_payload = self.prepare_order_carrier_update_payload(order_carrier_data)

        url = self.user_obj.website_url + constants.PrestaShopEndpoints.ORDER_CARRIERS.value
        response = commonutils.CustomRequestBase.make_request(
            'PUT', url, query_params=self.base_params, payload=order_carrier_update_payload
        )
        response = xmltodict.parse(response)

        return response

def pull_orders_from_magento1(magento1_user_info_obj, filters: Dict[str, Any]) -> Dict[str, Any]:
    user_shop_name = magento1_user_info_obj.shop_name
    website_url = magento1_user_info_obj.website_url
    username = magento1_user_info_obj.username
    password = magento1_user_info_obj.password
    magento1_conn = magento.API(website_url, username, password)
    if "from" in filters and filters["from"]:
        date_from = filters["from"]
    else:
        date_from = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    if "to" in filters and filters["to"]:
        date_to = filters["to"]
        date_to = (datetime.strptime(date_to, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
    else:
        date_to = (datetime.now() + timedelta(days=7)).strftime('%Y-%m-%d')
    order_filter = {'created_at':{'from':date_from, 'to':date_to}}
    for key in filters.keys():
        if key == 'from' or key == 'to':
            continue
        else:
            order_filter[key] = filters[key]
    try:
        order_increment_ids = [x['increment_id'] for x in magento1_conn.order.list(order_filter)]
        response = magento1_conn.order.info_multi(order_increment_ids)
    except Exception as e:
        logger.exception('Error fetching orders from magento1 store : ')
        logger.exception(f'Magento1 shop name: {user_shop_name}')

        raise Exception('Unable to fetch orders from magento1 store')

    return response


def get_magento1_orders_service(filters, auth_token, magento1_user_info_obj):
    response = {}
    response['shop_name'] = magento1_user_info_obj.shop_name
    response['auth_access_token'] = magento1_user_info_obj.auth_token
    magento1_orders_response = pull_orders_from_magento1(magento1_user_info_obj, deepcopy(filters))
    if type(magento1_orders_response) == list and len(magento1_orders_response):
        pickup_loc_list = get_clients_pickup_locations_by_type(auth_token, "magento1")
        orders = prepare_pickrr_orders_from_magneto1_orders(
            magento1_user_info_obj, magento1_orders_response, auth_token, pickup_loc_list)
        response["pickup_locations"] = pickup_loc_list
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["other"] = magento1_orders_response
    return response

def prepare_pickrr_orders_from_magneto1_orders(user_obj, magento_response, auth_token, pickup_loc_list):
    try:
        unplaced_orders = []
        placed_orders = []
        orders = magento_response
        magento1_order_ids = [order['increment_id'] for order in orders]
        already_placed_order_ids = fetch_duplicate_client_orders(
            magento1_order_ids, auth_token, 'magento_v1')
        auth_access_token = auth_token
        for order in magento_response:
            if user_obj.is_multilocation:
                pass
            pickrr_dict = magento1_to_pickrr_dict(user_obj, auth_token, order, pickup_loc_list)
            if order['increment_id'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}

def magento1_to_pickrr_dict(user_obj, auth_token, json_dict, pickup_loc_list):
    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:
        order_dict = {
            "pickrr_dict": "",
            "order_id": str(json_dict['increment_id']),
            "created_at": (
                datetime.strptime(json_dict['created_at'], '%Y-%m-%d %H:%M:%S')).strftime(
                '%Y-%m-%d  %H:%M:%S'),
            "platform_order_id": str(json_dict['increment_id'])
        }
        pickrr_dict = pickup_loc_list[0].copy()
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['auth_access_token'] = auth_token
        pickrr_dict['website_url'] = user_obj.website_url
        pickrr_dict['item_list'] = []
        other_info = []
        items_name = ""
        quantity = 0
        for item in json_dict['items']:
            item_dict = {}
            item_dict['item_name'] = item['name']
            if len(items_name) > 1:
                items_name = items_name + ', ' + (item['name'])
            else:
                items_name = item['name']
            item_dict['sku'] = item['sku']
            item_dict['price'] = item['price_incl_tax']
            item_dict['quantity'] = item['qty_ordered']
            quantity += int(float(item['qty_ordered']))
            pickrr_dict['item_list'].append(item_dict)
            d = {
                "order_item_id": item['item_id'],
                "qty": item['qty_ordered']
            }
            other_info.append(d.copy())
        pickrr_dict["other_info"] = other_info
        try:
            address = json_dict['shipping_address']
        except:
            address = json_dict['billing_address']
        pickrr_dict['to_name'] = address['firstname'] + " " + address['lastname']
        pickrr_dict['to_email'] = address['email']
        pickrr_dict['to_phone_number'] = str(address['telephone'])
        pickrr_dict['to_pincode'] = str(address['postcode'])
        street = address["street"]
        pickrr_dict['to_address'] = str(street + str(" ") +
                                        address['city'] + str(" ") +
                                        address['region'] + str(" ") +
                                        address['country_id'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['order_time'] = (
                datetime.strptime(json_dict['created_at'], '%Y-%m-%d %H:%M:%S')).strftime(
            '%Y-%m-%d  %H:%M:%S')
        pickrr_dict['item_weight'] = 0.5
        pickrr_dict['item_length'] = 10
        pickrr_dict['item_height'] = 10
        pickrr_dict['item_breadth'] = 10
        pickrr_dict['item_name'] = items_name
        pickrr_dict['client_order_id'] = str(json_dict['increment_id'])
        pickrr_dict['platform_order_id'] = str(json_dict['order_id'])
        pickrr_dict['total_discount'] = json_dict['discount_amount']
        pickrr_dict['shipping_charge'] = json_dict['shipping_incl_tax']
        pickrr_dict['invoice_number'] = "retail_" + str(json_dict['increment_id'])
        pickrr_dict['invoice_value'] = json_dict['grand_total']
        cod = 0.0
        try:
            if json_dict['payment']["additional_information"][0].lower() == "cash on delivery":
                cod = json_dict['payment']['amount_ordered']
        except:
            pass
        if 'cashondelivery' in json_dict['payment']["method"].lower():
            cod = json_dict['payment']['amount_ordered']
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict

def update_order_status_to_magento1(filters, auth_token, magento1_user_info_obj):
    user_shop_name = magento1_user_info_obj.shop_name
    website_url = magento1_user_info_obj.website_url
    username = magento1_user_info_obj.username
    password = magento1_user_info_obj.password
    magento1_conn = magento.API(website_url, username, password)
    order_increment_id = filters['order_id']
    order_status = filters['order_status']
    try:
        magento1_conn.order.addcomment(order_increment_id, order_status, comment=None, notify=False)
        response = magento1_conn.order.info(order_increment_id)
    except Exception as e:
        logger.exception('Error updating the status of order : ')
        logger.exception(f'Magento1 shop name: {user_shop_name}')

        raise Exception('Unable to update the status of order')

    return response

def update_tracking_id_to_magento1(filters, auth_token, magento1_user_info_obj):
    user_shop_name = magento1_user_info_obj.shop_name
    website_url = magento1_user_info_obj.website_url
    username = magento1_user_info_obj.username
    password = magento1_user_info_obj.password
    magento1_conn = magento.API(website_url, username, password)
    order_increment_id = filters['order_id']
    order_id = magento1_conn.order.info(order_increment_id)['order_id']
    shipment_id = magento1_conn.shipment.list({'order_id':order_id})[0]['increment_id']
    tracking_title = "Pickrr Tracking"
    try:
        magento1_conn.shipment.addtrack(shipment_id, 'custom', tracking_title, filters['tracking_id'])
        response = magento1_conn.shipment.info(shipment_id)
    except Exception as e:
        logger.exception('Error updating the tracking id : ')
        logger.exception(f'Magento1 shop name: {user_shop_name}')

        raise Exception('Unable to update the tracking id')

    return response

class WooCommercePickrrService:
    FETCH_USER_DETAILS_FROM_PICKRR_URL = settings.PICKRR_BASE_URL + constants.WooCommercePickrrConstants.FETCH_CLIENTS.value

    @staticmethod
    def fetch_user_data_from_pickrr(auth_token):
        qp = {'auth_token': auth_token}

        response = commonutils.CustomRequestBase.make_request(
            'GET', WooCommercePickrrService.FETCH_USER_DETAILS_FROM_PICKRR_URL, query_params=qp
        )
        return response


class CommonServices:
    """
    CommonServices class includes methods that are generic and should be usable by other services also.
    """

    @staticmethod
    def get_tag(status: str, user_obj) -> str:
        """
        status: Order status code at pickrr
        user_obj: user object from plugin user model

        Returns status of the order set by the user corresponding to the order status at pickrr
        """
        tag = None
        if status == 'OP' and user_obj.order_placed:
            tag = user_obj.order_placed
        if status == 'PP' and user_obj.picked_up:
            tag = user_obj.picked_up
        if status == 'OT' and user_obj.transit:
            tag = user_obj.picked_up
        if status == 'RTO' and user_obj.rto:
            tag = user_obj.rto
        if status == 'OC' and user_obj.cancelled:
            tag = user_obj.cancelled
        if status == 'DL' and user_obj.delivered:
            tag = user_obj.delivered
        if status == 'RTD' and user_obj.rtd:
            tag = user_obj.rtd

        return tag


class WixServiceBase:
    def __init__(self, user_obj):
        self.base_headers = {'Content-Type': 'application/json'}
        self.user_obj = user_obj

    def pull_orders(self, params: dict) -> list:
        """
        Pull orders from Wix for provided filters
        parameters:- params: filters
        return:- response: list
        """

        url, payload = constants.WixEndpoints.ORDERS.value, {
            'query': {}
        }

        date_from, date_to = params.pop('date_from', None), params.pop('date_to', None)
        if date_from and date_to:
            payload['query']['filter'] = '{'\
                            '"$and": ['\
                                '{"dateCreated": {"$gte": ' + f'"{date_from}T00:00:00.000Z"' + '}},'\
                                '{"dateCreated": {"$lte": ' + f'"{date_to}T00:00:00.000Z"' + '}}'\
                            ']'\
                        '}'

        self.base_headers.update({
            'Authorization': WixAuthenticationService().get_access_token_from_refresh_token(self.user_obj)
        })

        limit = constants.WixConstants.WIX_ORDERS_PAGE_LIMIT.value  # no. of orders to fetch per request
        offset = params.pop('offset')  # no. of orders to skip from start

        payload['query']['paging'] = {
            'limit': limit,
            'offset': offset
        }

        response = commonutils.CustomRequestBase.make_request(
            'POST', url, headers=self.base_headers, payload=json.dumps(payload))

        if type(response) == dict:
            response = response.get('orders', [])

        return response

    def get_order(self, order_id):
        """
        Fetches order with order id = order_id from Wix
        """

        url = constants.WixEndpoints.ORDER.value.format(orderId=order_id)
        self.base_headers.update({
            'Authorization': WixAuthenticationService().get_access_token_from_refresh_token(self.user_obj)
        })

        response = commonutils.CustomRequestBase.make_request(
            'GET', url, headers=self.base_headers)

        return response['order']


class WixOrderFetchService(WixServiceBase):
    """
    Service class to order fetching for Wix
    """
    def prepare_pickrr_orders(self, wix_order_response, existing_order_ids: set):
        orders, error = {}, {}

        try:
            unplaced_orders = []
            placed_orders = []
            pickup_addresses = WixModelDao.fetch_addresses(self.user_obj.auth_token)
            pickup_address = pickup_addresses[0] if pickup_addresses else {}

            for order in wix_order_response:
                address_details = order.get('shippingInfo', {}).get('shipmentDetails', {}).get('address', {})
                pickrr_dict = self.wix_to_pickrr_dict(order, address_details, pickup_address)
                if order['id'] in existing_order_ids:
                    placed_orders.append(pickrr_dict)
                else:
                    unplaced_orders.append(pickrr_dict)

            orders = {
                'uorders': unplaced_orders,
                'porders': placed_orders
            }
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
            error = {'err': str(e), 'other': error}

        return orders, error

    def fetch_orders(self, params):
        response = {
            'shop_name': self.user_obj.shop_name,
            'err': None,
            'pickup_locations': [],
            'orders': None,
            'other': None
        }

        wix_order_response = self.pull_orders(params)

        if not wix_order_response:
            response.update({
                'err': 'No orders found',
                'other': wix_order_response
            })
            return

        unique_order_ids = {order.get('id') for order in wix_order_response}
        existing_order_ids = fetch_duplicate_client_orders(list(unique_order_ids), self.user_obj.auth_token,
                                                           'wix')

        orders, error = self.prepare_pickrr_orders(wix_order_response, set(existing_order_ids))

        response.update({
            'err': error,
            'orders': orders
        })

        return response

    def wix_to_pickrr_dict(self, order, address_details, pickup_address):
        order_dict = constants.PickrrOrderDict()
        pickrr_dict = dict()

        order_dict['order_id'] = order['id']
        order_dict['created_at'] = order.get('dateCreated', '')
        order_dict['platform_order_id'] = order['number']
        order_dict['pickrr_dict'] = pickrr_dict

        pickrr_dict['auth_token'] = self.user_obj.auth_token
        pickrr_dict['auth_access_token'] = self.user_obj.auth_token
        pickrr_dict['website_url'] = self.user_obj.website_url

        pickrr_dict['from_name'] = pickup_address.name
        pickrr_dict['from_address'] = pickup_address.address_line
        pickrr_dict['from_pincode'] = pickup_address.pin_code
        pickrr_dict['from_phone_number'] = pickup_address.phone_number
        pickrr_dict['from_email'] = pickup_address.email

        pickrr_dict['to_name'] = f"{address_details.get('fullName', {}).get('firstName', '')} \
                                    {address_details.get('fullName', {}).get('lastName', '')}"
        pickrr_dict['to_address'] = address_details.get('addressLine1', '')
        pickrr_dict['to_pincode'] = address_details.get('zipCode', 'NA')
        pickrr_dict['to_phone_number'] = address_details.get('phone')
        pickrr_dict['to_email'] = address_details.get('email')

        pickrr_dict['item_list'] = []
        pickrr_dict['items_name'] = []
        pickrr_dict['quantity'] = 0
        pickrr_dict['other_info'] = []
        for item in order.get('lineItems', []):
            pickrr_dict['items_name'].append(item.get('product_name', ''))
            item_dict = {
                'item_name': item.get('name', ''),
                'sku': item.get('sku'),
                'price': float(item.get('price', 0.0)),
                'quantity': int(item.get('quantity', 0))
            }
            pickrr_dict['quantity'] += int(item.get('quantity', 0))
            pickrr_dict['item_list'].append(item_dict)
            pickrr_dict['other_info'].append({
                'qty': int(item.get('quantity', 0))
            })

        pickrr_dict['item_length'] = 10
        pickrr_dict['item_height'] = 10
        pickrr_dict['item_breadth'] = 10
        pickrr_dict['item_weight'] = 0.5

        pickrr_dict['items_name'] = ', '.join(pickrr_dict['items_name'])

        pickrr_dict['invoice_number'] = 0  # invoice number not present at wix side
        pickrr_dict['invoice_value'] = float(order.get('totals', {}).get('total', 0.0))
        pickrr_dict['cod_amount'] = float(pickrr_dict['invoice_value']) if \
            constants.WixPaymentMethods.COD.value == order.get('billingInfo', {}).get('paymentMethod') else 0

        pickrr_dict['client_order_id'] = order['number']  # This number is unique at user level
        pickrr_dict['platform_order_id'] = order['id']

        return order_dict


class WixOrderFulfillmentService(WixServiceBase):
    """
    service class to create, update order fulfillment on Wix
    """

    def create_fulfillment(self, data, order):
        """
        creates single fulfillment for all the items in the order
        payload: dict
        eg,
        {
            "fulfillment": {
              "lineItems": [
                {
                  "index": 1,
                  "quantity": 1
                }
              ],
              "trackingInfo": {
                "shippingProvider": "fedex",
                "trackingNumber": "1234",
                "trackingLink": "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=1234"
              }
            }
        }
        """

        from .preparator import wix_create_fulfillment_payload

        if order.get('fulfillments'):
            return self.update_fulfillment(data)

        url = constants.WixEndpoints.CREATE_FULFILLMENT.value.format(orderId=data['order_id'])
        self.base_headers.update({
            'Authorization': WixAuthenticationService().get_access_token_from_refresh_token(self.user_obj)
        })
        payload = wix_create_fulfillment_payload(data, order)

        return commonutils.CustomRequestBase.make_request(
            'POST', url, headers=self.base_headers, payload=json.dumps(payload)
        )

    def update_fulfillment(self, data, order):
        """
        updates the fulfillment for a order.
        payload: dict
        eg,
        {
            "fulfillmentTrackingInfo": {
                "shippingProvider": "fedex",
                "trackingNumber": "123",
                "trackingLink": "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=1234"
            }
        }
        """

        from .preparator import wix_update_fulfillment_payload

        if not order.get('fulfillments'):
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{'message': 'fulfillment doens\'t exist'}
            )

        fulfillment_id = order['fulfillments'][0]['id']
        url = constants.WixEndpoints.UPDATE_FULFILLMENT.value.format(
            orderId=data['order_id'], fulfillmentId=fulfillment_id
        )
        self.base_headers.update({
            'Authorization': WixAuthenticationService().get_access_token_from_refresh_token(self.user_obj)
        })
        payload = wix_update_fulfillment_payload(data)

        return commonutils.CustomRequestBase.make_request(
            'PUT', url, headers=self.base_headers, payload=json.dumps(payload)
        )


class WixAuthenticationService:
    def __init__(self):
        self.headers = {'Content-Type': 'application/json'}

    def get_access_token(self, auth_code: str, user_obj):
        url = constants.WixEndpoints.AUTH.value

        payload = {
            'grant_type': 'authorization_code',
            'client_id': constants.WixConstants.WIX_APP_ID.value,
            'client_secret': constants.WixConstants.WIX_APP_SECRET.value,
            'code': auth_code
        }

        return self.get_token(user_obj, url, payload)

    def get_access_token_from_refresh_token(self, user_obj):
        refresh_token = user_obj.api_refresh_token
        url = constants.WixEndpoints.AUTH.value

        payload = {
            'grant_type': 'refresh_token',
            'client_id': constants.WixConstants.WIX_APP_ID.value,
            'client_secret': constants.WixConstants.WIX_APP_SECRET.value,
            'refresh_token': refresh_token,
        }

        return self.get_token(user_obj, url, payload)['access_token']

    def get_token(self, user_obj, url: str, payload: dict):
        response = commonutils.CustomRequestBase.make_request(
            'POST', url, headers=self.headers, payload=json.dumps(payload)
        )

        user_obj.api_refresh_token = response['refresh_token']

        try:
            user_obj.save()
        except Exception as e:
            wix_logger.exception(f'error while saving refresh_token: {e}')

        if payload['grant_type'] == 'authorization_code':
            url = constants.WixEndpoints.COMPLETE_AUTHENTICATION.value.format(access_token=response['access_token'])
            requests.get(url)  # send request at wix marking completion of auth flow

        return response

    def authorize(self, auth_token: str, shop_name: str):
        """
        auth_token: pickrr auth token of the user.
        shop_name: shop name of the user.

        Authorizes wix app for a user
        """
        query_params = {
            'appId': constants.WixConstants.WIX_APP_ID.value,
            'state': auth_token + ':' + shop_name,
            # This redirect url should be present in the wix app's redirect urls part
            'redirectUrl': 'https://cfapi.pickrr.com' + reverse('wix_user_access_token'),
        }

        url = constants.WixEndpoints.AUTHORIZE_APP.value + '?' + urlencode(query_params, safe='')
        return redirect(url)


def zoho_access_token(zoho_user_info_obj):
    payload = {
        "refresh_token" : zoho_user_info_obj.refresh_token,
        "client_id" : zoho_user_info_obj.client_id,
        "client_secret" : zoho_user_info_obj.client_secret,
        "grant_type" : "refresh_token",
    }
    location = zoho_user_info_obj.location
    url = 'https://accounts.zoho.%s/oauth/v2/token'%(location)
    response = requests.post(url, data=payload).json()
    return response


def pull_orders_from_zoho(zoho_user_info_obj, page, pull_status):
    user_shop_name = zoho_user_info_obj.shop_name
    website_url = zoho_user_info_obj.website_url
    client_id = zoho_user_info_obj.client_id
    client_secret = zoho_user_info_obj.client_secret
    store_id = zoho_user_info_obj.store_id
    location = zoho_user_info_obj.location
    access_token = zoho_access_token(zoho_user_info_obj)
    access_token = access_token['access_token']
    headers = {
        'Authorization': 'Zoho-oauthtoken %s'%(access_token),
        'X-com-zoho-store-organizationid':store_id,
        'X-ZOHO-Include-Formatted':'true'
    }
    fetch_url = 'https://commerce.zoho.%s/store/api/v1/salesorders?order_status=%s&sort_column=created_time&sort_order=D&page=%s&per_page=25'%(location, pull_status, page)
    try:
        response = requests.get(
            fetch_url,
            headers=headers
        ).json()
        response["auth_access_token"] = access_token
    except Exception as e:
        logger.exception('Error fetching orders from zoho store : ')
        logger.exception(f'Zoho shop name: {user_shop_name}')

        raise Exception('Unable to fetch orders from zoho store')

    return response


def get_zoho_orders_service(page, pull_status, auth_token, zoho_user_info_obj):
    response = {}
    response['shop_name'] = zoho_user_info_obj.shop_name
    response["page"] = page + 1
    zoho_orders_response = pull_orders_from_zoho(zoho_user_info_obj, page, pull_status)
    if not 'salesorders' in zoho_orders_response:
        response["err"] = zoho_orders_response
        return response
    if type(zoho_orders_response['salesorders']) == list and len(zoho_orders_response):
        pickup_loc_list = get_clients_pickup_locations_by_type(auth_token, "zoho")
        orders = prepare_pickrr_orders_from_zoho_orders(
            zoho_user_info_obj, zoho_orders_response, auth_token, pickup_loc_list)
        response["pickup_locations"] = pickup_loc_list
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["page"] = -1
        response["other"] = zoho_orders_response
    return response


def prepare_pickrr_orders_from_zoho_orders(user_obj, zoho_response, auth_token, pickup_loc_list):
    try:
        unplaced_orders = []
        placed_orders = []
        orders = zoho_response['salesorders']
        zoho_order_ids = [order['salesorder_id'] for order in orders]
        already_placed_order_ids = fetch_duplicate_client_orders(
            zoho_order_ids, auth_token, 'zoho')
        auth_access_token = zoho_response["auth_access_token"]
        for order in zoho_response['salesorders']:
            if user_obj.is_multilocation:
                pass
            pickrr_dict = zoho_to_pickrr_dict(user_obj, auth_token, order, pickup_loc_list, auth_access_token)
            if order['salesorder_id'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                unplaced_orders.append(pickrr_dict.copy())
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def zoho_to_pickrr_dict(user_obj, auth_token, json_dict, pickup_loc_list, auth_access_token):
    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:
        import dateutil.parser as parser
        created_at = parser.parse(str(json_dict['created_time']))
        created_at = created_at.isoformat()
        order_dict = {
            "pickrr_dict": "",
            "order_id": str(json_dict['salesorder_id']),
            "created_at": created_at,
            "platform_order_id": str(json_dict['salesorder_number'])
        }
        pickrr_dict = pickup_loc_list[0].copy() if pickup_loc_list else {}
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['auth_access_token'] = auth_access_token
        pickrr_dict['website_url'] = user_obj.website_url
        pickrr_dict['item_list'] = []
        other_info = []
        items_name = ""
        quantity = 0
        lengths = []
        heights = []
        widths = []
        weight = []
        location = user_obj.location
        url = 'https://commerce.zoho.%s/store/api/v1/salesorders/%s/'%(location, json_dict['salesorder_id'])
        store_id = user_obj.store_id
        headers = {
            'Authorization': 'Zoho-oauthtoken %s'%(auth_access_token),
            'X-com-zoho-store-organizationid':store_id
        }
        try:
            response = requests.get(
                url,
                headers=headers
            ).json()
        except Exception as e:
            logger.exception('Error fetching orders from zoho store : ')
            logger.exception(f'Zoho shop name: {user_obj.shop_name}')

            raise Exception('Unable to fetch orders from zoho store')
        for item in response['salesorder']['line_items']:
            item_dict = {}
            item_dict['item_name'] = item['name']
            if len(items_name) > 1:
                items_name = items_name + ', ' + (item['name'])
            else:
                items_name = item['name']
            item_dict['sku'] = item['sku']
            item_dict['price'] = item['rate']
            item_dict['quantity'] = item['quantity']
            quantity += int(item['quantity'])
            pickrr_dict['item_list'].append(item_dict)
            d = {
                "order_item_id": item['item_id'],
                "qty": item['quantity']
            }
            other_info.append(d.copy())
            lengths.append(item['package_details']['length'])
            heights.append(item['package_details']['height'])
            widths.append(item['package_details']['width'])
            weight.append(item['package_details']['weight'])
        pickrr_dict["other_info"] = other_info
        pickrr_dict['to_name'] = response['salesorder']['contact_person_details'][0]['first_name'] + " " + response['salesorder']['contact_person_details'][0]['last_name']
        pickrr_dict['to_email'] = response['salesorder']['contact_person_details'][0]['email']
        pickrr_dict['to_phone_number'] = response['salesorder']['contact_person_details'][0]['phone']
        try:
            address = response['salesorder']['shipping_address']
        except:
            address = response['salesorder']['billing_address']
        pickrr_dict['to_pincode'] = str(address['zip'])
        street = ','.join(address["street2"])
        pickrr_dict['to_address'] = str(street + str(" ") +
                                        address['city'] + str(" ") +
                                        address['state'] + str(" ") +
                                        address['country']+ str(" ") +
                                        address['country_code'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['order_time'] = created_at
        pickrr_dict['item_weight'] = sum(weight) if type(weight[0]) == int else '-'
        pickrr_dict['item_length'] = sorted(lengths)[-1]
        pickrr_dict['item_height'] = sorted(heights)[-1]
        pickrr_dict['item_breadth'] = sorted(widths)[-1]
        pickrr_dict['item_name'] = items_name
        pickrr_dict['client_order_id'] = str(json_dict['salesorder_id'])
        pickrr_dict['platform_order_id'] = str(json_dict['salesorder_number'])
        pickrr_dict['total_discount'] = response['salesorder']['discount_total']
        pickrr_dict['shipping_charge'] = response['salesorder']['shipping_charge']
        pickrr_dict['invoice_number'] = response['salesorder']['invoices'][0]['invoice_number'] if response['salesorder']['invoices'] else '-'
        pickrr_dict['invoice_value'] = response['salesorder']['total']
        cod = 0.0
        try:
            if response['salesorder']['payment_mode'] == "cash_on_delivery":
                cod = response['salesorder']['total']
        except:
            pass
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict


def update_order_status_to_zoho(order_id, order_status, auth_token, zoho_user_info_obj):
    user_shop_name = zoho_user_info_obj.shop_name
    store_id = zoho_user_info_obj.store_id
    access_token = zoho_access_token(zoho_user_info_obj)
    access_token = access_token['access_token']
    location = zoho_user_info_obj.location
    headers = {
        'Authorization': 'Zoho-oauthtoken %s'%(access_token),
        'X-com-zoho-store-organizationid':store_id
    }
    fetch_url = 'https://commerce.zoho.%s/store/api/v1/salesorders/%s/status/%s/'%(location, order_id, order_status)
    try:
        response = requests.post(
            fetch_url,
            headers=headers
        ).json()
        response["auth_access_token"] = access_token
    except Exception as e:
        logger.exception('Error updating the status of orders from zoho store : ')
        logger.exception(f'Zoho shop name: {user_shop_name}')

        raise Exception('Unable to update the status of order')

    return response


def update_tracking_id_to_zoho(order_id, tracking_number, carrier, auth_token, zoho_user_info_obj):
    user_shop_name = zoho_user_info_obj.shop_name
    store_id = zoho_user_info_obj.store_id
    access_token = zoho_access_token(zoho_user_info_obj)
    access_token = access_token['access_token']
    location = zoho_user_info_obj.location
    headers = {
        'Authorization': 'Zoho-oauthtoken %s'%(access_token),
        'X-com-zoho-store-organizationid':store_id
       }
    fetch_url = 'https://commerce.zoho.%s/store/api/v1/salesorders/%s/shipmentorders'%(location, order_id)
    payload = {
        "tracking_number": tracking_number,
        "carrier": carrier
    }
    try:
        response = requests.put(
            fetch_url,
            data=json.dumps(payload),
            headers=headers
        ).json()
        response["auth_access_token"] = access_token
    except Exception as e:
        logger.exception('Error updating the tracking_id of orders from zoho store : ')
        logger.exception(f'Zoho shop name: {user_shop_name}')

        raise Exception('Unable to update the tracking_id of order')

    return response


def get_zoho_refresh_token(auth_code, location, zoho_user_info_obj):
    auth_token = zoho_user_info_obj.auth_token
    payload = {
        "code": auth_code,
        "client_id": zoho_user_info_obj.client_id,
        "client_secret": zoho_user_info_obj.client_secret,
        'redirect_uri': 'https://cfapi.pickrr.com' + reverse('zoho_user_access_token'),
        "grant_type": "authorization_code"
    }
    url = 'https://accounts.zoho.%s/oauth/v2/token'%(location)
    response = requests.post(url, data=payload)
    return response


def zoho_authorize(zoho_user_info_obj):
    """
    auth_token: pickrr auth token of the user.
    shop_name: shop name of the user.

    Authorizes zoho app for a user
    """
    auth_token = zoho_user_info_obj.auth_token
    query_params = {
        'scope': zoho_user_info_obj.scope,
        'client_id': zoho_user_info_obj.client_id,
        'response_type': 'code',
        'access_type': 'offline',
        'state': auth_token,
        # This redirect url should be present in the zoho app's redirect urls part
        'redirect_uri': 'https://cfapi.pickrr.com' + reverse('zoho_user_access_token'),
    }

    url = 'https://accounts.zoho.in/oauth/v2/auth?' + urlencode(query_params, safe='')
    return redirect(url)


class TranslateService:

    @staticmethod
    def translate_data(data):
        translated_data = []
        for row in data:
            comma_in_column = re.findall(r'\"(.+?)\"', row)
            if comma_in_column:
                row = row.replace('"', '')
                for sub_string in comma_in_column:
                    if sub_string in row:
                        row = row.replace(sub_string, sub_string.replace(',', ' '))
            columns = row.split(',')
            for column in range(len(columns)):
                if column in [2, 4]:
                    value = columns[column].replace(' ', ' <1> ')
                    if " " not in value:
                        value = value + ' <1> '
                    translated_value = google_language_translator(value)
                    translated_value = translated_value.replace('<1>', ' ').replace('  ', ' ').strip(' ')
                    columns[column] = translated_value
            column_string = ','.join(columns)
            translated_data.append(column_string)
        return translated_data
