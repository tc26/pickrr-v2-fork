import csv

from django.http import HttpResponse
from rest_framework import generics, status, views
from rest_framework.response import Response
from rest_framework.request import Request

from . import commonutils
from . import constants
from . import shopifyservices

from . import models
from .exceptions import (EntityNotFoundException, InputFileHandlerException, InvalidRequestException)
from utils.helpers import filter_or_none
from utils.file_handlers import CsvFileHandler
import json


from .dao import ModelDaoBase
from .errors import GeneralErrorCodes
from .models import ShopifyUserModel
from .services import CommonServices

import logging

logger = logging.getLogger('django.request')


class ShopifyUpdateOrderStatus(views.APIView):

    def get_valid_status(self, params):
        status = params["status"]
        valid_status = constants.pickrr_shopify_status_mapper.get(status)
        return valid_status, valid_status

    def check_if_fulfilment(self, params):
        url = f"https://{params['shop_name']}.myshopify.com/admin/api/2020-07/orders/{params['shopify_order_id']}/fulfillments.json"
        headers = {
            "content-type": "application/json",
            "X-Shopify-Access-Token": params["shopify_token"]
        }
        fulfillments = commonutils.make_get_request(url, headers)
        if "fulfillments" in fulfillments and not len(fulfillments["fulfillments"]):
            return False
        else:
            return True

    def post(self, request):
        try:
            params = commonutils.read_post_json(request.body)
            ful_res = None
            get_ful_res = None
            st_res = None
            tag_res = None
            if "need_fulfillment" in params and params["need_fulfillment"]:
                fulfilment = True
                if "periodic" in params:
                    fulfilment = self.check_if_fulfilment(params)

                details = {
                    "location_id": params["location_id"],
                    "tracking_number": params["tracking_number"] if "tracking_number" in params else None,
                    "notify_customer": params["notify_customer"] if "notify_customer" in params else True,
                    "courier_used": params["courier_used"] if "courier_used" in params else "PICKRR"
                }
                pickrr_dict = shopifyservices.prepare_shopify_fulfillment_data(details)
                if fulfilment:
                    ful_res = shopifyservices.fulfill_shopify_order(params["shopify_token"], params['shop_name'], str(params['shopify_order_id']), pickrr_dict)
            url = f"https://{params['shop_name']}.myshopify.com/admin/api/2020-07/orders/{params['shopify_order_id']}/fulfillments.json"
            headers = {
                "content-type": "application/json",
                "X-Shopify-Access-Token": params["shopify_token"]
            }
            fulfillments = commonutils.make_get_request(url, headers)
            if "fulfillments" in fulfillments and not len(fulfillments["fulfillments"]):
                return Response({"err": "Fulfillments not found"}, status=status.HTTP_201_CREATED)
            fulfillment_list = fulfillments["fulfillments"] if "fulfillments" in fulfillments else []
            fulfillment_id = None
            for fulfillment in fulfillment_list:
                if "tracking_company" in fulfillment and fulfillment["tracking_company"]:# and fulfillment["tracking_company"].lower() == "pickrr":
                    fulfillment_id = fulfillment["id"]
                    break
            if not fulfillment_id:
                return Response({"err": "Fulfillments not found"}, status=status.HTTP_201_CREATED)
            url = f"https://{params['shop_name']}.myshopify.com/admin/api/2020-07/orders/{params['shopify_order_id']}/fulfillments/{fulfillment_id}/events.json"
            track_status, track_info = self.get_valid_status(params)
            data = {
              "event": {
                "status": track_status,
                "message": track_info
              }
            }
            get_ful_res = commonutils.make_post_request(url, data, headers)
            shopify_user_obj = None
            from .models import ShopifyUserModel
            try:
                shopify_user_obj = ShopifyUserModel.objects.get(token=params["shopify_token"])

                # Adding tags to shopify orders
                tag = CommonServices.get_tag(status=params['status'], user_obj=shopify_user_obj)

                if tag:
                    service = shopifyservices.AddTagsInOrderService(shopify_user_obj)
                    tag_res = service.add_tags({'order_id': params['shopify_order_id'], 'tags': tag})
            except:
                pass
            if params["status"] == "DL" and params['amount'] and shopify_user_obj and shopify_user_obj.update_cod_status:
                url = f"https://{params['shop_name']}.myshopify.com/admin/api/2021-01/orders/{params['shopify_order_id']}/transactions.json"
                transactions = commonutils.make_get_request(url, headers)
                parent_id = None
                kind = "capture"
                for trans in transactions["transactions"]:
                    if trans["id"]:
                        parent_id = trans["id"]
                        kind = trans["kind"]
                        break
                if parent_id:
                    url = f"https://{params['shop_name']}.myshopify.com/admin/api/2021-01/orders/{params['shopify_order_id']}/transactions.json"
                    payload = {
                      "transaction": {
                        "currency": params['currency'],
                        "amount": str(params['amount']),
                        "kind": kind,
                        "parent_id": parent_id,
                      }
                    }
                    if kind == "sale":
                        payload["transaction"]["source"] = "external"
                    st_res = commonutils.make_post_request(url, payload, headers)
            return Response({"success": True, "ful_res": ful_res, "st_res": st_res,
                             "get_ful_res": get_ful_res, "tag_res": tag_res},
                            status=status.HTTP_201_CREATED)
        except Exception as e:
            logger.exception(str(e))
            error_args = {
                'params': params if 'params' in locals() else None,
                'pickrr_dict': pickrr_dict if 'pickrr_dict' in locals() else None,
                'ful_res': ful_res if 'ful_res' in locals() else None,
                'get_ful_res': get_ful_res if 'get_ful_res' in locals() else None,
                'st_res': st_res if 'st_res' in locals() else None,
                'fulfillments': fulfillments if 'fulfillment' in locals() else None,
                'track_status': track_status if 'track_status' in locals() else None,
                'get_ful_res': get_ful_res if 'get_ful_res' in locals() else None,
            }

            logger.error(f'Error while updating Shopify order status\n {json.dumps(error_args)}')
            return Response({"err": str(e)}, status=status.HTTP_424_FAILED_DEPENDENCY)


class UpdateTrackingDetailOnShopify(views.APIView):

    def post(self, request):
        """

        :param request:
        {
            "store_name": "harish-30",
            "auth": "GRV$12-PICKRR-SHOPIFY-#12LXN$",
            "shopify_token": "217c93aaa1da0b96586c27174771797c",
            "orders": [
                {
                "id": "2278205718630",
                "location_id": "34639446118"
                }
            ]
        }
        :return:
        """
        try:
            params = commonutils.read_post_json(request.body)
            error = []
            if params["auth"] != "GRV$12-PICKRR-SHOPIFY-#12LXN$":
                return Response({"err": "Unauthorized"}, status=status.HTTP_401_UNAUTHORIZED)
            store_name = params["store_name"]
            shopify_token = params["shopify_token"]
            for order in params["orders"]:
                pickrr_dict = {}
                try:
                    pickrr_dict = shopifyservices.prepare_shopify_fulfillment_data(order)
                    res = shopifyservices.fulfill_shopify_order(shopify_token, store_name, str(order["id"]), pickrr_dict)
                    if "errors" in res:
                        pickrr_dict["err"] = str(res["errors"])
                        error.append(pickrr_dict)
                except Exception as e:
                    pickrr_dict["err"] = str(e)
                    error.append(pickrr_dict)
            return Response({"err": error},  status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_424_FAILED_DEPENDENCY)


class UpdateTrackingDetailsOnShopifyViaCsv(views.APIView, ModelDaoBase):
    """
    Update tracking details on shopify via csv. format :
    store_name, shopify_token, auth

    order_id,location_id,tracking_number
    1,123,123456
    2,234,234567
    3,345,345678
    """

    model = ShopifyUserModel
    CSV_FIELDNAMES = ('order_id', 'location_id', 'tracking_number', 'error')

    def validate_request(self, body):
        store_name = body.get('store_name')
        shopify_token = body.get('shopify_token')

        if not all([store_name, shopify_token]):
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Both `store_name` and `shopify_token` are mandatory in request'
                }
            )

        user_objs = filter_or_none(ShopifyUserModel, shop_name=store_name)
        if not user_objs:
            raise EntityNotFoundException(
                GeneralErrorCodes.ENTITY_NOT_FOUND,
                **{
                    'message': f'store name: {store_name} does not exist in system'
                }
            )
        is_shopify_token_valid = False
        for user_obj in user_objs:
            if user_obj.token == shopify_token:
                is_shopify_token_valid = True
        if not is_shopify_token_valid:
            raise EntityNotFoundException(
                GeneralErrorCodes.ENTITY_NOT_FOUND,
                **{
                    'message': f'Shopify token: {shopify_token} does not exist against store name {store_name}'
                }
            )

    def insert_csv_row(self, csv_writer, **kwargs):
        row_data = {fieldname: kwargs.get(fieldname) for fieldname in self.CSV_FIELDNAMES}
        csv_writer.writerow(row_data)

    def post(self, request):
        try:
            body = request.POST
            if body.get('auth') != 'GRV$12-PICKRR-SHOPIFY-#12LXN$':
                return Response({"err": "Unauthorized"}, status=status.HTTP_401_UNAUTHORIZED)

            self.validate_request(body)

            csv_file = request.FILES.get('file')
            csv_handler = CsvFileHandler(csv_file)
            payload = csv_handler("dict")

            store_name = body['store_name']
            shopify_token = body['shopify_token']

            response = HttpResponse(content_type='text/csv', status=status.HTTP_200_OK)
            response['Content-Disposition'] = 'attachment; filename="response.csv"'
            csv_writer = csv.DictWriter(response, fieldnames=self.CSV_FIELDNAMES)
            csv_writer.writeheader()

            for data in payload:
                order_id, exc = None, None
                try:

                    order_id = data.pop('order_id')
                    pickrr_dict = shopifyservices.prepare_shopify_fulfillment_data(data)
                    res = shopifyservices.fulfill_shopify_order(shopify_token, store_name, order_id, pickrr_dict)
                    if 'errors' in res:
                        exc = str(res['errors'])

                except Exception as e:
                    exc = str(e)
                finally:
                    self.insert_csv_row(csv_writer, order_id=order_id, location_id=data['location_id'],
                                        tracking_number=data.get('tracking_number'), error=exc)

            return response

        except InputFileHandlerException as exc:
            return Response({'err': f'Invalid csv received - {str(exc)}'}, status=status.HTTP_400_BAD_REQUEST)
        except InvalidRequestException as exc:
            return Response({'err': f'{str(exc)}'}, status=status.HTTP_400_BAD_REQUEST)
        except EntityNotFoundException as exc:
            return Response({'err': f'Invalid user entries provided - {str(exc)}'}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as exc:
            return Response({'err': str(exc)}, status=status.HTTP_424_FAILED_DEPENDENCY)


class UpdateShopifyUser(views.APIView):
    """
    param_request -

        {
            "auth_token":"abcd1234",
            "shop_name":"shopify_demo",
            "token":"12345678",
            "is_active":"False",
            "is_multilocation":"True"
        }

    Response:

    {
        "success": true,
        "failed": {
            "ksta": "not found"
        }
    }
    """

    def post(self, request):
        try:
            params = commonutils.read_post_json(request.body)
            if not "auth_token" in params and not params["auth_token"]:
                return Response({"err": "auth_token mandatory"}, status=status.HTTP_200_OK)
            if not "shop_name" in params and not params["shop_name"]:
                return Response({"err": "shop_name mandatory"}, status=status.HTTP_401_UNAUTHORIZED)
            shop_name = params["shop_name"]
            auth_token = params["auth_token"]
            shopify_user_obj = models.ShopifyUserModel.objects.get(auth_token=str(auth_token),
                                                                   shop_name=str(shop_name))
            if not shopify_user_obj:
               return Response({"err": "No shopify user is associated with given shop name"}, status=status.HTTP_401_UNAUTHORIZED)
            params.pop('auth_token')
            params.pop('shop_name')
            failed = {}
            has_update = False
            for dict_key, dict_val in params.items():
                if dict_key not in shopify_user_obj.__dict__.keys():
                    failed[dict_key] = 'not found'
                shopify_user_obj.__setattr__(dict_key, dict_val)
                has_update = True
            if has_update:
                shopify_user_obj.save()
            return Response({'success': True, "failed": failed},  status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_424_FAILED_DEPENDENCY)


class ShopifyApiBase(views.APIView, ModelDaoBase):

    model = ShopifyUserModel

    def validate_request(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')

        if not all([auth_token, shop_name]):
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': 'Both `auth_token` and `shop_name are mandatory in request'
                }
            )


class ShopifyOrderFetchApi(ShopifyApiBase):

    def get_filters(self, request, user_obj):
        # pull_status = request.query_params.get('status') or user_obj.pull_status
        # date_from = request.query_params.get('date_from')
        # date_to = request.query_params.get('date_to')
        days = request.query_params.get('days', 1)
        next_url = request.query_params.get('next_url', None)

        filters = {
            'days': days,
            'next_url': next_url
        }

        return filters

    def get(self, request: Request) -> Response:
        """
        :return: returns a Response object with orders fetched from shopify
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            self.validate_request(request)
            auth_token, shop_name = request.query_params['auth_token'], request.query_params['shop_name']
            if auth_token not in constants.SHOPIFY_MIGRATED_CLIENTS:
                if "days" in request.GET and int(request.GET["days"]) > 0:
                    days = int(request.GET["days"])
                else:
                    days = 1
                if "next_url" in request.GET:
                    next_url = request.GET["next_url"]
                    url = "https://pickrr.herokuapp.com/fetch-shop-orders/%s/?next_url=%s" % (shop_name, next_url)
                else:
                    url = "https://pickrr.herokuapp.com/fetch-shop-orders/%s/?days=%s" % (shop_name, days)
                headers = {'content-type': 'application/json'}
                import requests
                order_data = requests.get(url, headers=headers).json()
                return Response(data=order_data, status=status.HTTP_200_OK)
            user_obj = self.validate_user(auth_token, shop_name)
            filters = self.get_filters(request, user_obj)
            service = shopifyservices.ShopifyOrderFetchService(user_obj)
            order_listing_response = service.fetch_orders(filters)
            response.data = order_listing_response

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response


class SyncShopifyUsers(views.APIView, ModelDaoBase):
    model = ShopifyUserModel

    def validate_request(self, request):
        auth_token = request.query_params.get('auth_token')

        if not all([auth_token]):
            raise InvalidRequestException(
                GeneralErrorCodes.INVALID_REQUEST,
                **{
                    'message': ' `auth_token` is mandatory in request'
                }
            )

    def get(self, request: Request) -> Response:

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            self.validate_request(request)
            users = shopifyservices.sync_from_heroku()
            response.data = users
        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc
        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc
        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc
        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }

        return response


class UpdateOrderTags(views.APIView, ModelDaoBase):
    """
    Updates tag in a shopify order
    """
    model = ShopifyUserModel

    def put(self, request: Request) -> Response:
        """
        Request query params:
        {
            "auth_token":"abcd1234",
            "shop_name":"shopify_demo",
        }

        Request payload:
        {
            "order_id": 450789469,
            "status": "OP" # can be any pickrr order status (OP, PP, OT, RTO, OC, DL, RTD)
        }

        Response:

        {
            "order": {
                "id": 3661679329382,
                "admin_graphql_api_id": "gid://shopify/Order/3661679329382",
                "app_id": 1354745,
                .
                .
                .
                .
                "tags": "out for delivery, delivered, rto",
                .
                .
                .
            }
        }
        """

        exc_obj = None
        response = Response(status=status.HTTP_200_OK)

        try:
            if not request.data:
                raise InvalidRequestException(
                    GeneralErrorCodes.INVALID_REQUEST,
                    **{
                        'message': 'Empty payload is not supported'
                    }
                )

            auth_token, shop_name = request.query_params.get('auth_token'), request.query_params.get('shop_name')
            user_obj = self.validate_user(auth_token, shop_name)

            request.data['tags'] = CommonServices.get_tag(status=request.data['status'], user_obj=user_obj)
            service = shopifyservices.AddTagsInOrderService(user_obj)
            response.data = service.add_tags(request.data)

        except InvalidRequestException as exc:
            response.status_code, exc_obj = status.HTTP_400_BAD_REQUEST, exc

        except EntityNotFoundException as exc:
            response.status_code, exc_obj = status.HTTP_404_NOT_FOUND, exc

        except Exception as exc:
            response.status_code, exc_obj = status.HTTP_500_INTERNAL_SERVER_ERROR, exc

        finally:
            if response.status_code >= status.HTTP_300_MULTIPLE_CHOICES:
                response.data = {
                    'error': str(exc_obj)
                }
        return response
