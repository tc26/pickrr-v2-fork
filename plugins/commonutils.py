import requests
import json
from .constants import REQUEST_TIMEOUT


def make_post_request(url, payload, headers=None):
    if not headers:
        headers = {'content-type': 'application/json'}
    res = requests.post(url, data=json.dumps(payload), headers=headers)
    return res.json()


def make_get_request(url, headers=None, timeout=REQUEST_TIMEOUT):
    if not headers:
        headers = {'content-type': 'application/json'}
    res = requests.get(url, headers=headers, timeout=timeout)
    return res.json()


def read_post_json(json_val):
    try:
        return json.loads(json_val)
    except Exception as e:
        return {'err': str(e)}


class CustomRequestBase:
    GET  = lambda url, qp, headers, body: requests.request('GET',  url, headers=headers, params=qp)
    POST = lambda url, qp, headers, body: requests.request('POST', url, headers=headers, data=body)
    PUT  = lambda url, qp, headers, body: requests.request('PUT',  url, headers=headers, params=qp, data=body)

    @staticmethod
    def make_request(method, url, headers=None, query_params=None, payload=None):
        if not headers:
            headers = {}
        if not query_params:
            query_params = {}
        if not payload:
            payload = {}

        request = getattr(CustomRequestBase, method, None)
        if not request:
            pass

        api_response, response = '', {}
        try:
            api_response = request(url, query_params, headers, payload)
            response = api_response.json()
        except json.decoder.JSONDecodeError:
            response = api_response.text
        except requests.exceptions.Timeout:
            pass
        except requests.exceptions.TooManyRedirects:
            pass
        except requests.exceptions.RequestException:
            pass

        return response


def cleanphonenumber(phone):
    phone_number = "".join(phone.split())
    if phone_number[0] == ',':
        phone_number = phone_number[1:]
    phone_number.replace('-', '')
    phone_number.replace(' ', '')
    phone_number.replace(',', '')
    phone_number = phone_number.split(",")
    if phone_number:
        if len(phone_number) < 14:
            return phone_number[0]
        else:
            return ""
    else:
        return ''
