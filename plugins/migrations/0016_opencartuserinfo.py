# Generated by Django 2.2.8 on 2021-02-01 09:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugins', '0015_auto_20201126_1630'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpencartUserInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shop_name', models.CharField(default=None, max_length=255)),
                ('key1', models.CharField(default=None, max_length=255)),
                ('key2', models.CharField(default=None, max_length=255)),
                ('email', models.EmailField(max_length=254, verbose_name='Email Address')),
                ('website_url', models.CharField(default=None, max_length=255)),
                ('auth_token', models.CharField(max_length=255)),
                ('is_active', models.BooleanField(default=True)),
                ('pull_status', models.CharField(blank=True, max_length=16, null=True)),
                ('order_placed', models.CharField(blank=True, max_length=64, null=True)),
                ('picked_up', models.CharField(blank=True, max_length=64, null=True)),
                ('transit', models.CharField(blank=True, max_length=64, null=True)),
                ('rto', models.CharField(blank=True, max_length=64, null=True)),
                ('cancelled', models.CharField(blank=True, max_length=64, null=True)),
                ('delivered', models.CharField(blank=True, max_length=64, null=True)),
                ('rtd', models.CharField(blank=True, max_length=64, null=True)),
                ('update_tracking_id', models.BooleanField(default=False)),
                ('update_tracking_status', models.BooleanField(default=False)),
                ('is_multilocation', models.BooleanField(default=False)),
                ('pickrr_accounts', models.TextField(help_text='acc_name:auth_token,acc_name:auth_token')),
                ('version', models.CharField(max_length=8)),
            ],
        ),
    ]
