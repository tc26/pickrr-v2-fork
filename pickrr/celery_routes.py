# Use this file to define the celery routes

from kombu import Queue

celery_queues = (
    # Create the queues here 
    # Example:
    # Queue('demo', routing_key='demoKey'),
)

celery_routes = {
    # Create the task route here
    # Example:
    # 'orders.tasks.celery_demo': {
    #         'queue': 'demo',
    #         'routing_key': 'demokey',
    #     },
}