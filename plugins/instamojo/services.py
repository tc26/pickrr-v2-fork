import logging

from datetime import datetime, timezone

from django.conf import settings

from plugins.services import (get_clients_pickup_locations_by_type,
                              fetch_duplicate_client_orders)

from .utils import authorization_header, make_instamojo_request, response_dict
from .endpoints import BASE_URL, AUTHORIZE_ENDPOINT, ACCESS_TOKEN_ENDPOINT, STORE_ORDERS_ENDPOINT
from .models import UserModel

logger = logging.getLogger(__name__)


def generate_login_url(user_id):
    scope = ' '.join([str(scope) for scope in settings.INSTAMOJO_SCOPE])
    redirect_uri = settings.INSTAMOJO_REDIRECT_URI
    client_id = settings.INSTAMOJO_CLIENT_ID
    response_type = 'code'
    final_url = "{}{}?scope={}&redirect_uri={}&response_type={}&client_id={}&state={}"\
        .format(BASE_URL, AUTHORIZE_ENDPOINT, scope, redirect_uri, response_type, client_id, user_id)
    return final_url


def get_user_info_using_auth_token_shop_name(auth_token, shop_name):
    try:
        user_info = UserModel.objects.get(auth_token=auth_token,
                                          shop_name=shop_name)
    except UserModel.DoesNotExist:
        return None

    return user_info


def generate_access_token(code):
    payload = {
        'code': code,
        'client_id':settings.INSTAMOJO_CLIENT_ID,
        'client_secret':settings.INSTAMOJO_CLIENT_SECRET,
        'redirect_uri':settings.INSTAMOJO_REDIRECT_URI,
        'grant_type':'authorization_code'
    }
    response = make_instamojo_request(ACCESS_TOKEN_ENDPOINT, None,
                                      payload, 'POST')
    return response


def generate_refresh_token(refresh_token):
    payload = {
        'refresh_token': refresh_token,
        'client_id':settings.INSTAMOJO_CLIENT_ID,
        'client_secret':settings.INSTAMOJO_CLIENT_SECRET,
        'grant_type':'refresh_token'
    }
    response = make_instamojo_request(ACCESS_TOKEN_ENDPOINT, None,
                                      payload, 'POST')
    return response


def update_user_refresh_token(user_id, refresh_token):
    user = UserModel.objects.get(id=user_id)
    user.refresh_token = refresh_token
    user.save()


def update_user_token(user_id, refresh_token, access_token, expires_in):
    user = UserModel.objects.get(id=user_id)
    user.refresh_token = refresh_token
    user.access_token = access_token
    user.access_token_expires_in = expires_in
    user.access_token_created_at = datetime.now(timezone.utc)
    user.save()


def get_user_access_token(user):
    expires_in = user.access_token_expires_in
    created_at = user.access_token_created_at
    diff = (datetime.now(timezone.utc) - created_at).total_seconds()
    if diff < expires_in:
        return response_dict(True, user.access_token, None)
    else:
        response = generate_refresh_token(user.refresh_token)

        if response['is_success']:
            data = response['data']
            update_user_token(user.id, data['refresh_token'],
                              data['access_token'], data['expires_in'])
            return response_dict(True, data['access_token'], None)

        else:
            return response_dict(False, None, response['msg'])


def pull_orders(access_token, filters):
    headers = authorization_header(access_token)
    response = make_instamojo_request(STORE_ORDERS_ENDPOINT, headers,
                                      filters, 'GET')
    return response


def instamojo_to_pickrr_dict(user_obj, auth_token, response, pickup_loc_list,
                             access_token):
    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:
        order_dict = {
            "pickrr_dict": "",
            "order_id": str(response['id']),
            "created_at": str(response['created']),
            "platform_order_id": str(response['orderid'])
        }
        pickrr_dict = pickup_loc_list[0].copy() if pickup_loc_list else {}
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['access_token'] = access_token
        pickrr_dict['website_url'] = user_obj.website_url
        pickrr_dict['item_list'] = []
        other_info = []
        items_name = ""
        quantity = 0
        lengths = []
        heights = []
        widths = []
        weight = []
        for item in response['details']['items']:
            item_dict = {}
            item_dict['item_name'] = item['title']
            if len(items_name) > 1:
                items_name = items_name + ', ' + (item['title'])
            else:
                items_name = item['title']
            item_dict['sku'] = item['sku']
            item_dict['price'] = item['final_price']
            item_dict['quantity'] = item['quantity']
            quantity += int(item['quantity'])
            pickrr_dict['item_list'].append(item_dict)
            d = {
                "order_item_id": item['id'],
                "qty": item['quantity']
            }
            other_info.append(d.copy())

        pickrr_dict["other_info"] = other_info
        pickrr_dict['to_name'] = '{} {}'.format(response['firstname'], response['lastname'])
        pickrr_dict['to_email'] = response['email']
        pickrr_dict['to_phone_number'] = response['contact']
        pickrr_dict['to_pincode'] = str(response['code'])
        pickrr_dict['to_address'] = '{} {} {} {} {}'.format(
            response['address'], response['city'], response['state'],
            response['country_display'], response['country'])
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['order_time'] = str(response['created'])
        pickrr_dict['item_weight'] = 0.5
        pickrr_dict['item_length'] = 10
        pickrr_dict['item_height'] = 10
        pickrr_dict['item_breadth'] = 10
        pickrr_dict['item_name'] = items_name
        pickrr_dict['client_order_id'] = str(response['id'])
        pickrr_dict['platform_order_id'] = str(response['orderid'])
        pickrr_dict['total_discount'] = response['details']['discount']
        pickrr_dict['shipping_charge'] = response['details']['shipping_charge']
        # TODO: unable to find inovice number
        pickrr_dict['invoice_number'] = '-'
        pickrr_dict['invoice_value'] = response['total_order']
        cod = 0.0
        try:
            if response['payment_method'] == "Cash-On-Delivery":
                cod = response['total_order']
        except:
            pass
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict


def prepare_pickrr_orders(access_token, user_obj, response, auth_token, pickup_loc_list):
    unplaced_orders = []
    placed_orders = []
    orders = response['data']['orders']
    order_ids = [order['id'] for order in orders]
    already_placed_order_ids = fetch_duplicate_client_orders(
        order_ids, auth_token, 'instamojo'
    )
    for order in orders:
        if user_obj.is_multilocation:
            pass
        pickrr_dict = instamojo_to_pickrr_dict(user_obj, auth_token, order,
                                               pickup_loc_list, access_token)
        if order['id'] in already_placed_order_ids:
            placed_orders.append(pickrr_dict.copy())
        else:
            unplaced_orders.append(pickrr_dict.copy())
    orders = {
        "uorders": unplaced_orders,
        "porders": placed_orders
    }
    return orders


def get_orders(access_token, page, pull_status, auth_token, user_info_obj, filters):
    orders_response = pull_orders(access_token, filters)
    response = {}
    response['shop_name'] = user_info_obj.shop_name
    if orders_response['is_success']:
        if orders_response['data']['count'] > 0:
            pickup_loc_list = get_clients_pickup_locations_by_type(auth_token,
                                                                   "instamojo")
            orders = prepare_pickrr_orders(access_token, user_info_obj,
                                           orders_response, auth_token,
                                           pickup_loc_list)
            response["pickup_locations"] = pickup_loc_list
            response.update({'orders': orders})
        else:
            response["err"] = "No order found"
            response["other"] = orders_response
    else:
        return orders_response
    return response


def get_client_instamojo_status(user_obj, order_status):
    if order_status == 'OP':
        current_status = user_obj.order_placed
    elif order_status == 'RTO':
        current_status = user_obj.rto
    elif order_status == 'DL':
        current_status = user_obj.delivered
    elif order_status == 'OT':
        current_status = user_obj.transit
    elif order_status == 'PP':
        current_status = user_obj.picked_up
    elif order_status == 'OC':
        current_status = user_obj.cancelled
    else:
        current_status = None
    return current_status


def update_order_status(access_token, order_id, params):
    headers = authorization_header(access_token)
    headers.update({'Content-Type': 'application/json'})
    endpoint = '/v2/store/orders/{}/update-order/'.format(order_id)
    response = make_instamojo_request(endpoint, headers,
                                      params, 'PATCH')
    return response


def update_tracking_details(access_token, order_id, params):
    headers = authorization_header(access_token)
    headers.update({'Content-Type': 'application/json'})
    endpoint = '/v2/store/orders/{}/'.format(order_id)
    params = {
        "shipping": {
            "waybill": params.get('tracking_number'),
            "courier_partner": params.get('carrier'),
            "tracking_url": params.get('tracking_url')
        }
    }
    response = make_instamojo_request(endpoint, headers,
                                      params, 'PATCH')
    return response
