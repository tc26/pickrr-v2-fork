from rest_framework import generics, status, views
from rest_framework.response import Response
from . import models, services, dao
import collections
from utils import helpers
from . import constants
from . import commonutils
from django.shortcuts import redirect
import logging

logger = logging.getLogger('bikayi')


class BikayiFetchOrderApi(views.APIView):

    def get_filters(self, request):
        date_from = request.query_params.get('date_from', '')

        filters = {
            'date_from': date_from
        }
        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        bikayi_user_info_obj = dao.get_bikayi_user_info(auth_token, shop_name)

        if not bikayi_user_info_obj:
            return Response({
                'error': 'User has no associated store'
                },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        orders = services.get_bikayi_orders_service(filters, auth_token, bikayi_user_info_obj)

        return Response(orders, status=status.HTTP_200_OK)


def get_client_bikayi_status(bikayi_user_obj, order_status):
    current_status = "SHIPPED"
    if order_status == 'RTD':
        current_status = bikayi_user_obj.rtd
    elif order_status == 'RTO':
        current_status = bikayi_user_obj.rto
    elif order_status == 'DL':
        current_status = bikayi_user_obj.delivered
    elif order_status == 'OT':
        current_status = bikayi_user_obj.transit
    elif order_status == 'PP':
        current_status = bikayi_user_obj.picked_up
    elif order_status == 'OC':
        current_status = bikayi_user_obj.cancelled
    elif order_status == 'OP':
        current_status = bikayi_user_obj.order_placed
    else:
        pass
    return current_status


class BikayiUpdateOrderStatus(views.APIView):

    def post(self, request):
        post_data = request.data
        auth_token = post_data.get('auth_token')
        order_id = post_data.get('order_id')
        order_status = post_data.get('status')
        tracking_id = post_data.get('tracking_id')
        bikayi_user_obj = models.BikayiUserInfo.objects.get(
            auth_token=auth_token)
        bikayi_status = get_client_bikayi_status(bikayi_user_obj, order_status)
        map_dict = collections.OrderedDict()
        map_dict['status'] = str(bikayi_status)
        map_dict['appId'] = "PICKRR"
        map_dict['merchantId'] = str(bikayi_user_obj.merchant_id)
        map_dict['orderId'] = str(order_id)
        map_dict['wayBill'] = str(tracking_id)
        signature = services.create_bikayi_signature(map_dict)
        url = "https://asia-south1-bikai-d5ee5.cloudfunctions.net/platformPartnerFunctions-updateOrder"
        headers = {
            'content-type': 'application/json',
            "Authorization": signature
        }
        response = commonutils.make_post_request(url, map_dict, headers)
        return Response(response, status=status.HTTP_200_OK)


class BikayiUpdateTracking(views.APIView):

    def post(self, request):
        try:
            post_data = request.data
            auth_token = post_data.get('auth_token')
            order_id = post_data.get('order_id')
            order_status = post_data.get('status')
            tracking_url = post_data.get('tracking_url')
            tracking_id = post_data.get('tracking_id')
            bikayi_user_obj = models.BikayiUserInfo.objects.get(
                auth_token=auth_token)
            bikayi_status = get_client_bikayi_status(bikayi_user_obj, order_status)
            map_dict = collections.OrderedDict()
            map_dict['status'] = str(bikayi_status)
            map_dict['trackingLink'] = str(tracking_url)
            map_dict['appId'] = "PICKRR"
            map_dict['merchantId'] = str(bikayi_user_obj.merchant_id)
            map_dict['orderId'] = str(order_id)
            map_dict['wayBill'] = str(tracking_id)
            signature = services.create_bikayi_signature(map_dict)
            url = "https://asia-south1-bikai-d5ee5.cloudfunctions.net/platformPartnerFunctions-updateOrder"
            headers = {
                'content-type': 'application/json',
                "Authorization": signature
            }
            response = commonutils.make_post_request(url, map_dict, headers)
            logger.info(response)
            return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            logger.exception(e)


class BikayiClientRegistration(views.APIView):

    def post(self, request):
        try:
            response_data = {}
            register_data = helpers.read_post_json(request.body)
            merchant_id = register_data['merchant_id']
            response = services.get_merchant_info(merchant_id)
            if response['status'] == 200:
                payload = dict()
                payload["email"] = str(response['merchant']['email'])
                payload["merchant_id"] = merchant_id
                payload["name"] = str(response['merchant']['name'])
                payload["phone_number"] = str(response['merchant']['phoneNumber'])
                payload["pickup_address"] = response['merchant']['location']
                user_obj, created = models.BikayiUserInfo.objects.get_or_create(
                    shop_name=str(payload["phone_number"]),
                    merchant_id=merchant_id, email=payload["email"]
                )
                if not created:
                    response_data['err'] = "Already Registered"
                    return Response(response_data, status=status.HTTP_200_OK)
                payload["auth_token"] = ""
                pickrr_accounts = ""
                url = constants.PICKRR_BASE_URL + "api/bikayi/client/registration/"
                response = services.update_details_to_pickrr(url, payload)
                if "err" in response and "auth_token" not in response:
                    response_data['err'] = response
                else:
                    payload["auth_token"] = response["auth_token"]
                    pickrr_accounts = response["acc_name"] + ":" + payload["auth_token"]
                user_obj.auth_token = payload["auth_token"]
                user_obj.pickrr_accounts = pickrr_accounts
                user_obj.save()
                response_data['success'] = True
            else:
                response_data['err'] = "Something went wrong!! Try after sometime"
            return Response(response_data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_200_OK)


class BikayiMerchantDocs(views.APIView):

    def post(self, request):
        try:
            register_data = helpers.read_post_json(request.body)
            merchant_id = register_data['merchant_id']
            bikayi_user_obj = models.BikayiUserInfo.objects.get(
                merchant_id=str(merchant_id))
            url = "https://asia-south1-bikai-d5ee5.cloudfunctions.net/platformPartnerFunctions-fetchIdv"
            map_dict = collections.OrderedDict()
            map_dict['appId'] = constants.PICKRR_BIKAYI_APPID
            map_dict['merchantId'] = str(bikayi_user_obj.merchant_id)
            signature = services.create_bikayi_signature(map_dict)
            headers = {
                'content-type': 'application/json',
                "Authorization": signature
            }
            response = commonutils.make_post_request(url, map_dict, headers)
            return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_200_OK)


class BikayiMerchantLogin(views.APIView):

    def get(self, request):
        try:
            merchant_id = request.query_params.get('merchant_id', '')
            if not merchant_id:
                return Response({"err": "Invalid Credentials"}, status=status.HTTP_200_OK)
            bikayi_user_obj = models.BikayiUserInfo.objects.get(
                merchant_id=str(merchant_id))
            auth_token = bikayi_user_obj.auth_token
            url = "https://dashboard.pickrr.com/?auth_token=" + auth_token + "&unique_key=1h57gimwcaql"
            return redirect(url)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_200_OK)
