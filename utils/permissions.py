from rest_framework.permissions import BasePermission
from . import pickrr_services
import logging

logger = logging.getLogger(__name__)


class IsTokenVerified(BasePermission):
    """
    Allows access only to users which have auth token.
    Hits the pickkr backend api to verify that the token is correct.

        GET Request - Token must in query params
        For all other methods. POST, PUT, PATCH, DELETE. - Token in request body
    """
    message = "Auth Token missing or invalid."

    def has_permission(self, request, view):
        try:
            if request.method == 'GET':
                auth_token = request.query_params.get('auth_token')
            else:
                auth_token = request.data.get('auth_token')

            if auth_token and pickrr_services.verify_auth_token(auth_token):
                return True
        except Exception as e:
            logger.exception('Authentication error : ')

        return False
