from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_marketPlaceUserId(value):
    value = str(value)
    if len(value) != 6:
        raise ValidationError(
            _('%(value)s Market Place user id should not greater or less than 6'),
            params={'value': value},
        )


class EcwidUserInfo(models.Model):
    store_id = models.IntegerField(db_index=True)
    store_private_token = models.CharField(max_length=255)
    shop_name = models.CharField(max_length=255, default=None)
    email = models.EmailField()
    website_url = models.CharField(max_length=255, blank=True, null=True)
    auth_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(max_length=64, blank=True, null=True)
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(
        default=False,
        help_text="AWAITING_PROCESSING, PROCESSING, SHIPPED, DELIVERED, WILL_NOT_DELIVER, RETURNED, READY_FOR_PICKUP"
    )
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")

    def __str__(self):
        return self.shop_name


class PickupAddress(models.Model):
    user = models.ForeignKey(EcwidUserInfo, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class EazySlipsUserInfo(models.Model):
    license_key = models.CharField(max_length=255)
    shop_name = models.CharField(max_length=255, default=None)
    email = models.EmailField(verbose_name="Email Address", help_text="Email id used to create eazyslip account")
    website_url = models.CharField(max_length=255, blank=True, null=True)
    auth_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(max_length=16, blank=True, null=True,
                                   help_text="Order status (O{New Orders}, A{Processing}, E{Manifested}, G{Dispatched})")
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(
        default=False,
        help_text="AWAITING_PROCESSING, PROCESSING, SHIPPED, DELIVERED, WILL_NOT_DELIVER, RETURNED, READY_FOR_PICKUP"
    )
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")
    ch = (
        ("magento_v1", "Magento version 1"),
        ("magento_v2", "Magento version 2"),
        ("amazon", "Amazon"),
        ("prestashop", "PrestaShop"),
        ("opencart", "OpenCart"),
        ("woocommerce", "WooCommerce")
    )
    channel = models.CharField(choices=ch, max_length=16, default="opencart")

    def __str__(self):
        return self.shop_name


class EazySlipsPickupAddress(models.Model):
    user = models.ForeignKey(EazySlipsUserInfo, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class MagnetoUserInfo(models.Model):
    shop_name = models.CharField(max_length=255, default=None)
    username = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address")
    website_url = models.CharField(max_length=255, blank=True, null=True)
    auth_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(max_length=16, blank=True, null=True)
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")
    v = (
        ("magento_v1", "Magento version 1"),
        ("magento_v2", "Magento version 2"),
        ("shoptimize", "Shoptimize")
    )
    version = models.CharField(choices=v, max_length=16)

    def __str__(self):
        return self.shop_name


class MagentoPickupAddress(models.Model):
    user = models.ForeignKey(MagnetoUserInfo, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class EasyEcomUserInfo(models.Model):
    seller_id = models.CharField(
        max_length=255, blank=True, null=True, help_text="Amazon Seller Id")
    seller_user_id = models.CharField(
        max_length=255, blank=True, null=True, help_text="Amazon MWS auth token")
    token = models.CharField(
        max_length=255, blank=True, null=True,
        help_text="Seller Identification Token From EasyEcom")
    shop_name = models.CharField(max_length=255, default='')
    password = models.CharField(max_length=255, default="pickrrclient@123")
    email = models.EmailField(verbose_name="Email Address", help_text="Email id used to create eazyslip account")
    website_url = models.CharField(max_length=255, blank=True, null=True)
    auth_token = models.CharField(max_length=255, help_text="Pickrr Auth Token")
    webhook_token = models.CharField(
        max_length=255, blank=True, null=True, help_text="Easyecom Webhook Token")
    company_carrier_id = models.CharField(
        max_length=16, blank=True, null=True,
        help_text="Easyecom Company Carrier Id")
    api_access_token = models.CharField(
        max_length=255, blank=True, null=True, help_text="Easyecom Api Auth Token")
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=8, blank=True, null=True,
        help_text="Order status (1:Pending, 2:Assigned, 3:Confirmed, 4:Out of stock, 5:Printed, 6:Ready to dispatch, 7:Shipped, 9:Cancelled, 10:Returned, 11:Upcoming)")
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")

    def __str__(self):
        return self.shop_name


class EasyEcomPickupAddress(models.Model):
    user = models.ForeignKey(EasyEcomUserInfo, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)
    state_code = models.CharField(max_length=8, help_text="https://ddvat.gov.in/docs/List%20of%20State%20Code.pdf")

    def __unicode__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class JungleworksUserInfo(models.Model):
    api_key = models.TextField(max_length=255, blank=True, help_text="User Api Id")
    marketplace_user_id = models.IntegerField(
        validators=[validate_marketPlaceUserId], blank=True, help_text="Market Place User Id")
    shop_name = models.CharField(max_length=255, default=None,)
    email = models.EmailField(verbose_name="Email Address")
    website_url = models.CharField(max_length=255, blank=True, null=True)
    auth_token = models.CharField(max_length=255, help_text="Pickrr Auth Token")
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=8, blank=True, null=True,
        help_text="Order status (1:Pending, 2:Assigned, 3:Confirmed, 4:Out of stock, 5:Printed, 6:Ready to dispatch, 7:Shipped, 9:Cancelled, 10:Returned, 11:Upcoming)")
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")

    def __str__(self):
        return self.shop_name


class RemoteAddressTracker(models.Model):
    ip = models.CharField(max_length=64)
    remarks = models.CharField(max_length=64, blank=True, null=True)
    traffic = models.IntegerField(default=0)
    daily_traffic = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.ip) + " -> " + str(self.remarks)


class OpencartUserInfo(models.Model):
    shop_name = models.CharField(max_length=255, default=None)
    key1 = models.CharField(max_length=255, blank=True, null=True)
    key2 = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address")
    website_url = models.CharField(max_length=255, blank=True, null=True)
    auth_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(max_length=16, blank=True, null=True)
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")
    version = models.CharField(max_length=8)

    def __str__(self):
        return self.shop_name


class PrestaShopUserModel(models.Model):
    shop_token = models.CharField(max_length=255)
    auth_token = models.CharField(max_length=255)
    shop_name = models.CharField(max_length=255)
    email = models.EmailField(verbose_name='Email Address')
    website_url = models.CharField(
        max_length=255, blank=True, null=True,
        help_text='website domain. For eg: https://my-site.com/prestashop'
    )
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=16, blank=True, null=True,
        help_text='string representation of integer denoting status of order defined as per seller,'
                  'this status orders will be pulled from prestashop'
    )
    order_placed = models.CharField(max_length=64, blank=True, null=True, help_text='order placed mapping with seller')
    picked_up = models.CharField(max_length=64, blank=True, null=True, help_text='picked up mapping with seller')
    transit = models.CharField(max_length=64, blank=True, null=True, help_text='transit mapping with seller')
    rto = models.CharField(max_length=64, blank=True, null=True, help_text='rto mapping with seller')
    cancelled = models.CharField(max_length=64, blank=True, null=True, help_text='cancelled mapping with seller')
    delivered = models.CharField(max_length=64, blank=True, null=True, help_text='delivered mapping with seller')
    rtd = models.CharField(max_length=64, blank=True, null=True, help_text='rtd mapping with seller')
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text='acc_name:auth_token,acc_name:auth_token')

    class Meta:
        constraints = [models.UniqueConstraint(fields=['auth_token', 'shop_name'], name='auth_token_shop_name_idx'),
                       ]

    def __str__(self):
        return self.shop_name


class PrestaShopPickupAddress(models.Model):
    user = models.ForeignKey(PrestaShopUserModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class BikayiUserInfo(models.Model):
    shop_name = models.CharField(max_length=255, default=None)
    website_url = models.CharField(max_length=255, blank=True, null=True)
    merchant_id = models.CharField(max_length=255, default=None)
    email = models.EmailField(verbose_name="Email Address")
    auth_token = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(max_length=16, blank=True, null=True)
    order_placed = models.CharField(max_length=64, blank=True, null=True, default="IN_PROGRESS")
    picked_up = models.CharField(max_length=64, blank=True, null=True, default="IN_PROGRESS")
    transit = models.CharField(max_length=64, blank=True, null=True, default="SHIPPED")
    rto = models.CharField(max_length=64, blank=True, null=True, default="RETURNED")
    cancelled = models.CharField(max_length=64, blank=True, null=True, default="CANCELLED")
    delivered = models.CharField(max_length=64, blank=True, null=True, default="DELIVERED")
    rtd = models.CharField(max_length=64, blank=True, null=True, default="RETURNED")
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token", blank=True, null=True)

    def __str__(self):
        return self.shop_name


class ShopifyUserModel(models.Model):
    shop_name = models.CharField(max_length=255)
    token = models.CharField(max_length=255, blank=True, null=True)
    api_key = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(
        max_length=255, blank=True, null=True,
        help_text="https://{apikey}:{password}@{hostname}/admin/api/{version}/{resource}.json")
    shared_secret = models.CharField(max_length=255, blank=True, null=True)
    storefront_access_token = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(verbose_name='Email Address')
    website_url = models.CharField(
        max_length=255, blank=True, null=True,
        help_text='website domain. For eg: https://my-site.shopify.com/'
    )
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=16, blank=True, null=True,
        help_text='string representation of integer denoting status of order defined as per seller,'
                  'this status orders will be pulled from shopify'
    )
    auth_token = models.CharField(max_length=255)
    order_placed = models.CharField(max_length=64, blank=True, null=True, help_text='order placed mapping with seller')
    picked_up = models.CharField(max_length=64, blank=True, null=True, help_text='picked up mapping with seller')
    transit = models.CharField(max_length=64, blank=True, null=True, help_text='transit mapping with seller')
    rto = models.CharField(max_length=64, blank=True, null=True, help_text='rto mapping with seller')
    cancelled = models.CharField(max_length=64, blank=True, null=True, help_text='cancelled mapping with seller')
    delivered = models.CharField(max_length=64, blank=True, null=True, help_text='delivered mapping with seller')
    rtd = models.CharField(max_length=64, blank=True, null=True, help_text='rtd mapping with seller')
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    update_cod_status = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text='acc_name:auth_token,acc_name:auth_token')
    last_fetched_order_id = models.CharField(
        max_length=255, default="0", help_text='last order id fetched from shopify')
    auto_place_order = models.BooleanField(default=False)
    pre_fetch = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['auth_token', 'shop_name'], name='auth_token_shop_name'),
        ]

    def __str__(self):
        return self.shop_name


class ShopifyPickupAddress(models.Model):
    user = models.ForeignKey(ShopifyUserModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    location_id = models.CharField(max_length=6, verbose_name="Shopify Location Id")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class ShopifyOrder(models.Model):
    user = models.ForeignKey(ShopifyUserModel, on_delete=models.CASCADE)
    order_id = models.CharField(max_length=255)
    order = models.TextField(help_text='json string of order object')
    pickrr_dict = models.TextField(help_text='json string of pickrr_dict of the order')
    is_fulfilled = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    shopify_order_created_at = models.DateTimeField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'order_id'], name='user_order_id'),
        ]

        ordering = ['-shopify_order_created_at']


class WixUserInfo(models.Model):
    shop_name = models.CharField(max_length=255, default=None)
    email = models.EmailField(verbose_name="Email Address", help_text="Email id used to create Wix account")
    website_url = models.CharField(
        max_length=255, blank=True, null=True,
        help_text='website domain. For eg: https://accountname.wixsite.com/my-site'
    )
    auth_token = models.CharField(max_length=255, help_text="Pickrr Auth Token")
    api_refresh_token = models.CharField(
        max_length=255, blank=True, null=True, help_text="Wix Api Refresh Token")
    user_instance_id = models.CharField(
        max_length=255, blank=True, null=True, help_text="Instance id provided by Wix.")
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=64, blank=True, null=True,
        help_text="Order status (1:NOT_FULFILLED, 2:PARTIALLY_FULFILLED, 3:FULFILLED)")
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text="acc_name:auth_token,acc_name:auth_token")

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['auth_token', 'shop_name'], name='auth_token_shop_name_idx'),
        ]

    def __str__(self):
        return self.shop_name


class WixPickupAddress(models.Model):
    user = models.ForeignKey(WixUserInfo, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class ZohoUserModel(models.Model):
    client_id = models.CharField(max_length=255)
    client_secret = models.CharField(max_length=255)
    scope = models.CharField(max_length=255)
    store_id = models.CharField(max_length=255)
    location = models.CharField(max_length=255, null=True, blank=True)
    website_url = models.CharField(
        max_length=255, blank=True, null=True,
        help_text='redirect-uri-> website domain. For eg: https://accounts.zoho.in'
    )
    refresh_token = models.CharField(max_length=255, blank=True)
    email = models.EmailField(verbose_name='Email Address')
    auth_token = models.CharField(max_length=255)
    shop_name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=16, blank=True, null=True,
        help_text='string representation of integer denoting status of order defined as per seller,'
                  'this status orders will be pulled from zoho'
    )
    order_placed = models.CharField(max_length=64, blank=True, null=True, help_text='order placed mapping with seller')
    picked_up = models.CharField(max_length=64, blank=True, null=True, help_text='picked up mapping with seller')
    transit = models.CharField(max_length=64, blank=True, null=True, help_text='transit mapping with seller')
    rto = models.CharField(max_length=64, blank=True, null=True, help_text='rto mapping with seller')
    cancelled = models.CharField(max_length=64, blank=True, null=True, help_text='cancelled mapping with seller')
    delivered = models.CharField(max_length=64, blank=True, null=True, help_text='delivered mapping with seller')
    rtd = models.CharField(max_length=64, blank=True, null=True, help_text='rtd mapping with seller')
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(help_text='acc_name:auth_token,acc_name:auth_token')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['auth_token', 'shop_name'], name='auth_token_shop_name_idx'),
        ]

    def __str__(self):
        return self.shop_name


class ZohoPickupAddress(models.Model):
    user = models.ForeignKey(ZohoUserModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']


class WoocommUserInfo(models.Model):
    auth_token = models.CharField(max_length=255, help_text="Pickrr Auth Token")
    email = models.EmailField(verbose_name='Email Address')
    shop_name = models.CharField(max_length=64, help_text="Woocommerce Store name")
    username = models.CharField(max_length=64, help_text="Woocommerce Client Key")
    password = models.CharField(max_length=64, help_text="Woocommerce Client Secret")
    pull_status = models.CharField(max_length=64, blank=True, null=True)
    order_placed = models.CharField(max_length=64, blank=True, null=True)
    picked_up = models.CharField(max_length=64, blank=True, null=True)
    transit = models.CharField(max_length=64, blank=True, null=True)
    rto = models.CharField(max_length=64, blank=True, null=True)
    cancelled = models.CharField(max_length=64, blank=True, null=True)
    delivered = models.CharField(max_length=64, blank=True, null=True)
    rtd = models.CharField(max_length=64, blank=True, null=True)
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multiple_location = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.shop_name
