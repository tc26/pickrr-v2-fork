# Generated by Django 2.2.8 on 2020-09-07 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugins', '0006_eazyslipsuserinfo_pickrr_accounts'),
    ]

    operations = [
        migrations.AddField(
            model_name='ecwiduserinfo',
            name='pickrr_accounts',
            field=models.TextField(default='', help_text='acc_name:auth_token,acc_name:auth_token'),
            preserve_default=False,
        ),
    ]
