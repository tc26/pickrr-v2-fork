from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions, status

from django.shortcuts import redirect

from plugins.instamojo.utils import response_dict
from plugins.instamojo.services import (
    generate_login_url, get_user_info_using_auth_token_shop_name,
    generate_access_token, get_orders, update_user_refresh_token,
    generate_refresh_token, get_client_instamojo_status, update_order_status,
    update_tracking_details, update_user_token, get_user_access_token
)
from .serializers import (AppAuthorizationSerializer, FetchOrdersSerializer,
                          OrderStatusUpdateSerializer, CarrierUpdateSerializer)


class AppAuthorizationView(APIView):
    """
    Generating login URL only if user exists in db. Fetching user using
    auth-token and shop-name
    """

    def get(self, request, format=None):
        serializer = AppAuthorizationSerializer(data=request.query_params)
        if serializer.is_valid():
            try:
                user = get_user_info_using_auth_token_shop_name(
                    serializer.data.get('auth_token'), serializer.data.get('shop_name')
                )
                if user:
                    url = generate_login_url(user_id=user.id)
                    return redirect(url)
                else:
                    return Response(response_dict(False, None, "User does not exists!"))
            except Exception as e:
                return Response(response_dict(False, None, str(e)))
        else:
            return Response(response_dict(False, None, serializer.errors))


class AccessTokenView(APIView):
    """
    Redirect URI mentioned in the InstaMojo app. First checking if getting
    valid code, if yes then creating access token.
    """
    def get(self, request, format=None):
        try:
            error = request.GET.get('error', None)
            code = request.GET.get('code', None)
            user_id = request.GET.get('state')
            if error:
                return Response(response_dict(False, None, error))
            else:
                access_token_response = generate_access_token(code)
                if access_token_response['is_success']:
                    refresh_token = access_token_response['data']['refresh_token']
                    access_token = access_token_response['data']['access_token']
                    expires_in = access_token_response['data']['expires_in']
                    update_user_token(user_id, refresh_token, access_token, expires_in)
                return Response(access_token_response)
        except Exception as e:
            return Response(response_dict(False, None, str(e)))


class FetchOrdersView(APIView):
    """
    Fetching orders from IntaMojo and returning a response in pikcrr dict
    """
    def get_params(self, data):
        params = {
            'page': data.get('page', 1),
            'pull_status': data.get('pull_status', None),
            'payment_status': data.get('payment_status', None),
            'payment_method': data.get('payment_method', None),
            'waybill__isblank': data.get('waybill__isblank', None),
            'limit': data.get('limit', 25)
        }
        return params

    def get(self, request):
        serializer = FetchOrdersSerializer(data=request.GET)
        if serializer.is_valid():
            try:
                data = serializer.data
                page = data.get('page', 1)
                pull_status = data.get('pull_status', None)
                auth_token = data.get('auth_token')
                shop_name = data.get('shop_name')
                filters = self.get_params(data)
                user_info_obj = get_user_info_using_auth_token_shop_name(auth_token,
                                                                         shop_name)
                if not user_info_obj:
                    return Response(response_dict(False, None, "User does not exists!"))

                filters['pull_status'] = pull_status if pull_status else user_info_obj.pull_status
                response = get_user_access_token(user_info_obj)

                if response['is_success']:
                    access_token = response['data']

                    try:
                        orders = get_orders(access_token, page, pull_status,
                                            auth_token, user_info_obj, filters)
                        return Response(orders, status=status.HTTP_200_OK)
                    except Exception as e:
                        return Response(response_dict(False, None, response['msg']))

                else:
                    return Response(response_dict(False, None, response['msg']))

            except Exception as e:
                return Response(response_dict(False, None, str(e)))

        else:
            return Response(response_dict(False, None, serializer.errors))


class OrderStatusUpdateView(APIView):
    """
    API to update order status
    """
    def post(self, request):
        serializer = OrderStatusUpdateSerializer(data=request.data)
        if serializer.is_valid():
            try:
                auth_token = serializer.data.get('auth_token')
                shop_name = serializer.data.get('shop_name')
                order_status = serializer.data.get('order_status')
                order_id = serializer.data.get('order_id')
                comments = serializer.data.get('comments')
                user_obj = get_user_info_using_auth_token_shop_name(auth_token,
                                                                    shop_name)
                if user_obj:
                    if not user_obj.update_tracking_status:
                        msg = 'Updating tracking status not allowed for this Shop'
                        return Response(response_dict(False, None, msg))
                    order_status = get_client_instamojo_status(user_obj, order_status)
                    response = get_user_access_token(user_obj)

                    if response['is_success']:
                        access_token = response['data']
                        response = update_order_status(access_token, order_id,
                                                       {'order_status': order_status,
                                                        'comments': comments})
                        if response['is_success']:
                            return Response(response)

                    return Response(response_dict(False, None, response['msg']))

                else:
                    return Response(response_dict(False, None, "User does not exists!"))

            except Exception as e:
                return Response(response_dict(False, None, str(e)))
        else:
            return Response(response_dict(False, None, serializer.errors))


class CarrierUpdateView(APIView):
    """
    API to update order carrier on InstaMojo
    """
    def post(self, request):
        serializer = CarrierUpdateSerializer(data=request.data)
        if serializer.is_valid():
            try:
                auth_token = serializer.data.get('auth_token')
                shop_name = serializer.data.get('shop_name')
                order_id = serializer.data.get('order_id')
                tracking_number = serializer.data.get('tracking_number')
                carrier = serializer.data.get('carrier')
                tracking_url = serializer.data.get('tracking_url')
                user_obj = get_user_info_using_auth_token_shop_name(auth_token,
                                                                    shop_name)
                if user_obj:
                    if not user_obj.update_tracking_id:
                        msg = 'Updating tracking id is not allowed for this Shop'
                        return Response(response_dict(False, None, msg))
                    response = get_user_access_token(user_obj)

                    if response['is_success']:
                        access_token = response['data']
                        response = update_tracking_details(
                            access_token, order_id,
                            {'tracking_number': tracking_number, 'carrier': carrier,
                             'tracking_url': tracking_url}
                        )
                        if response['is_success']:
                            return Response(response)

                    return Response(response_dict(False, None, response['msg']))

                else:
                    return Response(response_dict(False, None, "User does not exists!"))
            except Exception as e:
                return Response(response_dict(False, None, str(e)))
        else:
            return Response(response_dict(False, None, serializer.errors))
