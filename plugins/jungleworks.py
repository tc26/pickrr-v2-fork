from rest_framework import generics, status, views
from rest_framework.response import Response
from utils import permissions as custom_permissions

from . import accessors, models, serializers, services, dao, constants, commonutils


class JungleworksFetchOrderApi(views.APIView):

    def get_filters(self, request):
        page = int(request.query_params.get('page', 1))
        status = request.query_params.get('status', '')
        date_from = request.query_params.get('date_from', '')
        date_to = request.query_params.get('date_to', '')

        filters = {
            'page': page,
            'date_from': date_from,
            'date_to': date_to,
        }

        if status:
            filters['status'] = status

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')

        jungleworks_user_info_obj = dao.jungleworks_user_info_obj(auth_token, shop_name)
        if not jungleworks_user_info_obj:
            return Response({
                'error': 'User has no associated store'
            },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        orders = services.get_jungleworks_orders_service(auth_token, jungleworks_user_info_obj, filters)

        return Response(orders, status=status.HTTP_200_OK)


class JungleworksUpdateOrderStatus(views.APIView):

    def get_valid_status(self, params):
        status = params["status"]
        valid_status = constants.pickrr_jungleworks_status_mapper[status]
        return valid_status

    def post(self, request):
        try:
            api_key = request.data.get('api_key')
            user_id = request.data.get('user_id')
            job_id = request.data.get('job_id')
            order_status = request.data.get('status')
            marketplace_user_id = request.data.get('marketplace_user_id')

            payload = {
                'api_key': api_key,
                'user_id': user_id,
                'job_id': job_id,
                'status': order_status,
                'marketplace_user_id': marketplace_user_id
            }

            url = f"https://api.yelo.red/open/task/updateStatus"
            res = commonutils.make_post_request(url, payload)
            return Response(res, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_200_OK)
