from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework import status, views

from .serializers import (UpdateChannelSerializer, EcwidUserInfoSerializer,
                          EazySlipsUserInfoSerializer,
                          MagnetoUserInfoSerializer,
                          EasyEcomUserInfoSerializer,
                          JungleworksUserInfoSerializer,
                          OpencartUserInfoSerializer,
                          PrestaShopUserModelSerializer,
                          BikayiUserInfoSerializer,
                          ShopifyUserModelSerializer)

from utils import permissions as custom_permissions
from plugins.commonutils import read_post_json
from .constants import ChannelModelsMap
from . import services


class UpdateChannelInfoView(UpdateAPIView):
    serializer_class = UpdateChannelSerializer

    def response_dict(self, is_success, msg, data):
        return {
            'is_success': is_success,
            'msg': msg,
            'data': data
        }

    def get_serializer_to_update(self, key):
        channel_serializers = {
            self.serializer_class.ECWID: EcwidUserInfoSerializer,
            self.serializer_class.EAZYSLIPS: EazySlipsUserInfoSerializer,
            self.serializer_class.MAGENTO: MagnetoUserInfoSerializer,
            self.serializer_class.EASYECOM: EasyEcomUserInfoSerializer,
            self.serializer_class.JUNGLEWORKS: JungleworksUserInfoSerializer,
            self.serializer_class.OPENCART: OpencartUserInfoSerializer,
            self.serializer_class.PRESTASHOP: PrestaShopUserModelSerializer,
            self.serializer_class.BIKAYI: BikayiUserInfoSerializer,
            self.serializer_class.SHOPIFY: ShopifyUserModelSerializer
        }
        serializer = channel_serializers.get(key, None)
        if serializer:
            return serializer
        else:
            msg = "Please enter valid channel type"
            response = self.response_dict(False, msg, None)
            raise ValidationError(response)

    def get_model_name_from_serializer(self, serializer):
        return serializer.Meta.model

    def get_object(self):
        channel_type = self.request.data.get('channel_type')
        shop_name = self.request.data.get('user_shop_name')
        auth_token = self.request.data.get('user_auth_token')
        serializer = self.get_serializer_to_update(channel_type)
        model = self.get_model_name_from_serializer(serializer)
        try:
            obj = model.objects.get(shop_name=shop_name, auth_token=auth_token)
        except model.DoesNotExist:
            msg = "Please enter valid shop name and auth token"
            response = self.response_dict(False, msg, None)
            raise ValidationError(response)
        return obj

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            channel_type = self.request.data.get('channel_type')
            update_serializer = self.get_serializer_to_update(channel_type)
            serializer = update_serializer(self.get_object(),
                                           data=request.data,
                                           partial=partial)
            if serializer.is_valid():
                serializer.save()
                msg = "Details updated successfully!"
                response = self.response_dict(True, msg, serializer.data)
                return Response(response)
            else:
                response = self.response_dict(False, serializer.errors, None)
                return Response(response)
        else:
            response = self.response_dict(False, serializer.errors, None)
            return Response(response)

    def put(self, request, *args, **kwargs):
        return self.update(request, partial=False)

    def patch(self, request, *args, **kwargs):
        return self.update(request, partial=True)


class BulkOrderCSVValidation(views.APIView):

    def post(self, request):
        try:
            csv_file = request.FILES.get('order_data')
            from utils.file_handlers import CsvFileHandler, WriteToCSV
            csv_handler = CsvFileHandler(csv_file)
            payload = csv_handler(None)
            from plugins.services import TranslateService
            translated_data = TranslateService.translate_data(payload)
            csv_data = WriteToCSV.write_string_list_to_csv(translated_data)
            from plugins.placeorder import PlaceOrderService
            place_order_service = PlaceOrderService()
            validated_data = place_order_service.send_bulk_csv_data_to_pickrr_for_validation(csv_data, request.POST)
            return Response(data=validated_data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'err': str(e), "translated_data": []}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CreateChannelClient(views.APIView):
    model_class = ChannelModelsMap

    def response_dict(self, is_success, msg, data):
        return {
            'is_success': is_success,
            'msg': msg,
            'data': data
        }

    def get_func_to_create(self, key):
        channel_models = {
            self.model_class.WOOCOM: services.create_wooccom_client_service,
            self.model_class.MAGENTO: services.create_magento_client_service,
            self.model_class.EASYECOM: services.create_easyecom_client_service,
            self.model_class.SHOPIFY: services.create_shopify_client_service
        }
        func = channel_models.get(key, None)
        if func:
            return func
        else:
            msg = "Please enter valid channel type"
            response = self.response_dict(False, msg, None)
            raise ValidationError(response)

    def post(self, request):
        try:
            params = read_post_json(request.body)
            func = self.get_func_to_create(params["channel_type"])
            params.pop("channel_type")
            res = func(params)
            return Response(data=res, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                {'err': str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
