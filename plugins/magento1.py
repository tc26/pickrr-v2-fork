from rest_framework import generics, status, views
from rest_framework.response import Response
from utils import permissions as custom_permissions
from . import commonutils

from . import accessors, models, serializers, services, dao


class Magento1FetchOrderApi(views.APIView):

    def get_filters(self, request, magento1_user_info_obj):
        # page = int(request.query_params.get('page', 1))
        status = request.query_params.get('status', '')
        date_from = request.query_params.get('date_from', '')
        date_to = request.query_params.get('date_to', '')

        filters = {
            # 'page': page,
            'from': date_from,
            'to': date_to
        }
        if status:
            filters['status'] = status
        else:
            filters['status'] = magento1_user_info_obj.pull_status

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        magento1_user_info_obj = dao.get_magento1_user_info(auth_token, shop_name)

        if not magento1_user_info_obj:
            return Response({
                'error': 'User has no associated store'
                },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request, magento1_user_info_obj)
        orders = services.get_magento1_orders_service(filters, auth_token, magento1_user_info_obj)

        return Response(orders, status=status.HTTP_200_OK)

class Magento1CarrierUpdateApi(views.APIView):
    """
    API to update order carrier on magento1
    """

    def get_filters(self, request):
        order_id = request.data.get('order_id')
        tracking_id = request.data.get('tracking_id')

        filters = {
            'order_id': order_id,
            'tracking_id': tracking_id
        }

        return filters

    def put(self, request):
        if not request.data:
            return Response({
                'error': 'Empty payload is not supported'
                },
                status=status.HTTP_404_NOT_FOUND)
        auth_token, shop_name = request.data['auth_token'], request.data['shop_name']
        magento1_user_info_obj = dao.get_magento1_user_info(auth_token, shop_name)
        filters = self.get_filters(request)

        if not magento1_user_info_obj.update_tracking_id:
            return Response({
                'error': 'Updating tracking id not allowed for this Shop'
                },
                status=status.HTTP_404_NOT_FOUND)
        order_carrier_update_response = services.update_tracking_id_to_magento1(filters, auth_token, magento1_user_info_obj)
        return Response(order_carrier_update_response, status=status.HTTP_200_OK)

def get_client_magento_status(magento_user_obj, order_status):
    current_status = "pickrr_shipped"
    if order_status == 'RTD':
        current_status = magento_user_obj.rtd
    elif order_status == 'RTO':
        current_status = magento_user_obj.rto
    elif order_status == 'DL':
        current_status = magento_user_obj.delivered
    elif order_status == 'OT':
        current_status = magento_user_obj.transit
    elif order_status == 'PP':
        current_status = magento_user_obj.picked_up
    elif order_status == 'OC':
        current_status = magento_user_obj.cancelled
    else:
        pass
    return current_status

class Magento1OrderStatusUpdateApi(views.APIView):
    """
    API to update order status on magento1
    """

    def get_filters(self, request, magento1_user_info_obj):
        order_id = request.data.get('order_id')
        order_status = request.data.get('order_status')
        order_status = get_client_magento_status(magento1_user_info_obj, order_status)
        filters = {
            'order_id': order_id,
            'order_status': order_status
        }

        return filters

    def put(self, request):
        if not request.data:
            return Response({
                'error': 'Empty payload is not supported'
                },
                status=status.HTTP_404_NOT_FOUND)
        auth_token, shop_name = request.data['auth_token'], request.data['shop_name']
        magento1_user_info_obj = dao.get_magento1_user_info(auth_token, shop_name)
        filters = self.get_filters(request, magento1_user_info_obj)

        if not magento1_user_info_obj.update_tracking_status:
            return Response({
                'error': 'Updating tracking status not allowed for this Shop'
                },
                status=status.HTTP_404_NOT_FOUND)
        order_status_update_response = services.update_order_status_to_magento1(filters, auth_token, magento1_user_info_obj)
        return Response(order_status_update_response, status=status.HTTP_200_OK)
