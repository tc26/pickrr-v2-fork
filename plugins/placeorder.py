from rest_framework import generics, status, views
from utils.helpers import google_language_translator, make_post_request, make_post_request_with_file
from django.conf import settings
from . import constants
import os


class PlaceOrderService(views.APIView):

    PICKRR_PLACE_ORDER_URL = settings.PICKRR_BASE_URL + constants.PickrrEndpoints.PLACE_ORDER.value
    PICKRR_BULK_ORDER_VALIDATION_URL = settings.PICKRR_BASE_URL + constants.PickrrEndpoints.BULK_ORDER_CSV_VALIDATION.value

    def place_order_to_pickrr(self, order_data):
        return make_post_request(self.PICKRR_PLACE_ORDER_URL, order_data)

    def validate_order_data(self, order_data):
        for key, value in order_data.items():
            if key in ["to_name", "to_address"]:
                value = value.replace(' ', ' <1> ')
                if " " not in value:
                    value = value + ' <1> '
                translated_value = google_language_translator(value)
                translated_value = translated_value.replace('<1>', ' ').replace('  ', ' ').strip(' ')
                order_data[key] = translated_value
        return order_data

    def send_bulk_csv_data_to_pickrr_for_validation(self, csv_file, other_params):
        try:
            current_file = csv_file.name
            script_dir = os.path.dirname(__file__)
            rel_path = settings.BASE_DIR + "/"
            abs_file_path = os.path.join(script_dir, rel_path)
            files = [
                ('order_data', (current_file, open(abs_file_path + current_file, 'rb'), 'text/csv'))
            ]
            pickrr_res = make_post_request_with_file(
                self.PICKRR_BULK_ORDER_VALIDATION_URL, other_params, files)
            os.remove(abs_file_path + current_file)
            return pickrr_res
        except Exception as e:
            return {"err": str(e)}
