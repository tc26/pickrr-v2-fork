from django.apps import AppConfig


class InstamojoConfig(AppConfig):
    name = 'instamojo'
