from rest_framework import generics, status, views
from rest_framework.response import Response
from utils import permissions as custom_permissions
from . import commonutils

from . import accessors, models, serializers, services, dao


class Magento2FetchOrderApi(views.APIView):

    def get_filters(self, request):
        page = int(request.query_params.get('page', 1))
        status = request.query_params.get('status', '')
        date_from = request.query_params.get('date_from', '')
        date_to = request.query_params.get('date_to', '')

        filters = {
            'page': page,
            'date_from': date_from,
            'date_to': date_to
        }
        if status:
            filters['status'] = status

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        magento2_user_info_obj = dao.get_magento_user_info(auth_token, shop_name, "magento_v2")

        if not magento2_user_info_obj:
            return Response({
                'error': 'User has no associated store'
                },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        # absolute_url = self.request.build_absolute_uri()
        orders = services.get_magento2_orders_service(filters, auth_token, magento2_user_info_obj)

        return Response(orders, status=status.HTTP_200_OK)


def get_client_magento_status(magento_user_obj, order_status):
    current_status = "pickrr_shipped"
    if order_status == 'RTD':
        current_status = magento_user_obj.rtd
    elif order_status == 'RTO':
        current_status = magento_user_obj.rto
    elif order_status == 'DL':
        current_status = magento_user_obj.delivered
    elif order_status == 'OT':
        current_status = magento_user_obj.transit
    elif order_status == 'PP':
        current_status = magento_user_obj.picked_up
    elif order_status == 'OC':
        current_status = magento_user_obj.cancelled
    else:
        pass
    return current_status


class Magento2UpdateOrderStatus(views.APIView):

    def post(self, request):
        post_data = request.data
        website_url = post_data.get('website_url')
        auth_token = post_data.get('auth_token')
        order_id = post_data.get('order_id')
        access_token = post_data.get('access_token') if 'access_token' in post_data else None
        order_status = post_data.get('status')
        magento2_user_obj = models.MagnetoUserInfo.objects.get(
            website_url=website_url, auth_token=auth_token)
        magento_status = get_client_magento_status(magento2_user_obj, order_status)
        if not access_token:
            username = magento2_user_obj.username
            password = magento2_user_obj.password
            url = website_url + "/rest/V1/integration/admin/token"
            headers = {'content-type': 'application/json'}
            payload = {'username': username, 'password': password}
            token = commonutils.make_post_request(url, payload, headers)
            access_token = "Bearer " + token
        url = website_url + "/rest/V1/orders/%s/comments" % str(order_id)
        status_dict = {
            "statusHistory": {
                "comment": "Order Placed on PICKRR",
                "created_at": "",
                "entity_id": str(order_id),
                "entity_name": "",
                "is_customer_notified": 0,
                "is_visible_on_front": 0,
                "parent_id": 0,
                "status": magento_status,
                "extension_attributes": {}
            }
        }
        headers = {'content-type': 'application/json', "Authorization": access_token}
        response = commonutils.make_post_request(url, status_dict, headers)
        return Response(response, status=status.HTTP_200_OK)
