from rest_framework import serializers
from . import models as plugin_models
from mongo import services as mongo_services


class EcwidUserInfoSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    class Meta:
        model = plugin_models.EcwidUserInfo
        fields = '__all__'


class PickupAddressSerializer(serializers.Serializer):
    mongo_id = serializers.CharField(max_length=255, read_only=True)
    auth_token = serializers.CharField(max_length=255)
    name = serializers.CharField(max_length=128)
    address_line = serializers.CharField()
    city = serializers.CharField(max_length=128)
    state = serializers.CharField(max_length=128)
    pin_code = serializers.CharField(max_length=6)
    phone_number = serializers.CharField(max_length=28)
    email = serializers.EmailField(required=False)

    def to_representation(self, instance):
        instance['mongo_id'] = instance['_id']
        return super(PickupAddressSerializer, self).to_representation(instance)

    def create(self, validated_data):
        return mongo_services.PickupAddressService().create_pickup_address(validated_data)


class WixUserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = plugin_models.WixUserInfo
        fields = '__all__'


class WixPickupAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = plugin_models.WixPickupAddress
        fields = '__all__'
