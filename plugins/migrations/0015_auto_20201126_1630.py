# Generated by Django 2.2.8 on 2020-11-26 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugins', '0014_remoteaddresstracker'),
    ]

    operations = [
        migrations.AlterField(
            model_name='remoteaddresstracker',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
