from . import commonutils
from . import models
from . import constants
from . import services
from copy import deepcopy
from datetime import datetime, timedelta
import os
import sys
from .services import fetch_duplicate_client_orders
from typing import Dict, Optional
from typing_extensions import Literal
from dataclasses import dataclass
from rest_framework import status

# TODO: Move the following typing to the separate module/file
# Link for complete list https://docs.google.com/spreadsheets/u/1/d/e/2PACX-1vTxztVKy9fB1lq2ZGj681r8-emdwzywcZVsziRjSFTBGVNTNtKPLHRPgdCVcjmWR8WxuJ9Bv6jhw0-A/pubhtml
StateCode = Literal[
    "AP",
    "AR",
    "AS",
    "BH",
    "CT",
    "GA",
    "GJ",
    "HR",
    "HP",
    "JK",
    "JA",
    "KA",
    "KL",
    "MP",
    "MH",
    "MN",
    "ME",
    "MI",
    "NL",
    "OR",
    "PB",
    "RJ",
    "SK",
    "TN",
    "TS",
    "TR",
    "UP",
    "UT",
    "WB",
    "DL",
    "PY",
    "AN",
    "CH",
    "DN",
    "DD",
    "LD"
]

CompanyLevelTaxRate = Literal[0, 3, 5, 12, 18, 28]


@dataclass
class Address:
    address_line_1: str
    address_line_2: Optional[str]
    state_code: StateCode
    pin_code: str
    country: str

# Payload to be sent to the EasyEcom Create Company v2 API


@dataclass
class CompanyCreateV2ReqPayload:
    phone: int
    company_name: str
    email: str
    client_id: str
    # It is assigned by the EasyEcom. Not supposed to send
    branding_user_id: Optional[str]
    password: str
    companyLevelTaxRate: Optional[CompanyLevelTaxRate]
    shipping_address: Address
    billing_address: Address

# Payload received from EasyEcom Create Company v2 API


@dataclass
class CompanyCreateV2ResPayloadData:
    token: str
    companyId: str


@dataclass
class CompanyCreateV2ResPayload:
    code: status
    message: str
    data: CompanyCreateV2ResPayloadData


Version = Literal['v1', 'v2']


def check_v1(v: Version):
    return v == 'v1'


def add_user_info(data: dict, version: Version = 'v1'):
    try:
        address = data.pop('address')
        address['name'] = data.pop('client_account_name')
        user_data = data
        user_instance, user_created = models.EasyEcomUserInfo.objects.get_or_create(**user_data)
        address_instance, addr_created = models.EasyEcomPickupAddress.objects.get_or_create(user=user_instance, **address)
        res = easyecom_amazon_registration(user_instance, address_instance, version)
        return res
    except Exception as e:
        print(str(e))
        raise Exception({"err": e})


def easyecom_amazon_registration(easyecom_user_info_obj: models.EasyEcomUserInfo, address: models.EasyEcomPickupAddress, version: Version = 'v1'):

    reg_res = create_easyecom_account(easyecom_user_info_obj, address, version)

    if ('err' in reg_res and reg_res['err']) or not status.is_success(reg_res["code"]):
        raise Exception({"source": "easyecom", **reg_res})

    easyecom_user_info_obj.token = reg_res["data"]["token"]

    token_res = generate_easyecom_api_token(easyecom_user_info_obj.email, easyecom_user_info_obj.password)

    if not status.is_success(token_res["code"]):
        raise Exception({"source": "easyecom", **token_res})

    api_token = str(token_res["data"]["api_token"])
    easyecom_user_info_obj.api_access_token = api_token
    easyecom_user_info_obj.save()
    add_carrier_to_account(easyecom_user_info_obj)
    return {"success": True}


# Ref: https://bitbucket.org/easyecom/easyecom/wiki/EasyEcomApi
def create_easyecom_account(
        easyecom_user_info_obj: models.EasyEcomUserInfo,
        address: models.EasyEcomPickupAddress,
        version: Version = 'v1'
) -> CompanyCreateV2ResPayload:

    is_v1 = check_v1(version)

    # Version wise changes. (Case sensitive)
    url = "https://api.easyecom.io/Company/Create" if is_v1 else "https://api.easyecom.io/company/V2/create"
    company_level_tax_key = 'company_level_tax_rate' if is_v1 else 'companyLevelTaxRate'

    easy_ecom_payload_address = {
        "address_line_1": address.address_line,
        "address_line_2": "",
        "state_code": address.state_code,
        "pin_code": address.pin_code,
        "country": "India"
    }

    # EasyEcom payload structure. See the link above
    data = {
        "phone": address.phone_number,
        "company_name": easyecom_user_info_obj.shop_name,
        "email": easyecom_user_info_obj.email,
        "client_id": constants.EASYECOM_PICKRR_CLIENT_ID,
        "password": easyecom_user_info_obj.password,
        "confirm_without_inventory": 1,
        company_level_tax_key: 0,
        "shipping_address": easy_ecom_payload_address,
        "billing_address": easy_ecom_payload_address

    }

    if is_v1:
        data['credentials'] = [{
            "m_id": 8,
            "seller_id": easyecom_user_info_obj.seller_id,
            "seller_user_id": easyecom_user_info_obj.seller_user_id,
            "cp_auto_create": 1
        }]

    res: CompanyCreateV2ResPayload = commonutils.make_post_request(url, data)
    return res


def add_carrier_to_account(easyecom_user_info_obj):
    try:
        payload = {
            "carrier_id": 5859,
            # "username": "dummy",
            # "password": "dummy",
            "token": constants.EASYECOM_PICKRR_CLIENT_ID
        }
        url = constants.EASYECOM_API_BASE_URL + "/Credentials/addCarrierCredentials?api_token=%s" % easyecom_user_info_obj.api_access_token
        res = commonutils.make_post_request(url, payload)
        easyecom_user_info_obj.webhook_token = res["data"]["webhookToken"]
        easyecom_user_info_obj.company_carrier_id = res["data"]["companyCarrierId"]
        easyecom_user_info_obj.save()
        return res
    except Exception as e:
        return {"err": str(e)}


def generate_easyecom_api_token(email, password):
    url = "https://app.easyecom.io/getApiToken?email=%s&password=%s" % (email, password)
    res = commonutils.make_get_request(url)
    return res


def check_v2(version):
    return version == "v2"


def easyecom_orders_service(filters, auth_token, easyecom_user_info_obj, version):

    is_v2 = check_v2(version)

    response = {}
    response['shop_name'] = easyecom_user_info_obj.shop_name
    easyecom_orders_response = pull_orders_from_easyecom(easyecom_user_info_obj, deepcopy(filters), version)
    if "data" not in easyecom_orders_response or easyecom_orders_response["code"] != 200:
        response["err"] = easyecom_orders_response
        return response
    response["page"] = -1

#   Only execute if v2
    if is_v2 and easyecom_orders_response["data"] and easyecom_orders_response["data"]["nextUrl"]:
        response["next_url"] = easyecom_orders_response["data"]["nextUrl"]

#   Extracting the orders list as per the API version
    ee_response_order_list = []
    if easyecom_orders_response["data"]:
        if is_v2 and easyecom_orders_response["data"]["orders"]:
            ee_response_order_list = easyecom_orders_response["data"]["orders"]
        else:
            ee_response_order_list = easyecom_orders_response["data"]

    if ee_response_order_list and type(ee_response_order_list) == list:
        pickup_loc_list = services.get_clients_pickup_locations_by_type(auth_token, "easyecom")
        orders = prepare_pickrr_orders_from_easyecom_orders(
            easyecom_user_info_obj, easyecom_orders_response, auth_token, pickup_loc_list, version)
        response["pickup_locations"] = pickup_loc_list
        response["page"] = filters['page'] + 1
        response.update({'orders': orders})
    else:
        response["err"] = "No order found"
        response["other"] = easyecom_orders_response
    response["page"] = -1
    return response


def pull_orders_from_easyecom(easyecom_user_info_obj, filters, version):

    is_v2 = check_v2(version)

    pull_status = easyecom_user_info_obj.pull_status
    api_token = easyecom_user_info_obj.api_access_token

#   It will only run in case of v2
    if is_v2 and 'next_url' in filters and type(filters['next_url']) == str and filters['next_url']:
        next_page_url = constants.EASYECOM_API_BASE_URL + filters['next_url']
        next_page_orders = commonutils.make_get_request(next_page_url)
        next_page_orders['auth_access_token'] = api_token
        return next_page_orders

    easyecom_orders_url = constants.EASYECOM_API_BASE_URL + \
        (f'/orders/V2/getAllOrders?marketShipped=0&limit=250&api_token={api_token}' if is_v2 else f'/orders/getAllOrders?selfShip=1&api_token={api_token}')
    # easyecom_orders_url = constants.EASYECOM_API_BASE_URL + f'/orders/getAllOrders?api_token={api_token}'

    default_date_format: str = '%Y-%m-%d'

    if not filters['date_from'] and not filters["date_to"]:
        filters['date_from'] = (datetime.now()).strftime(default_date_format)
        filters['date_to'] = (datetime.now() + timedelta(days=1)).strftime(default_date_format)
    else:
        if not filters['date_from']:
            filters['date_from'] = (datetime.now() - timedelta(days=1)).strftime(default_date_format)
        if not filters["date_to"]:
            filters['date_to'] = (datetime.now() + timedelta(days=1)).strftime(default_date_format)

        # if date_to (end_date) is received in params, so just add 1 day to get the data of requested end_date
        else:
            filters['date_to'] = (datetime.fromisoformat(filters['date_to']) +
                                  timedelta(days=1)).strftime(default_date_format)

    filters_mapping = {
        'date_from': 'start_date',
        'date_to': 'end_date'
    }
    if pull_status:
        filters_mapping['status'] = "status_id"
        filters["status"] = pull_status
    if "status" in filters and len(filters["status"]):
        filters_mapping['status'] = "status_id"

    for param in filters:
        param_name = filters_mapping.get(param, None)
        if not param_name:
            continue

        if filters.get(param):
            easyecom_orders_url += f"&{param_name}={filters[param]}"

    orders = commonutils.make_get_request(easyecom_orders_url)
    orders["auth_access_token"] = api_token
    return orders


def update_qc_easyecom(json_dict):
    url = constants.EASYECOM_API_BASE_URL + '/orders/updateQC?api_token=%s' % (json_dict["api_token"])
    try:
        data = {}
        data['invoiceId'] = json_dict['invoiceId']
        data['courier'] = json_dict['courier']
        data['awbNum'] = json_dict['awbNum']
        response = commonutils.make_post_request(url, data)
        if response['code'] == 200:
            return {'err': None, 'message': response['message']}
        else:
            return {'err': response['message'], 'message': response['message']}
    except Exception as e:
        return {"err": str(e)}


def prepare_pickrr_orders_from_easyecom_orders(user_obj, json_dict, auth_token, pickup_loc_list, version):

    is_v2 = check_v2(version)

    try:
        unplaced_orders = []
        placed_orders = []
        all_orders = []
        items = json_dict["data"]["orders"] if is_v2 else json_dict["data"]
        easyecom_order_ids = [order['reference_code'] for order in items]
        already_placed_order_ids = fetch_duplicate_client_orders(easyecom_order_ids, auth_token, 'easyecom')
        auth_access_token = json_dict["auth_access_token"]
        for order in items:
            is_skip = False
            if order["order_status"].lower() in ["shipped", "cancelled", "returned", "upcoming"]:
                is_skip = True
            if user_obj.is_multilocation:
                pass
            pickrr_dict = easyecom_to_pickrr_dict(user_obj, auth_token, order,
                                                  pickup_loc_list, auth_access_token, version)
            if order['reference_code'] in already_placed_order_ids:
                placed_orders.append(pickrr_dict.copy())
            else:
                if not is_skip:
                    unplaced_orders.append(pickrr_dict.copy())
            all_orders.append(pickrr_dict.copy())
        unplaced_orders = sorted(unplaced_orders, key=lambda k: k['created_at'], reverse=True)
        placed_orders = sorted(placed_orders, key=lambda k: k['created_at'], reverse=True)
        orders = {
            "uorders": unplaced_orders,
            "porders": placed_orders,
            "all_orders": all_orders
        }
        return orders
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        return {"err": str(e),
                "other": error}


def easyecom_to_pickrr_dict(user_obj, auth_token, json_dict, pickup_loc_list, auth_access_token, version):
    is_v2 = check_v2(version)
    order_dict = {
        "pickrr_dict": "",
        "order_id": "",
        "created_at": "",
        "platform_order_id": ""
    }
    try:
        order_dict = {
            "pickrr_dict": "",
            "order_id": str(json_dict['order_id']),
            "created_at": str(json_dict['order_date']),
            "platform_order_id": str(json_dict['order_id'])
        }
        pickrr_dict = pickup_loc_list[0].copy()
        pickrr_dict['auth_token'] = auth_token
        pickrr_dict['item_list'] = []
        items_name = ""
        quantity = 0
        wt = 0
        l, w, h = 0, 0, 0

        # Keys changed as per API version
        quantity_key = 'item_quantity' if is_v2 else 'quantity'
        contact_number_key = 'contact_num' if is_v2 else 'contactNum'

        for item in json_dict['suborders']:
            item_dict = {}
            item_dict['item_name'] = item['productName']
            if len(json_dict['suborders']) > 1:
                items_name = items_name + ', ' + (item['productName'])
            else:
                items_name = item['productName']
            item_dict['sku'] = item['sku']
            item_dict['price'] = float(item['selling_price']) / int(item[quantity_key]
                                                                    ) if int(item[quantity_key]) > 0 else item['selling_price']
            item_dict['quantity'] = item[quantity_key]
            quantity += int(item[quantity_key])
            pickrr_dict['item_list'].append(item_dict)
            try:
                wt += float(item['weight'])
            except Exception:
                wt += 0
            l = item["length"]
            w = item["width"]
            h = item["height"]
        pickrr_dict['to_name'] = str(json_dict['customer_name'])
        pickrr_dict['to_name'] = str(pickrr_dict['to_name']).replace("None", "").strip()
        pickrr_dict['to_phone_number'] = str(json_dict[contact_number_key])
        pickrr_dict['to_pincode'] = str(json_dict['pin_code'])
        pickrr_dict['to_address'] = str(
            str(json_dict['address_line_1']) + str(" ")
            + str(json_dict['address_line_2'])
            + str(" ") + str(json_dict['city']) + str(" ")
            + str(json_dict['state']))
        pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
        pickrr_dict['to_email'] = json_dict['email']
        pickrr_dict['order_time'] = json_dict['order_date']
        pickrr_dict['invoice_number'] = str(json_dict['invoice_number'])
        pickrr_dict['item_weight'] = float(wt) / 1000 if wt != 0 else 0.5
        if quantity > 1:
            pickrr_dict['item_length'] = 10
            pickrr_dict['item_height'] = 10
            pickrr_dict['item_breadth'] = 10
        else:
            pickrr_dict['item_length'] = l if l else 10
            pickrr_dict['item_height'] = h if h else 10
            pickrr_dict['item_breadth'] = w if w else 10
        pickrr_dict['item_name'] = items_name
        pickrr_dict['invoice_value'] = json_dict['total_amount']
        pickrr_dict['client_order_id'] = str(json_dict['reference_code'])
        pickrr_dict['platform_order_id'] = str(json_dict['invoice_id'])
        order_dict['platform_order_id'] = str(json_dict['invoice_id'])
        pickrr_dict['easyecom_order_id'] = str(json_dict['order_id'])
        pickrr_dict['auth_access_token'] = auth_access_token
        pickrr_dict['order_status'] = str(json_dict["order_status"])
        cod = 0.0
        if json_dict['payment_mode'].lower() == 'cod':
            cod = json_dict['total_amount']
        pickrr_dict['cod_amount'] = cod
        pickrr_dict['quantity'] = quantity
        order_dict["pickrr_dict"] = pickrr_dict
        return order_dict
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = str(exc_type) + "-> " + str(fname) + "-> " + str(exc_tb.tb_lineno)
        order_dict["pickrr_dict"] = {"err": str(e)}
        order_dict["err"] = str(e),
        order_dict["other"] = error
        return order_dict
