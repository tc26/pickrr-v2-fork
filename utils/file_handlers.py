"""
Dedicated module to handle file inputs like csv, xl etc.
"""


from plugins.errors import FileHandlerErrorCodes
from plugins.exceptions import InputFileHandlerException
import csv


class FileHandler:
    def __init__(self, file):
        self.file = file

    def validate_extension(self, extension='csv'):
        if not self.file.name.endswith(f'.{extension}'):
            raise InputFileHandlerException(f'file upload does not proper extension({extension})')


class CsvFileHandler(FileHandler):
    extension = 'csv'

    def __call__(self, re_type):
        self.validate_extension(self.extension)
        csv_data = self.file.read().decode('utf8').split('\r\n')
        payload = []
        if len(csv_data) < 1:
            raise InputFileHandlerException(
                FileHandlerErrorCodes.EMPTY_CSV
            )
        if re_type == "list":
            for row in csv_data:
                payload.append(row.split(','))
        elif re_type == "dict":
            headers = csv_data[0].split(',')
            for data in csv_data[1:]:
                item = {}

                data = data.split(',')
                for header, value in zip(headers, data):
                    item[header] = value

                payload.append(item)
        else:
            payload = csv_data

        return payload


class WriteToCSV:

    @staticmethod
    def write_list_to_csv(data):
        with open('order_data.csv', 'wb') as output_file:
            writer = csv.writer(output_file)
            for row in data:
                writer.writerow([row])
        return output_file

    @staticmethod
    def write_string_list_to_csv(data):
        with open('order_data.csv', 'w') as output_file:
            writer = csv.writer(output_file)
            for row in data:
                datum = row.split(',')
                writer.writerow(datum)
        return output_file
