# Generated by Django 2.2.8 on 2020-09-24 11:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugins', '0010_easyecompickupaddress_easyecomuserinfo'),
    ]

    operations = [
        migrations.AddField(
            model_name='eazyslipsuserinfo',
            name='channel',
            field=models.CharField(choices=[('magento_v1', 'Magento version 1'), ('magento_v2', 'Magento version 2'), ('amazon', 'Amazon'), ('prestashop', 'PrestaShop'), ('opencart', 'OpenCart'), ('woocommerce', 'WooCommerce')], default='opencart', max_length=16),
        ),
    ]
