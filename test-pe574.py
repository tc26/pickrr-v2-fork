EASYECOM_API_BASE_URL = "https://api.easyecom.io"

"""
Api token can be generated by post request on /getApiToken
with body
{
    "email":"b2caccount@gmail.com",
    "password":"abcd1234"
}
"""
API_TOKEN = "e754e595c43034f3d40d525fe614b5063f25166f44a9e8eb24245826ed31b7aa"

import logging
logger = logging.getLogger(__name__)

import requests, json

def make_post_request(url, payload, headers=None):
    if not headers:
        headers = {'content-type': 'application/json'}
    res = requests.post(url, data=json.dumps(payload), headers=headers)
    return res.json()

url = f'{EASYECOM_API_BASE_URL}/Carrier/updateTrackingStatus?api_token={API_TOKEN}'

"""
Increment `current_shipment_status_id` for every post request.
"""
payload = {
    "current_shipment_status_id": 102,
    "awb": "2"
}
res = make_post_request(url, payload)

if res['code'] > 200:
    logger.error(f'error while sending tracking status update to Easyecom, request: {payload}, response: {res}')

print(res)
