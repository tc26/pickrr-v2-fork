from rest_framework import serializers

from plugins.models import (EcwidUserInfo, EazySlipsUserInfo, MagnetoUserInfo,
                            EasyEcomUserInfo, JungleworksUserInfo,
                            OpencartUserInfo, PrestaShopUserModel,
                            BikayiUserInfo, ShopifyUserModel)


class EcwidUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = EcwidUserInfo
        fields = '__all__'


class EazySlipsUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = EazySlipsUserInfo
        fields = '__all__'


class MagnetoUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MagnetoUserInfo
        fields = '__all__'


class EasyEcomUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = EasyEcomUserInfo
        fields = '__all__'


class JungleworksUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = JungleworksUserInfo
        fields = '__all__'


class OpencartUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = OpencartUserInfo
        fields = '__all__'


class PrestaShopUserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = PrestaShopUserModel
        fields = '__all__'


class BikayiUserInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = BikayiUserInfo
        fields = '__all__'


class ShopifyUserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShopifyUserModel
        fields = '__all__'


class UpdateChannelSerializer(serializers.Serializer):
    ECWID = 'ecwid'
    EAZYSLIPS = 'eazyslips'
    MAGENTO = 'magento'
    EASYECOM = 'easyecom'
    JUNGLEWORKS = 'jungleworks'
    OPENCART = 'opencart'
    PRESTASHOP = 'prestashop'
    BIKAYI = 'bikayi'
    SHOPIFY = 'shopify'

    channel_choices = (
        (ECWID, 'Ecwid'),
        (EAZYSLIPS, 'EazySlips'),
        (MAGENTO, 'Magento'),
        (EASYECOM, 'EasyEcom'),
        (JUNGLEWORKS, 'Jungleworks'),
        (OPENCART, 'Opencart'),
        (PRESTASHOP, 'PrestaShop'),
        (BIKAYI, 'Bikayi'),
        (SHOPIFY, 'Shopify'),
    )
    channel_type = serializers.ChoiceField(choices=channel_choices,
                                           required=True)
    user_shop_name = serializers.CharField(required=True)
    user_auth_token = serializers.CharField(required=True)

    class Meta:
        fields = ['channel_type', 'user_shop_name', 'user_auth_token']

