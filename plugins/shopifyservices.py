from . import commonutils
from .constants import ZANDUCARE_SKU_WEIGHT_MAP, ZANDUCARE_PICKRR_AUTH_TOKEN, ShopifyEndpoints
import shopify
from .models import ShopifyUserModel
import requests
from datetime import datetime, timedelta
import sys, os
import time
import json
from django.core import serializers
from . import constants
from . import preparator


"""
{
            "id": 3744282476646,
            "admin_graphql_api_id": "gid://shopify/Order/3744282476646",
            "app_id": 1354745,
            "browser_ip": null,
            "buyer_accepts_marketing": false,
            "cancel_reason": null,
            "cancelled_at": null,
            "cart_token": null,
            "checkout_id": null,
            "checkout_token": null,
            "closed_at": null,
            "confirmed": true,
            "contact_email": "laxman@pickrr.com",
            "created_at": "2021-04-24T15:59:45+05:30",
            "currency": "INR",
            "current_subtotal_price": "125.00",
            "current_subtotal_price_set": {
                "shop_money": {
                    "amount": "125.00",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "125.00",
                    "currency_code": "INR"
                }
            },
            "current_total_discounts": "0.00",
            "current_total_discounts_set": {
                "shop_money": {
                    "amount": "0.00",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "INR"
                }
            },
            "current_total_duties_set": null,
            "current_total_price": "152.63",
            "current_total_price_set": {
                "shop_money": {
                    "amount": "152.63",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "152.63",
                    "currency_code": "INR"
                }
            },
            "current_total_tax": "15.63",
            "current_total_tax_set": {
                "shop_money": {
                    "amount": "15.63",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "15.63",
                    "currency_code": "INR"
                }
            },
            "customer_locale": "en",
            "device_id": null,
            "discount_codes": [],
            "email": "laxman@pickrr.com",
            "financial_status": "paid",
            "fulfillment_status": null,
            "gateway": "manual",
            "landing_site": null,
            "landing_site_ref": null,
            "location_id": null,
            "name": "#lxn1190",
            "note": "",
            "note_attributes": [],
            "number": 190,
            "order_number": 1190,
            "order_status_url": "https://harish-30.myshopify.com/13809925/orders/d7df9b173c0b15857c4cf7242e951c0e/authenticate?key=9fb931620d959b8104c8b7adf7176a61",
            "original_total_duties_set": null,
            "payment_gateway_names": [
                "manual"
            ],
            "phone": null,
            "presentment_currency": "INR",
            "processed_at": "2021-04-24T15:59:45+05:30",
            "processing_method": "manual",
            "reference": null,
            "referring_site": null,
            "source_identifier": null,
            "source_name": "shopify_draft_order",
            "source_url": null,
            "subtotal_price": "125.00",
            "subtotal_price_set": {
                "shop_money": {
                    "amount": "125.00",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "125.00",
                    "currency_code": "INR"
                }
            },
            "tags": "",
            "tax_lines": [
                {
                    "price": "15.63",
                    "rate": 0.125,
                    "title": "CGST",
                    "price_set": {
                        "shop_money": {
                            "amount": "15.63",
                            "currency_code": "INR"
                        },
                        "presentment_money": {
                            "amount": "15.63",
                            "currency_code": "INR"
                        }
                    }
                }
            ],
            "taxes_included": false,
            "test": false,
            "token": "d7df9b173c0b15857c4cf7242e951c0e",
            "total_discounts": "0.00",
            "total_discounts_set": {
                "shop_money": {
                    "amount": "0.00",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "INR"
                }
            },
            "total_line_items_price": "125.00",
            "total_line_items_price_set": {
                "shop_money": {
                    "amount": "125.00",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "125.00",
                    "currency_code": "INR"
                }
            },
            "total_outstanding": "0.00",
            "total_price": "152.63",
            "total_price_set": {
                "shop_money": {
                    "amount": "152.63",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "152.63",
                    "currency_code": "INR"
                }
            },
            "total_price_usd": "2.03",
            "total_shipping_price_set": {
                "shop_money": {
                    "amount": "12.00",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "12.00",
                    "currency_code": "INR"
                }
            },
            "total_tax": "15.63",
            "total_tax_set": {
                "shop_money": {
                    "amount": "15.63",
                    "currency_code": "INR"
                },
                "presentment_money": {
                    "amount": "15.63",
                    "currency_code": "INR"
                }
            },
            "total_tip_received": "0.00",
            "total_weight": 0,
            "updated_at": "2021-04-24T15:59:46+05:30",
            "user_id": 88062790,
            "billing_address": {
                "first_name": "Laxman",
                "address1": "kailash colony",
                "phone": "8130179898",
                "city": "delhi",
                "zip": "110065",
                "province": "Delhi",
                "country": "India",
                "last_name": "Verma",
                "address2": null,
                "company": "pickrr",
                "latitude": null,
                "longitude": null,
                "name": "Laxman Verma",
                "country_code": "IN",
                "province_code": "DL"
            },
            "customer": {
                "id": 929486536806,
                "email": "laxman@pickrr.com",
                "accepts_marketing": false,
                "created_at": "2018-12-06T02:14:30+05:30",
                "updated_at": "2021-04-24T15:59:46+05:30",
                "first_name": "Laxman",
                "last_name": "Verma",
                "orders_count": 84,
                "state": "disabled",
                "total_spent": "103534.53",
                "last_order_id": 3744282476646,
                "note": null,
                "verified_email": true,
                "multipass_identifier": null,
                "tax_exempt": false,
                "phone": null,
                "tags": "",
                "last_order_name": "#lxn1190",
                "currency": "INR",
                "accepts_marketing_updated_at": "2019-01-15T12:23:48+05:30",
                "marketing_opt_in_level": null,
                "tax_exemptions": [],
                "admin_graphql_api_id": "gid://shopify/Customer/929486536806",
                "default_address": {
                    "id": 1031085457510,
                    "customer_id": 929486536806,
                    "first_name": "Laxman",
                    "last_name": "Verma",
                    "company": "pickrr",
                    "address1": "kailash colony",
                    "address2": "",
                    "city": "delhi",
                    "province": "Delhi",
                    "country": "India",
                    "zip": "110065",
                    "phone": "8130179898",
                    "name": "Laxman Verma",
                    "province_code": "DL",
                    "country_code": "IN",
                    "country_name": "India",
                    "default": true
                }
            },
            "discount_applications": [],
            "fulfillments": [],
            "line_items": [
                {
                    "id": 9823077335142,
                    "admin_graphql_api_id": "gid://shopify/LineItem/9823077335142",
                    "fulfillable_quantity": 1,
                    "fulfillment_service": "manual",
                    "fulfillment_status": null,
                    "gift_card": false,
                    "grams": 0,
                    "name": "shorts",
                    "price": "125.00",
                    "price_set": {
                        "shop_money": {
                            "amount": "125.00",
                            "currency_code": "INR"
                        },
                        "presentment_money": {
                            "amount": "125.00",
                            "currency_code": "INR"
                        }
                    },
                    "product_exists": true,
                    "product_id": 4964133011558,
                    "properties": [],
                    "quantity": 1,
                    "requires_shipping": true,
                    "sku": "",
                    "taxable": true,
                    "title": "shorts",
                    "total_discount": "0.00",
                    "total_discount_set": {
                        "shop_money": {
                            "amount": "0.00",
                            "currency_code": "INR"
                        },
                        "presentment_money": {
                            "amount": "0.00",
                            "currency_code": "INR"
                        }
                    },
                    "variant_id": 32174282637414,
                    "variant_inventory_management": "shopify",
                    "variant_title": null,
                    "vendor": "harish-30",
                    "tax_lines": [
                        {
                            "price": "15.63",
                            "price_set": {
                                "shop_money": {
                                    "amount": "15.63",
                                    "currency_code": "INR"
                                },
                                "presentment_money": {
                                    "amount": "15.63",
                                    "currency_code": "INR"
                                }
                            },
                            "rate": 0.125,
                            "title": "CGST"
                        }
                    ],
                    "duties": [],
                    "discount_allocations": []
                }
            ],
            "refunds": [],
            "shipping_address": {
                "first_name": "Laxman",
                "address1": "kailash colony",
                "phone": "8130179898",
                "city": "delhi",
                "zip": "110065",
                "province": "Delhi",
                "country": "India",
                "last_name": "Verma",
                "address2": null,
                "company": "pickrr",
                "latitude": null,
                "longitude": null,
                "name": "Laxman Verma",
                "country_code": "IN",
                "province_code": "DL"
            },
            "shipping_lines": [
                {
                    "id": 3193839648870,
                    "carrier_identifier": null,
                    "code": "cod",
                    "delivery_category": null,
                    "discounted_price": "12.00",
                    "discounted_price_set": {
                        "shop_money": {
                            "amount": "12.00",
                            "currency_code": "INR"
                        },
                        "presentment_money": {
                            "amount": "12.00",
                            "currency_code": "INR"
                        }
                    },
                    "phone": null,
                    "price": "12.00",
                    "price_set": {
                        "shop_money": {
                            "amount": "12.00",
                            "currency_code": "INR"
                        },
                        "presentment_money": {
                            "amount": "12.00",
                            "currency_code": "INR"
                        }
                    },
                    "requested_fulfillment_service_id": null,
                    "source": "shopify",
                    "title": "cod",
                    "tax_lines": [],
                    "discount_allocations": []
                }
            ]
        }
"""


def prepare_shopify_fulfillment_data(details):
    pickrr_dict = {}
    pickrr_dict["fulfillment"] = {
        "location_id": details["location_id"],
        "tracking_number": details["tracking_number"] if "tracking_number" in details else None,
        "notify_customer": details["notify_customer"] if "notify_customer" in details else True,
        "tracking_company": details["courier_used"] if "courier_used" in details else "PICKRR"
    }
    if "tracking_number" in details:
        if "tracking_url" in details:
            pickrr_dict["fulfillment"]["tracking_url"] = details['tracking_url']
        else:
            pickrr_dict["fulfillment"]["tracking_url"] = 'https://pickrr.com/tracking/#?tracking_id=' + str(details["tracking_number"])
    return pickrr_dict


def fulfill_shopify_order(shopify_token, store_name, order_id, pickrr_dict):
    url = "https://%s.myshopify.com/admin/api/2020-04/orders/%s/fulfillments.json" % (store_name, order_id)
    headers = {
        "content-type": "application/json",
        "X-Shopify-Access-Token": shopify_token
    }
    res = commonutils.make_post_request(url, pickrr_dict, headers)
    return res


class ShopifyServiceBase:

    def __init__(self, user_obj):
        self.base_params = {
            'token': user_obj.token
        }
        self.base_headers = {'Output-Format': 'JSON'}
        self.user_obj = user_obj
        self.client_order_status_mapping = {
            'RTD': self.user_obj.rtd,
            'RTO': self.user_obj.rto,
            'DL': self.user_obj.delivered,
            'OT': self.user_obj.transit,
            'PP': self.user_obj.picked_up,
            'OC': self.user_obj.cancelled
        }

    def pull_orders(self, url, shopify_token):
        try:
            headers = {
                "content-type": "application/json",
                "X-Shopify-Access-Token": shopify_token
            }
            response = requests.get(url, headers=headers)
            if "link" in response.headers:
                try:
                    if len(response.headers["link"].split(",")) > 0:
                        link = response.headers["link"].split(",")[1]
                    else:
                        link = response.headers["link"]
                except:
                    link = response.headers["link"]
                if link.split("; ")[1] == 'rel="next"':
                    next_url = link.split(";")[0].replace(" ", "")[1:-1]
                else:
                    next_url = None
            else:
                next_url = None
            return response.json(), next_url
        except json.decoder.JSONDecodeError:
            return {"err": response.text}, None
        except Exception as e:
            return {"err": str(e)}, None

    def get_store_details(self, store_name, shopify_token):
        try:
            url = "https://%s.myshopify.com/admin/api/2020-01/shop.json?" % (store_name)
            headers = {
                "content-type": "application/json",
                "X-Shopify-Access-Token": shopify_token
            }
            response = requests.get(url, headers=headers).json()
            return response
        except Exception as e:
            return {"err": str(e)}

    def get_all_locations(self, shopify_token, store_name):
        try:
            url = "https://%s.myshopify.com/admin/api/2020-04/locations.json" % (store_name)
            headers = {
                "content-type": "application/json",
                "X-Shopify-Access-Token": shopify_token
            }
            response = requests.get(url, headers=headers).json()
            if response["locations"]:
                return response
            else:
                return {"err": "No Location Found."}
        except Exception as e:
            return {"err": str(e)}

    def get_location(slef, shopify_token, store_name, location_id):
        """
        "location": {
            "id": 34639446118,
            "name": "kailash colony new",
            "address1": "138 ground floor",
            "address2": "Sant nagar",
            "city": "New Delhi",
            "zip": "110084",
            "province": "DL",
            "country": "IN",
            "phone": "+918512838980",
            "created_at": "2020-01-14T15:40:26+05:30",
            "updated_at": "2020-01-14T15:40:26+05:30",
            "country_code": "IN",
            "country_name": "India",
            "province_code": "DL",
            "legacy": false,
            "active": true,
            "admin_graphql_api_id": "gid://shopify/Location/34639446118"
        }
        :param shopify_token:
        :param store_name:
        :param location_id:
        :return:
        """
        try:
            url = "https://%s.myshopify.com/admin/api/2020-01/locations/%s.json" % (store_name, location_id)
            headers = {
                "content-type": "application/json",
                "X-Shopify-Access-Token": shopify_token
            }
            response = requests.get(url, headers=headers).json()
            return response["location"]
        except Exception as e:
            return {"err": str(e)}

    def fetch_shopify_default_address(self, store_name, auth_token, token):
        try:
            # addr = ShopifyAdd.objects.get(
            #     shop_name=store_name, auth_token=auth_token, customer_secret=token)
            # return dict(from_name=addr.shop_name, from_phone_number=addr.phone_number,
            #             from_pincode=addr.pincode, from_address=addr.address, location_id=addr.customer_key)
            return []
        except:
            return None

    def modify_pickrr_dict(self, pickrr_dict):
        if pickrr_dict['auth_token'] in [ZANDUCARE_PICKRR_AUTH_TOKEN]:
            pass
        return pickrr_dict


class ShopifyOrderFetchService(ShopifyServiceBase):

    def prepare_pickrr_dict(self, shopify_token, store_name, store_details, pickrr_auth_token, order, locations={},
                            default_add=None):
        try:
            pickrr_dict = {}
            item_name = ""
            total_quantity = 0
            item_list = []
            shopify_order_dict = order
            token = shopify_token.token
            phone_number = store_details['shop']['phone']
            TEMP_WT = 0
            items_removed = []
            if "refunds" in shopify_order_dict:
                for refunds in shopify_order_dict["refunds"]:
                    items_removed = [ids["line_item_id"] for ids in refunds["refund_line_items"]]
            for index in range(0, len(shopify_order_dict["line_items"])):
                if int(shopify_order_dict["line_items"][index]["fulfillable_quantity"]) == 0:
                    continue
                if shopify_order_dict["line_items"][index]["id"] in items_removed:
                    continue
                item_name = item_name + str(shopify_order_dict["line_items"][index]["title"]) + " - " + str(
                    shopify_order_dict["line_items"][index]["variant_title"]) + " : " + str(
                    shopify_order_dict["line_items"][index]["sku"]) + " x " + str(
                    shopify_order_dict["line_items"][index]["quantity"]) + str(", ")
                try:
                    total_quantity += int(shopify_order_dict["line_items"][index]["quantity"])
                except:
                    pass
                try:
                    d = {
                        "quantity": int(shopify_order_dict["line_items"][index]["quantity"]),
                        "item_name": str(shopify_order_dict["line_items"][index]["title"]),
                        "sku": str(shopify_order_dict["line_items"][index]["sku"]),
                        "price": float(shopify_order_dict["line_items"][index]["price"]),
                    }
                    try:
                        rate = 0
                        for tax in shopify_order_dict["line_items"][index]["tax_lines"]:
                            rate += tax["rate"]
                        d["item_tax_percentage"] = rate * 100
                        d["variant_title"] = str(shopify_order_dict["line_items"][index]["variant_title"])
                        if len(d["variant_title"]) > 0:
                            d["sku"] += " | " + d["variant_title"]
                    except:
                        pass
                    item_list.append(d.copy())
                except:
                    pass
                if pickrr_auth_token in [ZANDUCARE_PICKRR_AUTH_TOKEN]:
                    sku = str(shopify_order_dict["line_items"][index]["sku"])
                    try:
                        quantity_per_line_item = int(shopify_order_dict["line_items"][index]["quantity"])
                    except:
                        quantity_per_line_item = 1
                    if sku in ZANDUCARE_SKU_WEIGHT_MAP:
                        TEMP_WT += ZANDUCARE_SKU_WEIGHT_MAP[sku] * quantity_per_line_item
                    else:
                        TEMP_WT += 0.5 * quantity_per_line_item
            item_list.sort(key=lambda item: item['price'], reverse=True)
            pickrr_dict['item_list'] = item_list
            pickrr_dict['auth_token'] = str(pickrr_auth_token)
            pickrr_dict['total_discount'] = shopify_order_dict['total_discounts']
            pickrr_dict['item_weight'] = 0.5 if (float(shopify_order_dict['total_weight']) / 1000.0) < 0.5 else float(
                shopify_order_dict['total_weight']) / 1000.0
            if pickrr_auth_token in [ZANDUCARE_PICKRR_AUTH_TOKEN] and TEMP_WT:
                pickrr_dict['item_weight'] = round((TEMP_WT * 1.1), 3)
            pickrr_dict['item_length'] = 10
            pickrr_dict['item_height'] = 10
            pickrr_dict['item_breadth'] = 10
            pickrr_dict['invoice_number'] = "retail" + str(shopify_order_dict['number'])
            pickrr_dict['item_name'] = item_name[:-2][0:250]
            pickrr_dict['order_time'] = str(datetime.now()) + "T" + str(time.strftime('%H:%M'))
            pickrr_dict['from_name'] = str(store_details['shop']['name'])
            pickrr_dict['from_phone_number'] = str(phone_number)
            pickrr_dict['from_pincode'] = str(store_details['shop']['zip'])
            pickrr_dict['from_address'] = str(
                store_details['shop']['address1'] + " " + str(store_details['shop']['address2']))
            location_id = None

            try:
                pickrr_dict["location_id"] = str(store_details['shop']['primary_location_id'])
            except:
                try:
                    pickrr_dict["location_id"] = str(locations["locations"][0]["id"])
                except:
                    pickrr_dict["location_id"] = location_id
            # if store_name in ["tatvahealth"]:
            #     try:
            #         if len(locations["locations"]) > 0:
            #             location_dict = locations["locations"][0]
            #             pickrr_dict['from_name'] = str(location_dict['name'])
            #             pickrr_dict['from_phone_number'] = str(location_dict['phone'])
            #             pickrr_dict['from_pincode'] = str(location_dict['zip'])
            #             pickrr_dict['from_address'] = str(
            #                 location_dict['address1'] + " " + str(location_dict['address2']))
            #             pickrr_dict["location_id"] = str(location_dict["id"])
            #     except:
            #         pass
            try:
                if shopify_token.is_multiple_location:
                    location_id = str(shopify_order_dict["fulfillments"][0]["location_id"])
                    if location_id and len(location_id) > 0:
                        location_dict = self.get_location(token, store_name, location_id)
                        # if location_dict['active']:
                        #     pass
                        pickrr_dict['from_name'] = str(location_dict['name'])
                        pickrr_dict['from_phone_number'] = str(location_dict['phone'])
                        pickrr_dict['from_pincode'] = str(location_dict['zip'])
                        pickrr_dict['from_address'] = str(
                            location_dict['address1'] + " " + str(location_dict['address2']))
            except:
                pass

            if location_id and len(location_id) > 0:
                pickrr_dict["location_id"] = location_id
            if total_quantity:
                pickrr_dict['quantity'] = total_quantity
            else:
                pass

            if default_add:
                pickrr_dict['from_name'] = default_add['from_name']
                pickrr_dict['from_phone_number'] = default_add["from_phone_number"]
                pickrr_dict['from_pincode'] = default_add['from_pincode']
                pickrr_dict['from_address'] = default_add["from_address"]
                pickrr_dict['location_id'] = default_add["location_id"]
            try:
                pickrr_dict['to_name'] = str(shopify_order_dict['shipping_address']['name'])
            except:
                pickrr_dict['to_name'] = ""

            try:
                phone = str(shopify_order_dict["shipping_address"]["phone"])
                pickrr_dict['to_phone_number'] = commonutils.cleanphonenumber(phone)
            except:
                pickrr_dict['to_phone_number'] = ""

            try:
                pickrr_dict['to_pincode'] = str(shopify_order_dict["shipping_address"]["zip"])
            except:
                pickrr_dict['to_pincode'] = ""
            try:
                pickrr_dict['to_email'] = str(shopify_order_dict["customer"]["email"])
            except:
                pickrr_dict['to_email'] = ""

            try:
                pickrr_dict['to_address'] = str(str(shopify_order_dict["shipping_address"]["company"]).encode('ascii',
                                                                                                          'ignore').decode("utf-8")) + " " + str(str(
                    shopify_order_dict["shipping_address"]["address1"]).encode('ascii', 'ignore').decode("utf-8")) + " " + str(str(
                    shopify_order_dict["shipping_address"]["address2"]).encode('ascii', 'ignore').decode("utf-8")) + " " + str(
                    shopify_order_dict["shipping_address"]["city"]) + " " + str(
                    shopify_order_dict["shipping_address"]["country"])
                pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
            except:
                pickrr_dict['to_address'] = ""
            if not pickrr_dict['to_address']:
                try:
                    pickrr_dict['to_address'] = str((
                        shopify_order_dict["billing_address"]["company"]
                    ).encode(
                        'ascii', 'ignore'
                    ).decode("utf-8")) if shopify_order_dict["billing_address"]["company"] else ""
                    pickrr_dict['to_address'] += " " + str((
                        shopify_order_dict["billing_address"]["address1"]
                    ).encode('ascii', 'ignore').decode("utf-8")) + " "
                    pickrr_dict['to_address'] += str((
                        shopify_order_dict["billing_address"]["address2"]
                    ).encode('ascii', 'ignore').decode("utf-8")) if shopify_order_dict["billing_address"]["address2"] else ""
                    pickrr_dict['to_address'] += " " + str(
                        shopify_order_dict["billing_address"]["city"]) + " " + str(
                        shopify_order_dict["billing_address"]["country"])
                    pickrr_dict['to_address'] = str(pickrr_dict['to_address']).replace("None", "").strip()
                    pickrr_dict['to_name'] = str(shopify_order_dict['billing_address']['first_name'])
                    try:
                        pickrr_dict['to_pincode'] = str(shopify_order_dict["billing_address"]["zip"])
                    except:
                        str(shopify_order_dict["customer"]["default_address"]["zip"])
                    phone = str(shopify_order_dict["billing_address"]["phone"]) if "phone" in shopify_order_dict["billing_address"] else str(shopify_order_dict["phone"])
                    pickrr_dict['to_phone_number'] = commonutils.cleanphonenumber(phone)
                except:
                    pass

            total_price = float(shopify_order_dict["total_price"])
            if len(items_removed):
                total_price = float(shopify_order_dict["current_total_price"])
            pickrr_dict['cod_amount'] = total_price
            pickrr_dict['invoice_value'] = total_price
            try:
                pickrr_dict['client_order_id'] = str(shopify_order_dict["name"])
            except:
                pickrr_dict['client_order_id'] = str(shopify_order_dict["id"])

            try:
                pickrr_dict['shopify_order_id'] = str(shopify_order_dict["id"])
            except:
                pickrr_dict['shopify_order_id'] = ""

            if str(shopify_order_dict['financial_status']) == str("paid"):
                pickrr_dict['cod_amount'] = 0.0
            elif str(shopify_order_dict['financial_status']) == str("partially_paid"):
                session = shopify.Session("%s.myshopify.com" % (store_name), str(token))
                shopify.ShopifyResource.activate_session(session)
                shobj = shopify.Order.find(shopify_order_dict['id'])
                transobj = shobj.transactions()
                trandata = json.loads(transobj[0].to_json())
                if trandata['transaction']['status'] == str("success"):
                    pickrr_dict['cod_amount'] = float(
                        shopify_order_dict["total_price"]) - float(
                        str((trandata['transaction']['amount'])))
            pickrr_dict["shipping_charge"] = 0
            try:
                pickrr_dict["shipping_charge"] = shopify_order_dict["total_shipping_price_set"]["shop_money"]["amount"]
            except:
                pass
            jp = json.dumps(pickrr_dict)
            pickrr_dict = self.modify_pickrr_dict(pickrr_dict)
            return pickrr_dict
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = str(exc_type) + "->" + str(fname) + "->" + str(exc_tb.tb_lineno) + "->" + str(e)
            return {"err": str(error) + "having shopify order id: " + str(order["id"])}

    def fetch_orders(self, params):
        try:
            response = {
                'shop_name': self.user_obj.shop_name,
                'err': None,
                'pickup_locations': [],
                'orders': None,
                'other': None
            }
            context = {}
            days = params["days"]
            store_name = self.user_obj.shop_name
            pickrr_auth_token = self.user_obj.auth_token
            token_obj = self.user_obj
            token = token_obj.token
            created_at_min = str(
                (datetime.now() - timedelta(days=int(days))).date()) + "T00:00%2B05:30"  # + str(time.strftime('%H:%M'))
            url = "https://%s.myshopify.com/admin/api/2020-01/orders.json?limit=250&status=any&created_at_min=%s" % (
            store_name, created_at_min)
            next_url_flag = False
            next_url = None
            porders = []
            uorders = []
            allorders = []
            uorders_id = []
            error_orders = []
            if "next_url" in params and params["next_url"]:
                url = params["next_url"]
                next_url_flag = True
            store_orders, next_url = self.pull_orders(url, token)

            if "err" in store_orders:
                return {"err": store_orders}

            store_details = self.get_store_details(store_name, token)
            if not bool(store_details) and not bool(store_details["shop"]):
                return {"err": " Store Not Found"}
            locations = self.get_all_locations(token, store_name)
            default_add = None
            if not self.user_obj.is_multilocation:
                default_add = self.fetch_shopify_default_address(store_name, pickrr_auth_token, token)
            if "orders" in store_orders and len(store_orders["orders"]):
                orders = store_orders["orders"]
                order_ids = []
                for orderids in orders:
                    # order_ids.append(str(orderids["id"])+str(store_name[0:3]))
                    order_ids.append(str(orderids["name"]) + str(store_name[0:3]))
                # duplicate_ids = list(ShopifyID.objects.filter(
                #     order_id__in=order_ids).values_list('order_id', flat=True))
                id_list = []
                for order in orders:
                    d = {}
                    try:
                        if order["cancelled_at"]:
                            continue
                        d = {
                            "shopify_order_id": order["id"],
                            "created_at": order["created_at"]
                        }
                        id_list.append(str(order['name']))
                        pickrr_dict = self.prepare_pickrr_dict(
                            token_obj, store_name, store_details, pickrr_auth_token, order, locations, default_add)
                        d["pickrr_dict"] = pickrr_dict
                        d["is_fulfilled"] = False
                        """
                        and (
                                "tracking_number" in order["fulfillments"][0]) and (
                                order["fulfillments"][0]["tracking_number"])) or (
                            (str(order["name"]) + str(store_name[0:3])) in duplicate_ids))
                        """
                        if len(order["fulfillments"]):
                            try:
                                d["tracking_number"] = order["fulfillments"][0]["tracking_number"]
                                d["tracking_url"] = order["fulfillments"][0]["tracking_url"]
                            except:
                                d["tracking_number"] = None
                                d["tracking_url"] = None
                            d["is_fulfilled"] = True
                            porders.append(d.copy())
                        else:
                            uorders_id.append(str(order["id"]))
                            uorders.append(d.copy())
                        allorders.append(d.copy())
                    except Exception as e:
                        d["err"] = str(e)
                        error_orders.append(d)
                context["uorders"] = uorders
                context["porders"] = porders
                context["allorders"] = allorders
                context["error_orders"] = error_orders
                context["uorders_id_list"] = uorders_id
                context["id_list"] = id_list
                context["uorders_id"] = ','.join(uorders_id)
            response.update({
                'orders': context,
                'next_url': next_url
            })

            return response
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = str(exc_type) + "->" + str(fname) + "->" + str(exc_tb.tb_lineno) + "->" + str(e)
            return {"err": str(error)}


def fetch_shopify_clients() -> list:
    url = constants.PICKRR_HEROKU_BASE + constants.PickrrEndpoints.FETCH_SHOPIFY_CLIENTS.value
    res = requests.get(url)

    if res.status_code != 200:
        raise Exception(
            'Fetching client details failed',
            f"status code: {res.status_code}",
            f"message: {res.json()}"
        )

    users = res.json().get('clients', [])

    if not users:
        raise Exception(
            'No users found',
            f"status code: {res.status_code}",
            "message: {'clients': []}"
        )

    return users


def create_data(user_data: dict, suffix_counter: list) -> dict:
    if user_data['pickrr_auth_token'] == '<ENTER-PICKRR-AUTH-TOKEN>':
        user_data['pickrr_auth_token'] = user_data['pickrr_auth_token'] + str(suffix_counter[0])
        suffix_counter[0] += 1

    return {
                'shop_name': user_data['shop_name'],
                'token': user_data['token'],
                'email': 'email not available' if not user_data['failure_emails'] else user_data['failure_emails'],
                'website_url': 'website not available',
                'auth_token': user_data['pickrr_auth_token'],
                'update_tracking_id': True,
                'update_tracking_status': True,
                'is_multilocation': user_data['is_multiple_location'],
                'update_cod_status': False,
                'pickrr_accounts': f"{user_data['failure_emails']}:{user_data['pickrr_auth_token']}",
            }


def create_entry_in_shopify_user_model(**data):
    user_obj = ShopifyUserModel(**data)
    user_obj.save()
    return user_obj


def update_platform_token_on_pickrr_is_success(data: dict) -> bool:
    url = constants.PICKRRTESTING_BASE_URL + constants.PickrrEndpoints.UPDATE_PLATFORM_TOKEN.value
    payload = preparator.get_pickrr_platform_token_update_payload(data, platform='shopify')
    res = requests.post(url, headers={'content-type': 'application/json'}, data=json.dumps(payload))

    if res.status_code != 200:
        return False

    return True


def sync_from_heroku():
    users, users_not_updated_on_pickrr = list(), list()
    suffix_counter = [0]

    for user_data in fetch_shopify_clients():
        skip_creation = False
        user_obj = ""
        try:
            if ShopifyUserModel.objects.get(shop_name=user_data['shop_name']):
                skip_creation = True
        except:
            pass
        data = create_data(user_data, suffix_counter)
        if skip_creation:
            try:
                user_obj = create_entry_in_shopify_user_model(**data)
            except:
                pass

        if not update_platform_token_on_pickrr_is_success(data):
            users_not_updated_on_pickrr.append(user_obj)
        else:
            users.append(user_obj)

    return {
        'users': {
            'on_pickrr': serializers.serialize('json', users),
            'not_on_pickrr': serializers.serialize('json', users_not_updated_on_pickrr),
        }
    }


class AddTagsInOrderService(ShopifyServiceBase):
    def get_prev_tags(self, request_data: dict) -> str:
        url = ShopifyEndpoints.BASE.value.format(shop_name=self.user_obj.shop_name) + \
              ShopifyEndpoints.ORDERS.value.format(order_id=request_data['order_id'])

        query_params = {
            'fields': 'tags'
        }

        self.base_headers.update({
            "content-type": "application/json",
            "X-Shopify-Access-Token": self.user_obj.token,
        })

        response = commonutils.CustomRequestBase.make_request(
            'GET', url, headers=self.base_headers, query_params=query_params)

        return response['order']['tags']

    def add_tags(self, request_data: dict):
        request_data['tags'] = self.get_prev_tags({'order_id': request_data['order_id']}) + ', ' + request_data['tags']
        url = ShopifyEndpoints.BASE.value.format(shop_name=self.user_obj.shop_name) + \
              ShopifyEndpoints.ORDERS.value.format(order_id=request_data['order_id'])

        payload = {
            'order': {
                'id': request_data['order_id'],
                'tags': request_data['tags'],
            }
        }

        self.base_headers.update({
            "content-type": "application/json",
            "X-Shopify-Access-Token": self.user_obj.token,
        })

        response = commonutils.CustomRequestBase.make_request(
            'PUT', url, headers=self.base_headers, payload=json.dumps(payload))
        return response
