# Generated by Django 2.2.8 on 2021-05-25 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugins', '0031_auto_20210521_1510'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopifyusermodel',
            name='auto_place_order',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='shopifyusermodel',
            name='pre_fetch',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='shopifyusermodel',
            name='website_url',
            field=models.CharField(blank=True, help_text='website domain. For eg: https://my-site.shopify.com/', max_length=255, null=True),
        ),
    ]
