from django.contrib import admin
from . import models


class EcwidUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class PickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'is_default')


class EazySlipsUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class EazySlipsPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'is_default')


class MagnetoUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class MagentoPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'is_default')


class EasyEcomUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class EasyEcomPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'is_default')


class JungleworksUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class OpencartUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class BikayiUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token', 'email')
    search_fields = ('shop_name', 'auth_token', 'email')


class RemoteAddressTrackerAdmin(admin.ModelAdmin):
    list_display = ('ip', 'remarks', 'traffic', 'daily_traffic', 'is_active')
    list_display_links = ('ip', 'remarks', 'traffic', 'daily_traffic', 'is_active')
    ordering = ('-traffic',)
    search_fields = ('ip', 'remarks')
    list_filter = ('is_active',)


class PrestaShopUserModelAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token', 'email')


class PrestaShopPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'address_line', 'email')


class ShopifyUserModelAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token', 'email')
    search_fields = ('shop_name', 'auth_token', 'email')


class ShopifyPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'address_line', 'email')


class ZohoUserModelAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token')
    search_fields = ('shop_name', 'auth_token', 'email')


class ZohoPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'is_default')


class WixUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token', 'email')
    search_fields = ('shop_name', 'auth_token', 'email')


class WixPickupAddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'address_line', 'email')


class WoocommUserInfoAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'auth_token', 'email')
    search_fields = ('shop_name', 'auth_token', 'email')


admin.site.register(models.EcwidUserInfo, EcwidUserInfoAdmin)
admin.site.register(models.PickupAddress, PickupAddressAdmin)
admin.site.register(models.EazySlipsUserInfo, EazySlipsUserInfoAdmin)
admin.site.register(models.EazySlipsPickupAddress, EazySlipsPickupAddressAdmin)
admin.site.register(models.MagnetoUserInfo, MagnetoUserInfoAdmin)
admin.site.register(models.MagentoPickupAddress, MagentoPickupAddressAdmin)
admin.site.register(models.EasyEcomPickupAddress, EasyEcomPickupAddressAdmin)
admin.site.register(models.EasyEcomUserInfo, EasyEcomUserInfoAdmin)
admin.site.register(models.JungleworksUserInfo, JungleworksUserInfoAdmin)
admin.site.register(models.OpencartUserInfo, OpencartUserInfoAdmin)
admin.site.register(models.BikayiUserInfo, BikayiUserInfoAdmin)
admin.site.register(models.RemoteAddressTracker, RemoteAddressTrackerAdmin)
admin.site.register(models.PrestaShopUserModel, PrestaShopUserModelAdmin)
admin.site.register(models.PrestaShopPickupAddress,
                    PrestaShopPickupAddressAdmin)
admin.site.register(models.ShopifyUserModel, ShopifyUserModelAdmin)
admin.site.register(models.ShopifyPickupAddress, ShopifyPickupAddressAdmin)
admin.site.register(models.ZohoUserModel, ZohoUserModelAdmin)
admin.site.register(models.ZohoPickupAddress, ZohoPickupAddressAdmin)
admin.site.register(models.WixUserInfo, WixUserInfoAdmin)
admin.site.register(models.WixPickupAddress, WixPickupAddressAdmin)
admin.site.register(models.WoocommUserInfo, WoocommUserInfoAdmin)
