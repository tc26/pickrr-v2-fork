import logging

import json
import requests

from .models import UserModel
from .endpoints import BASE_URL

logger = logging.getLogger(__name__)


def authorization_header(access_token):
    header = {
        'Authorization': 'Bearer {}'.format(access_token)
    }
    return header


def response_dict(is_success, data, message):
    return {'is_success': is_success,
            'data': data,
            'msg': message}


def make_instamojo_request(endpoint, headers, payload, request_method):
    url = "{}{}".format(BASE_URL, endpoint)
    try:
        if request_method == 'GET':
            response = requests.get(url, headers=headers, params=payload, timeout=5)
        if request_method == 'POST':
            response = requests.post(url, data=payload, headers=headers, timeout=5)
        if request_method == 'PATCH':
            response = requests.patch(url, data=json.dumps(payload), headers=headers, timeout=5)
        json_response = response.json()
    except Exception as e:
        logger.exception('Error while making request to {}'.format(endpoint))
        logger.exception("{}".format(e))
        return response_dict(False, None, str(e))
    else:
        if 'error' in json_response:
            return response_dict(False, None, json_response)
        if json_response.get('success','') == False:
            return response_dict(False, None, json_response['message'])
        return response_dict(True, json_response, None)
