from rest_framework import generics, status, views
from rest_framework.response import Response
from . import commonutils

from . import models, services, dao


class OpencartFetchOrderApi(views.APIView):

    def get_filters(self, request):
        #page = int(request.query_params.get('page', 1))
        #status = request.query_params.get('status', '')
        date_from = request.query_params.get('date_from', '')
        date_to = request.query_params.get('date_to', '')

        filters = {
            'date_from': date_from,
            'date_to': date_to
        }
        # if status:
        #     filters['status'] = status

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        opencart_user_info_obj = dao.get_opencart_user_info(auth_token, shop_name)

        if not opencart_user_info_obj:
            return Response({
                'error': 'User has no associated store'
                },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        orders = services.get_opencart_orders_service(filters, auth_token, opencart_user_info_obj)

        return Response(orders, status=status.HTTP_200_OK)


def get_client_opencart_status(opencart_user_obj, order_status):
    current_status = "pickrr_shipped"
    if order_status == 'RTD':
        current_status = opencart_user_obj.rtd
    elif order_status == 'RTO':
        current_status = opencart_user_obj.rto
    elif order_status == 'DL':
        current_status = opencart_user_obj.delivered
    elif order_status == 'OT':
        current_status = opencart_user_obj.transit
    elif order_status == 'PP':
        current_status = opencart_user_obj.picked_up
    elif order_status == 'OC':
        current_status = opencart_user_obj.cancelled
    else:
        pass
    return current_status


class OpencartUpdateOrderStatus(views.APIView):

    def post(self, request):
        post_data = request.data
        auth_token = post_data.get('auth_token')
        order_id = post_data.get('order_id')
        order_status = post_data.get('status')
        opencart_user_obj = models.OpencartUserInfo.objects.get(
            auth_token=auth_token)
        opencart_status = get_client_opencart_status(opencart_user_obj, order_status)
        key1 = opencart_user_obj.key1
        key2 = opencart_user_obj.key2
        url = opencart_user_obj.website_url + "/index.php?route=extension/module/pickrr/history"
        data = {
                "key_1": str(key1),
                "key_2": str(key2),
                "order_id": str(order_id),
                "order_status": str(opencart_status),
                "notify": 1
            }
        headers = {'content-type': 'application/json'}
        response = commonutils.make_post_request(url, data, headers)
        return Response(response, status=status.HTTP_200_OK)


class OpenCartUpdateTracking(views.APIView):

    def post(self, request):
        try:
            post_data = request.data
            order_id = post_data.get('order_id')
            tracking_id = post_data.get('tracking_id')
            tracking_url = post_data.get('tracking_url')
            pickrr_auth_token = post_data.get('auth_token')
            response = services.update_tracking_id_to_opencart(
                order_id, tracking_id, tracking_url, pickrr_auth_token)
            return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"err": str(e)}, status=status.HTTP_200_OK)
