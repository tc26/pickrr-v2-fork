# Generated by Django 2.2.8 on 2020-10-01 07:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugins', '0011_eazyslipsuserinfo_channel'),
    ]

    operations = [
        migrations.AddField(
            model_name='easyecomuserinfo',
            name='company_carrier_id',
            field=models.CharField(blank=True, help_text='Easyecom Company Carrier Id', max_length=16, null=True),
        ),
        migrations.AddField(
            model_name='easyecomuserinfo',
            name='webhook_token',
            field=models.CharField(blank=True, help_text='Easyecom Webhook Token', max_length=255, null=True),
        ),
    ]
