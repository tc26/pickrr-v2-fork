from plugins.instamojo.models import UserModel as InstaMojoUserModel

from . import models


def get_user_ecwid_info(auth_token, store_id):
    try:
        user_info = models.EcwidUserInfo.objects.get(auth_token=auth_token, store_id=store_id)

    except models.EcwidUserInfo.DoesNotExist:
        return None

    return user_info


def get_all_ecwid_stores_for_user(auth_token):
    ecwid_stores = models.EcwidUserInfo.objects.filter(auth_token=auth_token)

    if not ecwid_stores:
        return None

    res = []
    for store in ecwid_stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'store_id': store.store_id,
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_magento_stores_for_user(auth_token):
    stores = models.MagnetoUserInfo.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_eazyslips_stores_for_user(auth_token):
    eazyslips_stores = models.EazySlipsUserInfo.objects.filter(auth_token=auth_token)

    if not eazyslips_stores:
        return None

    res = []
    for store in eazyslips_stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_ecwid_stores_users_data():
    ecwid_users = models.EcwidUserInfo.objects.all().values()

    if not ecwid_users:
        return []

    users_data = {}

    for ecwid_user in ecwid_users:
        auth_token = ecwid_user['auth_token']
        user = users_data.get(auth_token)
        if not user:
            user = {
                   'store_info': []
            }
            users_data[auth_token] = user

        store_info = {
            'store_id': ecwid_user.get('store_id'),
            'store_private_token': ecwid_user.get('store_private_token'),
            'shop_name': ecwid_user.get('shop_name'),
            'website_url': ecwid_user.get('website_url')
        }
        users_data[auth_token]['store_info'].append(store_info)

    return users_data


def get_all_easyecom_stores_for_user(auth_token):
    stores = models.EasyEcomUserInfo.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_jungleworks_stores_for_user(auth_token):
    jungleworks_stores = models.JungleworksUserInfo.objects.filter(auth_token=auth_token)

    if not jungleworks_stores:
        return None

    res = []
    for store in jungleworks_stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_opencart_stores_for_user(auth_token):
    stores = models.OpencartUserInfo.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_prestashop_stores_for_user(auth_token):
    """
    Retrieve all prestashop stores

    TO-DO: Move this along with all accessor functions to a common method, basically doing same thing
    """

    stores = models.PrestaShopUserModel.objects.filter(auth_token=auth_token)

    response = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            account = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(account)
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        response.append(store_info)

    return response


def get_all_bikayi_stores_for_user(auth_token):
    stores = models.BikayiUserInfo.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': "",
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_shopify_stores_for_user(auth_token):
    stores = models.ShopifyUserModel.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': "",
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts,
        }
        res.append(store_info)

    return res

def get_all_magento1_stores_for_user(auth_token):
    stores = models.MagnetoUserInfo.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_wix_stores_for_user(auth_token):
    """
    This function returns a dictionary containing user's all stores info on Wix
    """

    stores = models.WizUserInfo.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts,
        }
        res.append(store_info)

    return res

def get_all_zoho_stores_for_user(auth_token):
    stores = models.ZohoUserModel.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res


def get_all_instamojo_stores_for_user(auth_token):
    stores = InstaMojoUserModel.objects.filter(auth_token=auth_token)

    if not stores:
        return None

    res = []
    for store in stores:
        accounts = []
        for acc in store.pickrr_accounts.split(','):
            d = {
                "name": acc.split(':')[0],
                "auth_token": acc.split(':')[1]
            }
            accounts.append(d.copy())
        store_info = {
            'email': store.email,
            'shop_name': store.shop_name,
            'website_url': store.website_url,
            'auth_token': store.auth_token,
            'pickrr_accounts': accounts
        }
        res.append(store_info)

    return res
