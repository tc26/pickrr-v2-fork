from rest_framework import generics, status, views
from rest_framework.response import Response
from utils import permissions as custom_permissions

from . import accessors, models, serializers, services, dao


class EazySlipsFetchOrderApi(views.APIView):
    """
    API to fetch orders from eazyslips servers.
    """
    # permission_classes = [custom_permissions.IsTokenVerified]

    def get_filters(self, request):
        page = int(request.query_params.get('page', 1))
        status = request.query_params.get('status', '')
        date_from = request.query_params.get('date_from', '')
        date_to = request.query_params.get('date_to', '')

        filters = {
            'page': page,
            'date_from': date_from,
            'date_to': date_to
        }
        if status:
            filters['status'] = status

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        shop_name = request.query_params.get('shop_name')
        eazyslips_user_info_obj = dao.get_eazyslip_user_info(auth_token, shop_name)

        if not eazyslips_user_info_obj:
            return Response({
                'error': 'User has no associated store'
                },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        absolute_url = self.request.build_absolute_uri()  # Used for handling pagination
        orders = services.get_eazyslips_orders_service(filters, absolute_url, auth_token, eazyslips_user_info_obj)

        return Response(orders, status=status.HTTP_200_OK)
