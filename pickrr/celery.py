import os 
from celery import Celery
from django.conf import settings

# set the Django settings for celery programs
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pickrr.settings")

app = Celery("Pickrr")

# Using a string as the first argument in config_from_object means that
# the worker doesnot have to serialize the config object to child processes.

# Using the namespace = "CELERY". So every celery related setting should
# have a `CELERY_` prefix

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# Since celery will auto create queue and exchange
# We will set a default queue name and exchange name to avoid conflict with others

app.conf.task_default_exchange = "Pickkr.Celery.Exchange"
app.conf.task_default_queue = "pickrr_default"
app.conf.task_default_routing_key= "pickrr_default_routing"