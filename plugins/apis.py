import logging

from django.core.exceptions import FieldDoesNotExist
from rest_framework.exceptions import NotFound
from mongo import services as mongo_services
from rest_framework import generics, status, views
from rest_framework.response import Response
from utils import permissions as custom_permissions

from plugins.instamojo.models import UserModel as InstaMojoUserModel

from . import accessors, models, serializers, services
from .placeorder import PlaceOrderService


logger = logging.getLogger(__name__)


class WoocomFetchOrderAPI(views.APIView):
    permission_classes = [custom_permissions.IsTokenVerified]

    def post(self, request):
        try:
            request_data = request.data
            auth_token = request_data['auth_token']
            user_woocom_info = services.get_woocom_user_details(auth_token)

            orders = services.get_woocom_orders_service(user_woocom_info, request_data)

            return Response(orders, status=status.HTTP_200_OK)

        except Exception as e:
            return Response('Unable to fetch data', status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserAllEcwidStoresAPI(views.APIView):
    """
    This API is used to fetch all ecwid stores associated with the user.
    """
    permission_classes = [custom_permissions.IsTokenVerified]

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        stores_info = accessors.get_all_ecwid_stores_for_user(auth_token)

        if not stores_info:
            raise NotFound({"error": "No stores found"})

        return Response({"stores": stores_info})


class UserEcwidStoreAPI(generics.CreateAPIView,
                        generics.UpdateAPIView,
                        generics.RetrieveAPIView):

    permission_classes = [custom_permissions.IsTokenVerified]
    serializer_class = serializers.EcwidUserInfoSerializer
    queryset = models.EcwidUserInfo.objects.all()

    lookup_field = 'store_id'


class EcwidFetchOrderAPI(views.APIView):
    """
    API to fetch orders from ecwid servers.
    """
    permission_classes = [custom_permissions.IsTokenVerified]

    def get_filters(self, request):
        page_no = int(request.query_params.get('page_no', 1))
        from_date = request.query_params.get('from_date', '')
        to_date = request.query_params.get('to_date', '')

        filters = {
            'page_no': page_no,
            'from_date': from_date,
            'to_date': to_date
        }

        return filters

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        store_id = request.query_params.get('store_id')
        user_ewcid_info_obj = accessors.get_user_ecwid_info(auth_token, store_id)

        if not user_ewcid_info_obj:
            return Response({
                'error': 'User has no associated ewcid store'
                },
                status=status.HTTP_404_NOT_FOUND)

        filters = self.get_filters(request)
        absolute_url = self.request.build_absolute_uri()  # Used for handling pagination
        orders = services.get_ecwid_orders_service(filters, absolute_url, auth_token, user_ewcid_info_obj)

        return Response(orders, status=status.HTTP_200_OK)


class EcwidUsersAPI(views.APIView):
    """
    API to list all ecwid users and their pickup locations.
    """

    # permission_classes = [custom_permissions.IsTokenVerified]

    def get(self, request):
        users = accessors.get_ecwid_stores_users_data()

        res = []
        for user in users:
            locations = mongo_services.PickupAddressService().get_client_pickup_address(auth_token=user)
            serialized_data = serializers.PickupAddressSerializer(locations, many=True).data

            user_info = {
                'auth_token': user
            }
            user_info.update(users[user])
            user_info['locations'] = serialized_data
            res.append(user_info)

        return Response(res)


class PickupAddressAPI(views.APIView):
    """
    API to list, create, update, delete Pickup Addresses.
    """

    permission_classes = [custom_permissions.IsTokenVerified]
    serializer_class = serializers.PickupAddressSerializer

    def get(self, request):
        """
        list all the clients locations
        """
        auth_token = request.query_params.get('auth_token')

        locations = mongo_services.PickupAddressService().get_client_pickup_address(auth_token=auth_token)

        serialized_data = self.serializer_class(locations, many=True).data

        return Response(serialized_data)

    def post(self, request):
        """
        create a pickup address
        """

        serializer_instance = self.serializer_class(data=request.data)
        if serializer_instance.is_valid(raise_exception=True):
            instance = serializer_instance.save()
            # Remove Object id
            inserted_id = instance.pop('_id')
            instance['mongo_id'] = str(inserted_id)
            return Response(instance, status=status.HTTP_201_CREATED)

    @staticmethod
    def check_pickup_address(mongo_id):
        """
        Checks whether the pickup address with mongo id exists
        """
        pickup_obj = mongo_services.PickupAddressService().get_object_by_mongo_id(mongo_id)
        if not pickup_obj:
            raise NotFound({"error": "No PickupAddress with such mongoid"})

    def put(self, request, mongo_id):
        """
        Update the pickup address. Identified by mongo_id
        """
        self.check_pickup_address(mongo_id)
        serializer_instance = self.serializer_class(data=request.data, partial=True)
        if serializer_instance.is_valid(raise_exception=True):
            service_obj = mongo_services.PickupAddressService()
            service_obj.update_object_by_mongo_id(mongo_id, request.data.copy())

            return Response(status=status.HTTP_200_OK)

    def delete(self, request, mongo_id):
        """
        Remove the pickup address. Identified by mongo_id
        """
        self.check_pickup_address(mongo_id)
        service_obj = mongo_services.PickupAddressService()
        service_obj.remove_object_by_mongo_id(mongo_id)

        return Response(status=status.HTTP_200_OK)


class EcwidUpdateTrackingId(views.APIView):
    # permission_classes = [custom_permissions.IsTokenVerified]

    def post(self, request):

        post_data = request.data

        store_id = post_data.get('store_id')
        client_order_id = post_data.get('client_order_id')
        tracking_id = post_data.get('tracking_id')

        if not all([store_id, client_order_id, tracking_id]):
            return Response({'error': 'Insufficient Data'}, status=status.HTTP_400_BAD_REQUEST)

        sucess, error_info = services.update_tracking_id_ecwid(store_id, tracking_id, client_order_id)

        if not sucess:
            return Response({'error': error_info}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'success': True}, status=status.HTTP_200_OK)


class EcwidUpdateOrderStatus(views.APIView):
    # permission_classes = [custom_permissions.IsTokenVerified]

    def post(self, request):

        post_data = request.data

        store_id = post_data.get('store_id')
        client_order_id = post_data.get('client_order_id')
        order_status = post_data.get('status')

        if not all([store_id, client_order_id, order_status]):
            return Response({'error': 'Insufficient Data'}, status=status.HTTP_400_BAD_REQUEST)

        ecwid_user_obj = models.EcwidUserInfo.objects.get(store_id=store_id)
        ecwid_status = services.get_client_ecwid_status(ecwid_user_obj, order_status)
        if ecwid_status and ecwid_user_obj.update_tracking_status:
            sucess, error_info = services.update_order_status_ecwid(ecwid_user_obj, store_id, ecwid_status, client_order_id)

            if not sucess:
                return Response({'error': error_info}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error': "status not found"}, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK)


platform_info_mapper = {
    "eazyslips": accessors.get_all_eazyslips_stores_for_user,
    "ecwid": accessors.get_all_ecwid_stores_for_user,
    "magento_v2": accessors.get_all_magento_stores_for_user,
    "easyecom": accessors.get_all_easyecom_stores_for_user,
    "jungleworks": accessors.get_all_jungleworks_stores_for_user,
    "opencart": accessors.get_all_opencart_stores_for_user,
    'prestashop': accessors.get_all_prestashop_stores_for_user,
    "bikayi": accessors.get_all_bikayi_stores_for_user,
    "shopify": accessors.get_all_shopify_stores_for_user,
    "magento_v1":accessors.get_all_magento1_stores_for_user,
    "wix": accessors.get_all_wix_stores_for_user,
    "zoho": accessors.get_all_zoho_stores_for_user,
    "instamojo": accessors.get_all_instamojo_stores_for_user,
}


class UserAllStoresAPI(views.APIView):
    """
    This API is used to fetch all ecwid stores associated with the user.
    """
    permission_classes = [custom_permissions.IsTokenVerified]

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        platform_type = request.query_params.get('platform_type', '')
        if platform_type:
            stores_info = platform_info_mapper[platform_type](auth_token)
        else:
            stores_info = accessors.get_all_ecwid_stores_for_user(auth_token)

        if not stores_info:
            raise NotFound({"error": "No stores found"})

        return Response({"stores": stores_info})


class ClientAllPlatformAPI(views.APIView):
    """
    This API is used to fetch all stores associated with the user.
    """
    permission_classes = [custom_permissions.IsTokenVerified]

    def get(self, request):
        auth_token = request.query_params.get('auth_token')
        stores_info = services.get_all_active_platforms_for_user(auth_token)

        if not stores_info:
            raise NotFound({"error": "No stores found"})

        return Response({"stores": stores_info})


class ClientStoreInfoAPI(views.APIView):
    """
    API to retrieve client store information
    """

    permission_classes = [custom_permissions.IsTokenVerified]

    REQUIRED_KEYS = ('auth_token', 'shop_name', 'platform', )

    PLATFORM_MODEL_MAPPING = {
        'ecwid': models.EcwidUserInfo,
        'eazy_slips': models.EazySlipsUserInfo,
        'magento': models.MagnetoUserInfo,
        'easy_ecom': models.EasyEcomUserInfo,
        'jungle_works': models.JungleworksUserInfo,
        'opencart': models.OpencartUserInfo,
        'prestashop': models.PrestaShopUserModel,
        'bikayi': models.BikayiUserInfo,
        'shopify': models.ShopifyUserModel,
        'magento1':models.MagnetoUserInfo,
        'wix': models.WixUserInfo,
        'zoho':models.ZohoUserModel,
        'instamojo': InstaMojoUserModel,

    }

    def validate_request(self, query_params) -> list:
        """
        validate request parameters
        ~~ validates whether mandatory keys are present
        ~~ validates whether platform value is one of registered

        :param query_params: query params received in request
        """

        errors = []

        # validate whether all required keys are present in request
        missing_keys = []
        for key in self.REQUIRED_KEYS:
            if not query_params.get(key):
                missing_keys.append(key)
        if missing_keys:
            errors.append(f'Mandatory key[s] missing - {", ".join(missing_keys)}')
            return errors

        # validate whether platform value is one of registered
        platform_model = self.PLATFORM_MODEL_MAPPING.get(query_params['platform'])
        if not platform_model:
            errors.append(f'Invalid platform value - {query_params["platform"]},'
                          f' valid values are: {list(self.PLATFORM_MODEL_MAPPING.keys())}')

        return errors

    def get(self, request):
        qp = request.query_params
        request_schema_errors = self.validate_request(qp)
        if request_schema_errors:
            return Response({'err': request_schema_errors}, status=status.HTTP_400_BAD_REQUEST)

        platform_model = self.PLATFORM_MODEL_MAPPING[qp['platform']]

        try:
            store_info = list(platform_model.objects.filter(
                auth_token=qp['auth_token'],
                shop_name=qp['shop_name']
            ).values())
        except FieldDoesNotExist as exc:
            logger.exception(f'Exception raised for querying {platform_model.__name__} with filters '
                             f'auth_token: {qp["auth_token"]}, shop_name: {qp["shop_name"]}. Exception: {str(exc)}')
            return Response({'err': 'Invalid platform-shop_name configuration. Please contact at sales@pickrr.com'},
                            status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        return Response(data=store_info, status=status.HTTP_200_OK)


class ClientAllStoresInfoAPI(views.APIView):
    """
    API to retrieve client store information
    """

    REQUIRED_KEYS = ('auth_token', )

    PLATFORM_MAPPING = {
        'ecwid': {'model': models.EcwidUserInfo},
        'eazy_slips': {'model': models.EazySlipsUserInfo},
        'magento': {'model': models.MagnetoUserInfo},
        'easy_ecom': {'model': models.EasyEcomUserInfo},
        'jungle_works': {'model': models.JungleworksUserInfo},
        'opencart': {'model': models.OpencartUserInfo},
        'prestashop': {'model': models.PrestaShopUserModel},
        'bikayi': {'model': models.BikayiUserInfo},
        'shopify': {'model': models.ShopifyUserModel},
        'magento1': {'model': models.MagnetoUserInfo},
        'zoho': {'model': models.ZohoUserModel},
        'instamojo': {'model': InstaMojoUserModel}
    }

    def validate_request(self, query_params) -> list:
        """
        validate request parameters
        ~~ validates whether mandatory keys are present

        :param query_params: query params received in request
        """

        errors = []

        # validate whether all required keys are present in request
        missing_keys = []
        for key in self.REQUIRED_KEYS:
            if not query_params.get(key):
                missing_keys.append(key)
        if missing_keys:
            errors.append(f'Mandatory key[s] missing - {", ".join(missing_keys)}')

        return errors

    def get(self, request):
        qp = request.query_params
        request_schema_errors = self.validate_request(qp)
        if request_schema_errors:
            return Response({'err': request_schema_errors}, status=status.HTTP_400_BAD_REQUEST)

        data = []
        for platform_type, platform_mapper in self.PLATFORM_MAPPING.items():
            model = platform_mapper['model']
            try:
                stores_info = list(model.objects.filter(
                    auth_token=qp['auth_token']
                ).values())
                for store_info in stores_info:
                    store_info['platform_type'] = platform_type

                data.extend(stores_info)
            except FieldDoesNotExist as exc:
                logger.exception(f'Exception raised for querying {model.__name__} with filters '
                                 f'auth_token: {qp["auth_token"]}. Exception: {str(exc)}')
                return Response({'err': 'Invalid platform configuration. Please contact at sales@pickrr.com'},
                                status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        woocommerce_data = services.WooCommercePickrrService.fetch_user_data_from_pickrr(auth_token=qp['auth_token'])
        if not woocommerce_data.get('err'):
            woocommerce_data['platform_type'] = "woocommerce"
            data.append(woocommerce_data)

        return Response(data=data, status=status.HTTP_200_OK)


class PlaceOrderApi(PlaceOrderService):

    def post(self, request):
        try:
            post_data = request.data
            place_order_service = PlaceOrderService()
            validated_data = place_order_service.validate_order_data(post_data)
            place_order_res = place_order_service.place_order_to_pickrr(validated_data)
            return Response(data=place_order_res, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'err': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class TranslateData(views.APIView):

    def post(self, request):
        try:
            post_data = request.data
            translate_service = services.TranslateService()
            translated_data = translate_service.translate_data(post_data)
            return Response(data=translated_data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'err': str(e), "translated_data": []}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
