from django.db import models


class UserModel(models.Model):
    store_id = models.CharField(max_length=255)
    website_url = models.CharField(
        max_length=255, default=None,
        help_text='website domain.'
    )
    refresh_token = models.CharField(max_length=255)
    email = models.EmailField(verbose_name='Email Address')
    auth_token = models.CharField(max_length=255)
    access_token = models.CharField(max_length=255)
    access_token_created_at = models.DateTimeField()
    access_token_expires_in = models.IntegerField() #in seconds
    shop_name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    pull_status = models.CharField(
        max_length=16, blank=True, null=True,
        help_text='string representation of integer denoting status of order defined as per seller,'
                  'this status orders will be pulled from instamojo'
    )
    order_placed = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='order placed mapping with seller'
    )
    picked_up = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='picked up mapping with seller'
    )
    transit = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='transit mapping with seller'
    )
    rto = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='rto mapping with seller'
    )
    cancelled = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='cancelled mapping with seller'
    )
    delivered = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='delivered mapping with seller'
    )
    rtd = models.CharField(
        max_length=64, blank=True, null=True,
        help_text='rtd mapping with seller'
    )
    update_tracking_id = models.BooleanField(default=False)
    update_tracking_status = models.BooleanField(default=False)
    is_multilocation = models.BooleanField(default=False)
    pickrr_accounts = models.TextField(
        help_text='acc_name:auth_token,acc_name:auth_token'
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['auth_token', 'shop_name'],
                                    name='auth_token_shop_name_idx'),
        ]

    def __str__(self):
        return self.shop_name


class PickupAddress(models.Model):
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, verbose_name="Full Name")
    address_line = models.TextField()
    pin_code = models.CharField(max_length=6, verbose_name="Pincode")
    phone_number = models.CharField(max_length=28, verbose_name="Phone Number")
    seller_gstin = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(verbose_name="Email Address", blank=True, null=True)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        ordering = ['-is_default']
