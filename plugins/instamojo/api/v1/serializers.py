from rest_framework import serializers


class AppAuthorizationSerializer(serializers.Serializer):
    auth_token = serializers.CharField()
    shop_name = serializers.CharField()

    class Meta:
        fields = ('auth_token', 'shop_name')


class FetchOrdersSerializer(serializers.Serializer):
    auth_token = serializers.CharField()
    shop_name = serializers.CharField()
    limit = serializers.IntegerField(required=False)
    page = serializers.IntegerField(required=False)
    pull_status = serializers.CharField(required=False)
    payment_status = serializers.CharField(required=False)
    payment_method = serializers.CharField(required=False)
    waybill__isblank = serializers.CharField(required=False)

    class Meta:
        fields = ('auth_token', 'shop_name', 'limit', 'page', 'pull_status',
                  'payment_status', 'payment_method', 'waybill__isblank')


class OrderStatusUpdateSerializer(serializers.Serializer):
    auth_token = serializers.CharField()
    shop_name = serializers.CharField()
    order_id = serializers.IntegerField()
    order_status = serializers.CharField()
    comments = serializers.CharField(required=False)

    class Meta:
        fields = ('auth_token', 'shop_name', 'order_id', 'order_status',
                  'comments')


class CarrierUpdateSerializer(serializers.Serializer):
    auth_token = serializers.CharField()
    shop_name = serializers.CharField()
    order_id = serializers.IntegerField()
    tracking_number = serializers.CharField()
    carrier = serializers.CharField()
    tracking_url = serializers.URLField()

    class Meta:
        fields = ('auth_token', 'shop_name', 'order_id', 'tracking_number',
                  'carrier', 'tracking_url')
