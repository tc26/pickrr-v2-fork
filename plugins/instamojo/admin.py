from django.contrib import admin
from .models import UserModel, PickupAddress


class PickupAddressInline(admin.TabularInline):
    model = PickupAddress


class UserModelAdmin(admin.ModelAdmin):
    inlines = (PickupAddressInline,)


admin.site.register(UserModel, UserModelAdmin)
