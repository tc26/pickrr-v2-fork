BASE_URL = 'https://www.instamojo.com'
STORE_ORDERS_ENDPOINT = '/v2/store/orders/'
AUTHORIZE_ENDPOINT = '/oauth2/authorize/'
ACCESS_TOKEN_ENDPOINT = '/oauth2/token/'
